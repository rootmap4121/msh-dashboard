<?php
extract($_GET);
include('class/auth.php');
$table3="stock";
$table="stockoutreport";
if(!empty($quantity))
{
    if(!empty($emplidfor))
    {
        if($pquantity>=$quantity)
        {
            if($obj->exists($table3,array("pid"=>$pid))!=0)
            {
                if($obj->amount_decre($table3,array("pid"=>$pid),"quantity",$quantity)==1)
                {
                    $reportarray=array("pid"=>$pid,"quantity"=>$quantity,"price"=>$price,"emplid"=>$emplid,"emplidfor"=>$emplidfor,"date"=>date('Y-m-d'),"status"=>1);
                    if($obj->insert($table,$reportarray)==1)
                    {
                        $errmsg_arr[]= 'Successfully Added';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./stockoutlist.php");
                            exit();
                        }
                    }
                    else
                    {
                        $errmsg_arr[]='Failed Please Try Again';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./stockoutlist.php");
                            exit();
                        }
                    }
                }
                else 
                {
                    $errmsg_arr[]='Failed Please Try Again';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./stockoutlist.php");
                        exit();
                    }
                }
            }
            else 
            {
                if($obj->insert($table3,array("pid"=>$pid,"quantity"=>$quantity,"price"=>$price,"reorder"=>$reorder,"date"=>date('Y-m-d'),"status"=>1))==1)
                {
                    $errmsg_arr[]='Product Does Not Exist In Stock List';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./stockoutlist.php");
                        exit();
                    }
                }
                else 
                {
                    $errmsg_arr[]='Failed Please Try Again';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./stockoutlist.php");
                        exit();
                    }
                }
            }
        }
        else 
        {
            $errmsg_arr[]='Your Quantity is More Than Available Quantity';
            $errflag = true;
            if ($errflag) 
            {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./stockoutlist.php");
                exit();
            } 
        }
    }
    else 
    {
            $errmsg_arr[]='Please Select An Employee for Stock Out';
            $errflag = true;
            if ($errflag) 
            {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./stockoutlist.php");
                exit();
            } 
    }
}
else
{
    $errmsg_arr[]='Something is Wrong';
    $errflag = true;
    if ($errflag) 
    {
        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
        session_write_close();
        header("location: ./stockoutlist.php");
        exit();
    }
}    
?>
