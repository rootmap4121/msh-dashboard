<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Leave Info</a></li><li class='active'>Leave List</li>";
$table="requisation_leaveapplication";
if(isset($_POST['submit']))
{
    if(!empty($_POST['quantity']) && !empty($_POST['leave_type']))
    {
        $insert=array("leave_type"=>$_POST['leave_type'],"quantity"=>$_POST['quantity'],"year"=>date('Y'),"date"=>  date('Y-m-d'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
    }
 else {
                        $errmsg_arr[]= 'Some Field Are Empty';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
    }   
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $updatearray=array("id"=>$_POST['id'],"leave_type"=>$_POST['leave_type'],"quantity"=>$_POST['quantity']);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Leave Quantity</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                               <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Type </label>

                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="leave_type" placeholder="Leave Type" class="col-xs-10 col-sm-5">
                                            <?php
                                            $sqlleave=$obj->SelectAll("leave_type");
                                            if(!empty($sqlleave))
                                            foreach($sqlleave as $leave):
                                            ?>
                                            <option value="<?php echo $leave->id; ?>"><?php echo $leave->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> This Year Leave Quantity </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="quantity" placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>


                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

								<div class="hr hr-18 dotted hr-double"></div>           

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Leave Quantity List</h3>

                                        <div class="table-responsive">
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Leave Type</th>
                                                        <th>Leave Quantity</th>
                                                        <th>Date</th>
                                                        <th>Years</th>
                                                        <th>Edit </th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $row->id; ?></td>
                                                            <td><?php echo $obj->SelectAllByVal("leave_type","id",$row->leave_type,"name") ?></td>
                                                            <td><?php echo $row->quantity; ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->date; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->year; ?></span></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-pencil bigger-130"></i> Edit</a>
                                                                </div>
								<div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Update Leave
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Leave Type </label>

                                                                                                <div class="col-sm-8">
                                                                                                    <select id="form-field-1" name="leave_type" placeholder="Leave Type" class="col-xs-10 col-sm-5">
                                                                                                        <?php
                                                                                                        $sqlleave=$obj->SelectAll("leave_type");
                                                                                                        foreach($sqlleave as $leave):
                                                                                                        ?>
                                                                                                        <option <?php if($row->leave_type==$leave->id){ ?> selected="selected" <?php } ?> value="<?php echo $leave->id; ?>"><?php echo $leave->name; ?></option>
                                                                                                        <?php
                                                                                                        endforeach;
                                                                                                        ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                            <input type="hidden" name="id" value="<?php echo $row->id; ?>">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> This Year Leave Quantity </label>

                                                                                                <div class="col-sm-8">
                                                                                                    <input type="text" id="form-field-1" value="<?php echo $row->quantity; ?>" name="quantity" placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->




                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
