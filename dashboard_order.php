<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Dashboard Employee</a></li>";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
  <style type="text/css">
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:3px; border:#4e95f4 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{
		background: #D19500;
	}
	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		background: #f1da36;
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background: #EFEACB;
	}
</style>
</head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    <?php if($_SESSION['SESS_AMSIT_EMP_STATUS']==3){ ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <h3 class="header smaller lighter green"> Dashboardx</h3>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-dark">
                                                        <h5 class="bigger lighter"> <i class="icon-group"></i> Employee &AMP; Requisation </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="employeelist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                List of Employee
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="requisitionlist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Store Requisation List
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="vehiclerequest_requisitionlist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                vehicle request list
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="reservation_requisitionlist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Reservation Meeting list
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="onlineleaveapplicationform_list.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                leave application List
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-orange">
                                                            <h5 class="bigger lighter"> <i class="icon-hdd"></i> Product Info </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="product.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                List of Store Product
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="assetproductlist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                List of Asset Product
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="fieldproductlist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Inventory Field Product list
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="itemcondition_list.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Product Item Condition list
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="barcode_list.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Product Barcode List
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-blue">
                                                            <h5 class="bigger lighter"> <i class="icon-bullhorn"></i> Order Info </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="addorder.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Add Product Order
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="order.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                List Product Order
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="field_inventory_addorder.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Add Field Inventory Product Order
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="field_inventory_order.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Field Inventory Product Order List
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-green">
                                                            <h5 class="bigger lighter"> <i class="icon-bar-chart"></i> Report </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="report_stock.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Stock product
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="report_stockin.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Stock in product
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="report_stockout.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Stock out product
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="report_pricelist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Price List
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="report_product.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Product List
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-red">
                                                        <h5 class="bigger lighter"> <i class="icon-exchange"></i> Stock </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="stock.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Stock Detail Info
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="field_inventory_stock.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Field Inventory Stock Info
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="stockinproduct.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Stock In
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="field_inventory_stockinproduct.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Field Inventory Stock In Product
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="field_inventory_stockoutlist.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Field Inventory Stock Out Product
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-purple">
                                                            <h5 class="bigger lighter"> <i class="icon-cog"></i> Setting System </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="designation.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Designation Add | List
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="branch.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Branch Add | List
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="category.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Category Add | List
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="subcategory.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Sub-Category Add | List
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="producttype.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Product-Type Add | List
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-pink">
                                                            <h5 class="bigger lighter"> <i class="icon-plus"></i> Information Store </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="supplier.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Supplier | Add List
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="addproduct.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Product Add
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="employee.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Employee Add 
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="requisition.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Store Requisation From
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="onlineleaveapplicationform.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                leave application Form
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-grey">
                                                            <h5 class="bigger lighter"> <i class="icon-desktop"></i> Super Link </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Dash Board Online
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="#" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Dash Board Offline
                                                                                </a>
                                                                            </li>

                                                                            <li>
                                                                                <a href="export_db.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                <span class="label label-sm label-warning">Backup Database </span>
                                                                                </a>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <?php }elseif($_SESSION['SESS_AMSIT_EMP_STATUS']==2){ ?>
                        <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            
                                
                            
                                <div class="row">
                                    

                                    

                                    <div class="col-xs-6 col-sm-5 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body dark_orange topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/2.jpg" height="140" width="490">
                                                            </div>
                                                    </div>
                                                <div class="widget-body orange_gradient" style="height: 600px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black">HR Designation Wise</h3>
                                                             <table class="TFtable">
                                                                 <tr><td style="background: #D19500;">Designation</td>
                                                                     <td style="background: #D19500;">Male</td>
                                                                     <td style="background: #D19500;">Female</td>
                                                                     <td style="background: #D19500;">Total</td>
                                                                 </tr>
                                                                 <?php
                                                                 $depsql=$obj->SelectAll("designation");
                                                                 $x=1;
                                                                 if(!empty($depsql))
                                                                 foreach($depsql as $dep):

                                                                 ?>
                                                                 <tr>
                                                                     <td><?php echo $x.". "; echo $dep->name; ?></td>
                                                                     <td><?php
                                                                     $sqlmalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",1);
                                                                     if($sqlmalecount!=0)
                                                                     {
                                                                        $sqlmale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",1);
                                                                        $male=0;
                                                                        foreach($sqlmale as $mal):
                                                                            $male+=1;
                                                                        endforeach;
                                                                     }
                                                                     else
                                                                     {
                                                                         $male=0;
                                                                     }
                                                                     echo $male;
                                                                     ?></td>
                                                                     <td>
                                                                     <?php
                                                                     $sqlfemalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",2);
                                                                     if($sqlfemalecount!=0)
                                                                     {
                                                                        $sqlfemale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",2);
                                                                        $female=0;
                                                                        foreach($sqlfemale as $femal):
                                                                            $female+=1;
                                                                        endforeach;
                                                                     }
                                                                     else 
                                                                     {
                                                                         $female=0;
                                                                     }
                                                                     echo $female;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE designation='".$dep->id."'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                                 <?php
                                                                 $x++;
                                                                 endforeach; 
                                                                 ?>
                                                                 <tr>
                                                                     <td>Grand Total </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='1'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='2'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                            </table>
                                                      </div>
                                                    </div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-7 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                <div class="widget-body dark_green topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                            <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/3_1.jpg" height="140" width="900">
                                                            </div>
                                                    </div>
                                                <div class="widget-body green_gradient" style="height: 600px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter green"> 
                                                                 Product Order Menus
                                                             </h3>
                                                             
                                                             
                                                            
                                                            
                                                            <a href="addorder.php" class="col-sm-3 dash_panel" style="padding: 0;">
                                                                <img src="images/icons/order.png" class="dash_img">
                                                                <div class="dash_label">Add Order</div>
                                                            </a>
                                                            
                                                            <a href="order.php" class="col-sm-3 dash_panel" style="padding: 0;">
                                                                <img src="images/icons/orderlist.png" class="dash_img">
                                                                <div class="dash_label">Order List</div>
                                                            </a>
                                                            <div class="clearfix"></div>
                                                            <h3 class="header smaller lighter red"> 
                                                                Field Inventory Product Order Menus
                                                             </h3>
                                                             
                                                            <a href="field_inventory_addorder.php" class="col-sm-3 dash_panel" style="padding: 0;">
                                                                <img src="images/icons/shopppingcart.png" class="dash_img">
                                                                <div class="dash_label">Add Order</div>
                                                            </a>
                                                            
                                                            <a href="field_inventory_order.php" class="col-sm-3 dash_panel" style="padding: 0;">
                                                                <img src="images/icons/medical_invoice_information.png" class="dash_img">
                                                                <div class="dash_label">Order List</div>
                                                            </a>
                                                          
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <?php }elseif($_SESSION['SESS_AMSIT_EMP_STATUS']==1){ ?>
                        
                        <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            
                            
                            
                            <h3 class="header smaller lighter green"> Dashboard</h3>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-dark">
                                                        <h5 class="bigger lighter"> <i class="icon-group"></i> Leave Application Form </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="onlineleaveapplicationform_personal.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Leave Application Form
                                                                                </a>
                                                                            </li>

                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-orange">
                                                            <h5 class="bigger lighter"> <i class="icon-hdd"></i> My Leave Application List </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="onlineleaveapplicationform_list_personal.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                My Leave Application List
                                                                                </a>
                                                                            </li>

                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-blue">
                                                            <h5 class="bigger lighter"> <i class="icon-bullhorn"></i> Add New Requisation </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="requisition_personal.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                Add New Requisation
                                                                                </a>
                                                                            </li>

                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box">
                                            <div class="widget-box">
                                                    <div class="widget-header header-color-green">
                                                            <h5 class="bigger lighter"> <i class="icon-bar-chart"></i> My Requisation List </h5>
                                                    </div>

                                                    <div class="widget-body">
                                                            <div class="widget-main">
                                                                    <ul class="list-unstyled spaced2">
                                                                            <li>
                                                                                <a href="requisitionlist_personal.php" style="text-decoration: none;">
                                                                                <i class="icon-ok green"></i>
                                                                                My Requisation List
                                                                                </a>
                                                                            </li>

                                                                            
                                                                    </ul>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            

                            
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="space-6"></div>


                                <div class="col-sm-6">
                                        <div class="widget-box">
                                                <div class="widget-header widget-header-flat widget-header-small">
                                                        <h5>
                                                                <i class="icon-signal"></i>
                                                                Activity
                                                        </h5>

                                                </div>

                                                <div class="widget-body">
                                                        <div class="widget-main">
                                                                <div id="piechart-placeholder"></div>

                                                                <div class="hr hr8 hr-double"></div>
                                                        </div><!-- /widget-main -->
                                                </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                </div><!-- /span -->
                    <div class="col-sm-6">
                            <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                            <h4 class="lighter">
                                                    <i class="icon-signal"></i>
                                                    Sale Stats
                                            </h4>

                                            <div class="widget-toolbar">
                                                    <a href="#" data-action="collapse">
                                                            <i class="icon-chevron-up"></i>
                                                    </a>
                                            </div>
                                    </div>

                                    <div class="widget-body">
                                            <div class="widget-main padding-4">
                                                    <div id="sales-charts"></div>
                                            </div><!-- /widget-main -->
                                    </div><!-- /widget-body -->
                            </div><!-- /widget-box -->
                    </div>
                        </div><!-- /row -->
                        
                        <h1 align="center" style="font-family: monospace; font-size: 60px;">Welcome To Dashboard</h1>

                                                <h3 align="center" style="font-family: monospace;">Your Current IP Address : <?php echo $_SERVER['REMOTE_ADDR']; ?></h3>
                        <h3 align="center" style="font-family: monospace;">Access System Via : <?php echo $_SERVER['HTTP_USER_AGENT']; ?></h3>
                                                                <h2 align="center" style="font-family: monospace;">Please Note  : Your Current All Activity Has Been Monitored Via Auto Robot,Don't Try To Abuse It</h2>
                        <?php } ?>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->

<?php
//include('class/colornnavsetting.php');
include('class/footer.php');
?>


                <?php //echo $obj->bodyfooter(); ?>

		<!--[if !IE]> -->

		<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
						size: size
					});
				})
			
				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html', {tagValuesAttribute:'data-values', type: 'bar', barColor: barColor , chartRangeMin:$(this).data('min') || 0} );
				});
			
			
			
			
			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "Leave Application",  data: <?php echo $obj->emp_leave_quantity($input_by); ?>, color: "#68BC31"},
				{ label: "Store Requisation",  data: <?php echo $obj->totalrowsbyDID("requisationlist","empid",$input_by); ?>, color: "#2091CF"},
				{ label: "Vehecle Requisation",  data: <?php echo $obj->totalrowsbyDID("requisationlist_vehicle","empid",$input_by); ?>, color: "#AF4E96"},
				{ label: "Total Stock Out",  data: <?php echo $obj->totalrowsbyDID("stockoutreport","emplid",$input_by); ?>, color: "#DA5430"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
			
			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);
			
			
			
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}
				
			 });
			
			
			
			
			
			
				var d1 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d1.push([i, Math.sin(i)]);
				}
			
				var d2 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d2.push([i, Math.cos(i)]);
				}
			
				var d3 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.2) {
					d3.push([i, Math.tan(i)]);
				}
				
			
				var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
				$.plot("#sales-charts", [
					{ label: "Domains", data: d1 },
					{ label: "Hosting", data: d2 },
					{ label: "Services", data: d3 }
				], {
					hoverable: true,
					shadowSize: 0,
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						tickLength: 0
					},
					yaxis: {
						ticks: 10,
						min: -2,
						max: 2,
						tickDecimals: 3
					},
					grid: {
						backgroundColor: { colors: [ "#fff", "#fff" ] },
						borderWidth: 1,
						borderColor:'#555'
					}
				});
			
			
				$('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('.tab-content')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			
			
				$('.dialogs,.comments').slimScroll({
					height: '300px'
			    });
				
				
				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				});
			
				$('#tasks').sortable({
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'draggable-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					stop: function( event, ui ) {//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
						$(ui.item).css('z-index', 'auto');
					}
					}
				);
				$('#tasks').disableSelection();
				$('#tasks input:checkbox').removeAttr('checked').on('click', function(){
					if(this.checked) $(this).closest('li').addClass('selected');
					else $(this).closest('li').removeClass('selected');
				});
				
			
			})
		</script>
                </body>
                </html>
