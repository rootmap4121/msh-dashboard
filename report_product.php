<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Report</a></li>";
$table="product";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                 <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Product List</h3>


                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Product Name</th>
                                                        <th>Active Ingredients - Pack Size</th>
                                                        <th>Flat Rate</th>
                                                        <th>MRP</th>
                                                        
                                                        <th>Quantity</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td>
                                                                <?php 
                                                                $pack=$obj->SelectAllByID("product_type",array("id"=>$row->pack));
                                                                foreach($pack as $pac):
                                                                ?>
                                                                <span class="label label-sm label-success"><?php echo $pac->name; ?></span>
                                                                - 
                                                                <span class="label label-sm label-success"><?php echo $pac->size; ?></span>
                                                                <?php endforeach; ?>
                                                            </td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->price; ?></span></td>
                                                            <td><?php echo $row->mrp; ?></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->quantity; ?></span></td>
                                                            <td>
                                                             <img src="barcode/test_1D.php?text=<?php echo $row->barcode; ?>" alt="barcode" height="55" style="margin-right:50px; margin-bottom:20px;" />   

                                                            </td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 $x++; endforeach; 
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                       
                                                                							
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
