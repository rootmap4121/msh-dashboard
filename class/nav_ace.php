<ul class="nav ace-nav">
    <?php if($obj->filename()!='index.php'){ ?>
    <li class="red">
        <a href="javascript:history.go(-1)">
            <i class="icon-undo bigger-230"></i>
            Back
        </a>
    </li>
    <?php } ?>
   <li class="light-blue">
        <a href="accesslist.php">
            <i class="icon-credit-card"></i>
			Access Log History
        </a>
    </li>
    <li class="light-blue">
        <a href="change_password.php">
            <i class="icon-key"></i>
			Change Password 
        </a>
    </li>
<?php if($_SESSION['SESS_AMSIT_EMP_STATUS']==3){ ?>
    <li class="light-blue">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="assets/avatars/user.jpg" alt="Jason's Photo" />
            <span class="user-info">
                <small>Welcome,</small>
                <?php echo $_SESSION['SESS_AMSIT_EMP_NAME']; ?>
            </span>

            <i class="icon-caret-down"></i>
        </a>

        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <li>
                <a href="dashboard_setting.php">
                    <i class="icon-cog"></i>
                    Settings
                </a>
            </li>

            <li>
                <a href="test_profile.php">
                    <i class="icon-user"></i>
                    Profile
                </a>
            </li>

            <li class="divider"></li>

            <li>
                <a href="class/logout.php">
                    <i class="icon-off"></i>
                    Logout
                </a>
            </li>
        </ul>
    </li>
<?php }elseif($_SESSION['SESS_AMSIT_EMP_STATUS']==2){  ?>
    <li class="light-blue">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="assets/avatars/user.jpg" alt="Jason's Photo" />
            <span class="user-info">
                <small>Welcome,</small>
                <?php echo $_SESSION['SESS_AMSIT_EMP_NAME']; ?>
            </span>

            <i class="icon-caret-down"></i>
        </a>

        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <li>
                <a href="dashboard_setting.php">
                    <i class="icon-cog"></i>
                    Settings
                </a>
            </li>

            <li>
                <a href="test_profile.php">
                    <i class="icon-user"></i>
                    Profile
                </a>
            </li>

            <li class="divider"></li>

            <li>
                <a href="class/logout.php">
                    <i class="icon-off"></i>
                    Logout
                </a>
            </li>
        </ul>
    </li>
<?php }elseif($_SESSION['SESS_AMSIT_EMP_STATUS']==1){ ?>
    <?php
    if($_SESSION['SESS_AMSIT_EMP_STATUS']==1){
    $p4=$obj->notification_ind_super(4,$input_by);
    $p3=$obj->notification_ind_super(3,$input_by);
    $p2=$obj->notification_ind_super(2,$input_by);
    $p1=$obj->notification_ind_super(1,$input_by);

    $r1=$obj->notification_ind_user(1,$input_by);
    $r2=$obj->notification_ind_user(2,$input_by);
    $r3=$obj->notification_ind_user(3,$input_by);
    $r4=$obj->notification_ind_user(4,$input_by);
    
    $not=$p1+$p2+$p3+$p4+$r1+$r2+$r3+$r4;
    if($not!=0)
        {
    ?>
        <li class="red">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="icon-bell-alt icon-animated-bell"></i>
                <span class="badge badge-important"><?php echo $not; ?></span>
        </a>
    </li>
    <?php
        }
    }
    ?>
    <li class="light-blue">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src="assets/avatars/user.jpg" alt="Jason's Photo" />
            <span class="user-info">
                <small>Welcome,</small>
                <?php echo $_SESSION['SESS_AMSIT_EMP_NAME']; ?>
            </span>

            <i class="icon-caret-down"></i>
        </a>

        <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <li>
                <a href="#">
                    <i class="icon-cog"></i>
                    Settings
                </a>
            </li>

            <li>
                <a href="test_profile.php">
                    <i class="icon-user"></i>
                    Profile
                </a>
            </li>

            <li class="divider"></li>

            <li>
                <a href="class/logout.php">
                    <i class="icon-off"></i>
                    Logout
                </a>
            </li>
        </ul>
    </li>

<?php } ?>
</ul><!-- /.ace-nav -->

