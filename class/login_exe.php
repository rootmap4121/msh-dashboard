<?php
session_start();
$vvv = $_SERVER['REMOTE_ADDR'];
$ddd = date('Y-m-d');
$errmsg_arr = array();
$errflag = false;
extract($_GET);
if ($username == '') {
    $errmsg_arr[] = " Username Missing";
    $errflag = true;
}

if ($password == '') {
    $errmsg_arr[] = " Password Missing";
    $errflag = true;
}

if ($errflag) {
    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
    session_write_close();
    header("location: ../login.php");
    exit();
}
include_once ('db_Class.php');
$con = new db_class();
$user =$con->cleanQuery($username);
$getpassword =md5($con->cleanQuery($password));
//$getpassword =$con->cleanQuery($password);
$msg = '';
$exist_array = array("username" => $user);

if ($con->login($user,$getpassword) == 1) {
    session_regenerate_id();
    $data=$con->SelectAllByID("employee", $exist_array);
    foreach ($data as $row):
       $_SESSION['SESS_AMSIT_APPS_ID'] = $row->id;
       $_SESSION['SESS_AMSIT_EMP_NAME'] = $row->name;
       $_SESSION['SESS_AMSIT_EMP_STATUS'] = $row->status;
    endforeach;
    session_write_close();
    header("location: ../index.php");
    exit();
} else {
    $errmsg_arr[] = " Wrong Password";
    $errflag = true;
    if ($errflag) {
    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
    session_write_close();
    header("location: ../login.php");
    exit();
    }
}