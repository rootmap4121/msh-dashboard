<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Stock Info</a></li>";
$table="field_inventory_stock";
$table2="field_inventory_stockoutreport";
$table3="field_inventory_stockreport";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>
    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Stock Detail  <span style="float: right;"><a href="#" style="text-decoration: none;"><i class="icon-print"></i> Print Stock Detail</a></span></h3>
                                        <div class="table-header">
                                            Results for "Total Stock Product&rsquo;s" (<?php echo $obj->totalrows($table); ?>)
                                        </div>

                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Product Name</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Re-Order </th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><i class="icon-caret-right green"></i> <?php $pro=$obj->SelectAllByID("fieldproduct",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?></td>
                                                            <td><i class="icon-bar-chart orange"></i> <?php echo $row->quantity; ?></td>
                                                            <td><i class="icon-bookmark-empty blue"></i> <?php echo $row->price; ?></td>
                                                            <td><span class="label label-sm label-<?php if($row->reorder<$row->quantity){ ?>success<?php }else{ ?>warning<?php } ?> "><i class="icon-beaker dark"></i> <?php echo $row->reorder; ?></span></td>
                                                            <td><i class="icon-calendar gray"></i> <?php echo $row->date; ?></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-stockout<?php echo $row->id; ?>" role="button" data-toggle="modal"><i class="icon-exchange bigger-130"></i> Stock Out</a>
                                                                </div>
<div id="modal-stockout<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header no-padding">
                                <div class="table-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <span class="white">&times;</span>
                                        </button>
                                        Stock Out Form
                                </div>
                        </div>
                        <!-- /.modal-content -->

                        <form class="form-horizontal" name="designationedit" role="form" action="field_inventory_stockout.php" method="GET">
                            <input type="hidden" name="emplid" value="<?php echo $row->emplid; ?>">
         <?php $ror=$obj->SelectAllByID("fieldproduct",array("id"=>$row->pid)); foreach($ror as $or): ?>                   
         <input type="hidden" name="reorder" value="<?php echo $or->reorder; ?>">
         <?php endforeach; ?>                   
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>

                                <div class="col-sm-9">
                                    <input type="text" disabled="disabled" id="form-field-1" name="name" value="<?php $pro=$obj->SelectAllByID("fieldproduct",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?>" class="col-xs-10 col-sm-5" />
                                    <input type="hidden" name="pid" value="<?php echo $row->pid; ?>"  />
                                    <input type="hidden" name="emplid" value="<?php echo $input_by; ?>"  />
                                    <?php 
                                    if($obj->exists("field_inventory_stockoutreport",array("pid"=>$row->pid))==0)
                                    {
                                        $xp=0;
                                    }
                                    else 
                                    {
                                        $exp=$obj->SelectAllByID("field_inventory_stockoutreport",array("pid"=>$row->pid));
                                        $xp=0;
                                        foreach($exp as $ex):
                                            $xp+=$ex->quantity;
                                        endforeach;
                                    }
                                    ?>
                                    
                                    <input type="hidden" name="pquantity" value="<?php echo $row->quantity-$xp; ?>">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Available Quantity </label>

                                <div class="col-sm-9">
                                    <input type="text"  id="form-field-1" name="quantity" value="<?php echo $row->quantity-$xp; ?>"  class="col-xs-10 col-sm-5" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Stock Out For </label>

                                <div class="col-sm-9">
                                    <select name="emplidfor" style="width: 42%;">
                                        <?php $allemp=$obj->SelectAll("employee"); foreach($allemp as $lmp): ?>
                                        <option value="<?php echo $lmp->id; ?>"><?php echo $lmp->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Unite Price </label>

                                <div class="col-sm-9">
                                    <input type="text" readonly="readonly"  id="form-field-1" name="price" value="<?php echo $row->price; ?>"  class="col-xs-10 col-sm-5" />
                                </div>
                            </div>


                            <div class="space-4"></div>

                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Stock In Now</button>
                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                </div>
                            </div>
                        </form>

                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- end modal form -->
                                                                
                                                            </td>
                                                        </tr>
                                                 <?php $x++; endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                							
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
