<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Online Leave Application Info</a></li><li class='active'>Add Online Leave Application Form</li>";
$table="requisition_vehicle";
extract($_GET);

if(@$_GET['action']=='pdf')
{
include("pdf/MPDF57/mpdf.php");
$html .=$obj->company_report_logo()." ".$obj->company_report_head();
$html .=$obj->company_report_name("Vehicle Request");
    $datas=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
    foreach ($datas as $data):
       $html .="<h3>Serial #</h3>
                            <table width='100%' border='1'  id='sample-table-2' class='table table-hover'>
                              <tr>
                                <td valign='top'><strong>Date : </strong>".$data->date."</td>
                                <td valign='top'>&nbsp;</td>
                                <td width='214' valign='top'>&nbsp;</td>
                                <td width='345' valign='top' align='left'><strong>Signature : </strong> </td>
                                </tr>
                              <tr>
                                <td valign='top' height='27'><strong>Person Requesting : </strong>".$obj->SelectAllByVal("employee","id",$data->emplid,"name")."</td>
                                <td valign='top' height='27'><strong>Passengers Quantity :</strong>".$data->passengerquantity."</td>
                                <td colspan='2' valign='top'><strong>Passengers Name :</strong>".$data->passengernames."</td>
                              </tr>
                              <tr>
                                <td width='277' valign='top'><strong>Designation :</strong>".$obj->emp_designation($data->emplid)."</td>
                                <td width='237' valign='top'><strong>Phone : </strong>".$data->phone."</td>
                                <td valign='top'><strong>Budget Code :</strong>".$data->budgetcode."</td>
                                <td valign='top'><strong>Budget Holder : </strong>".$data->budgetholder."</td>
                              </tr>
                              <tr>
                                <td valign='top' height='29'><strong>Type of Travel : </strong></td>
                                <td colspan='3' valign='top'>".$data->typeoftravel."</td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1'  id='sample-table-2' class='table table-hover'>
  <tr>
    <td width='21%' valign='top'><strong>Start Date - End Date</strong></td>
    <td width='12%' valign='top'><strong>Day</strong></td>
    <td width='12%' valign='top'><strong>Pick up point</strong></td>
    <td width='15%' valign='top'><strong>Destination</strong></td>
    <td width='21%' align='center' valign='top'><strong>Duration</strong></td>
  </tr>
  <tr>
    <td valign='top'>".substr($data->startdatetime,0,10)." - ".substr($data->enddatetime,0,10)."</td>
    <td valign='top'>".$obj->getDays(substr($data->startdatetime,0,10),substr($data->enddatetime,0,10))."</td>
    <td valign='top'>".$data->startpoint."</td>
    <td valign='top'>".$data->destination."</td>
    <td valign='top'>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0'  id='sample-table-2' class='table table-hover'>
  <tr>
    <td width='50%' height='31' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong>".$obj->SelectAllByVal("employee","id",$data->supervisor,"name")."-".$obj->emp_leave_status($data->status)."</td>
    <td width='50%' valign='top' align='right'><strong>Date &amp; Time :</strong>".$data->startdatetime."</td>
    </tr>
  <tr>
    <td valign='top' height='31'><strong>Driver Information : </strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td valign='top' height='31'><strong>Vehicle Information :</strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'><strong>Special Instruction to Driver : </strong></td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>Note : </td>
    </tr>
  </table>";

endforeach;

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('leave.pdf','I');
}
elseif(@$_GET['action']=='excel')
{

header('Content-type: application/excel');
$filename = 'leaveapplicationlist.xls';
header('Content-Disposition: attachment; filename='.$filename);

$html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Leave Application List</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$html .="<body>";
$html .=$obj->company_report_head()."Report Creation Date : ".date('d-m-Y H:i:s');
$html .=$obj->company_report_name("Vehicle Request");

//start table
    $datas=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
    foreach ($datas as $data):
       $html .="<h3>Serial #</h3>
                            <table width='100%' border='1'  id='sample-table-2' class='table table-hover'>
                              <tr>
                                <td valign='top'><strong>Date : </strong>".$data->date."</td>
                                <td valign='top'>&nbsp;</td>
                                <td width='214' valign='top'>&nbsp;</td>
                                <td width='345' valign='top'><strong>Signature : </strong> </td>
                                </tr>
                              <tr>
                                <td valign='top' height='27'><strong>Person Requesting : </strong>".$obj->SelectAllByVal("employee","id",$data->emplid,"name")."</td>
                                <td valign='top' height='27'><strong>Passengers Quantity :</strong>".$data->passengerquantity."</td>
                                <td colspan='2' valign='top'><strong>Passengers Name :</strong>".$data->passengernames."</td>
                              </tr>
                              <tr>
                                <td width='277' valign='top'><strong>Designation :</strong>".$obj->emp_designation($data->emplid)."</td>
                                <td width='237' valign='top'><strong>Phone : </strong>".$data->phone."</td>
                                <td valign='top'><strong>Budget Code :</strong>".$data->budgetcode."</td>
                                <td valign='top'><strong>Budget Holder : </strong>".$data->budgetholder."</td>
                              </tr>
                              <tr>
                                <td valign='top' height='29'><strong>Type of Travel : </strong></td>
                                <td colspan='3' valign='top'>".$data->typeoftravel."</td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1'  id='sample-table-2' class='table table-hover'>
  <tr>
    <td width='21%' valign='top'><strong>Start Date - End Date</strong></td>
    <td width='12%' valign='top'><strong>Day</strong></td>
    <td width='12%' valign='top'><strong>Pick up point</strong></td>
    <td width='15%' valign='top'><strong>Destination</strong></td>
    <td width='21%' align='center' valign='top'><strong>Duration</strong></td>
  </tr>
  <tr>
    <td valign='top'>".substr($data->startdatetime,0,10)." - ".substr($data->enddatetime,0,10)."</td>
    <td valign='top'>".$obj->getDays(substr($data->startdatetime,0,10),substr($data->enddatetime,0,10))."</td>
    <td valign='top'>".$data->startpoint."</td>
    <td valign='top'>".$data->destination."</td>
    <td valign='top'>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0'  id='sample-table-2' class='table table-hover'>
  <tr>
    <td width='50%' height='31' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong>".$obj->SelectAllByVal("employee","id",$data->supervisor,"name")."-".$obj->emp_leave_status($data->status)."</td>
    <td width='50%' valign='top' align='right'><strong>Date &amp; Time :</strong>".$data->startdatetime."</td>
    </tr>
  <tr>
    <td valign='top' height='31'><strong>Driver Information : </strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td valign='top' height='31'><strong>Vehicle Information :</strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'><strong>Special Instruction to Driver : </strong></td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>Note : </td>
    </tr>
  </table>";

endforeach;
//end table

$html .="</tbody></table>";

$html .='</body></html>';

echo $html;

}
elseif(@$_GET['action']=='approve')
{
    $array=array("id"=>$_GET['id'],"status"=>2);
    if($obj->update($table,$array)==1)
    {
		$obj->notification_check_user_update($_GET['id'],"2",$input_by);
                    $errmsg_arr[]= 'Successfully Approved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
elseif(@$_GET['action']=='reject')
{
    $array=array("id"=>$_GET['id'],"status"=>3);
    if($obj->update($table,$array)==1)
    {
		$obj->notification_check_user_update($_GET['id'],"2",$input_by);
                    $errmsg_arr[]= 'Successfully Approved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Vehicle Request Form-<?php echo date('Y'); ?>
                         <a target="_blank" href="<?php echo $obj->filename(); ?>?action=pdf&AMP;id=<?php echo $id; ?>"><img src="images/pdf.png"></a> 
                                <a target="_blank" href="<?php echo $obj->filename(); ?>?action=excel&AMP;id=<?php echo $id; ?>"><img src="images/excel.png"></a>
                                
                         <span style="float: right;"><a target="_blank" href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                           
                        <?php include('class/esm.php'); ?>
                        <div class="row" id="printablediv">
                        
                        
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                                   <div>
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Vehicle Request"); ?>
                                    </div>
                            <?php
                            $datas=$obj->SelectAllByID($table,array("id"=>$id));
                            foreach ($datas as $data):
                                
                            ?>
                          <h3>Serial #</h3>
                            <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                              <tr>
                                <td valign='top'><strong>Date : </strong><?php echo $data->date; ?></td>
                                <td valign='top'>&nbsp;</td>
                                <td width='214' valign='top'>&nbsp;</td>
                                <td width='345' valign='top'><strong>Signature : </strong> </td>
                                </tr>
                              <tr>
                                <td valign='top' height='27'><strong>Person Requesting : </strong> <?php echo $obj->SelectAllByVal("employee","id",$data->emplid,"name"); ?></td>
                                <td valign='top' height='27'><strong>Passengers Quantity :</strong> <?php echo $data->passengerquantity;  ?></td>
                                <td colspan='2' valign='top'><strong>Passengers Name :</strong> <?php echo $data->passengernames;  ?></td>
                              </tr>
                              <tr>
                                <td width='277' valign='top'><strong>Designation :</strong> <?php echo $obj->emp_designation($data->emplid); ?></td>
                                <td width='237' valign='top'><strong>Phone : </strong> <?php echo $data->phone;  ?></td>
                                <td valign='top'><strong>Budget Code :</strong> <?php echo $data->budgetcode;  ?></td>
                                <td valign='top'><strong>Budget Holder : </strong> <?php echo $data->budgetholder;  ?></td>
                              </tr>
                              <tr>
                                <td valign='top' height='29'><strong>Type of Travel : </strong></td>
                                <td colspan='3' valign='top'><?php echo $data->typeoftravel;  ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='21%' valign='top'><strong>Start Date - End Date</strong></td>
    <td width='12%' valign='top'><strong>Day</strong></td>
    <td width='12%' valign='top'><strong>Pick up point</strong></td>
    <td width='15%' valign='top'><strong>Destination</strong></td>
    <td width='21%' align='center' valign='top'><strong>Duration</strong></td>
  </tr>
  <tr>
    <td valign='top'><?php echo substr($data->startdatetime,0,10);  ?> - <?php echo substr($data->enddatetime,0,10);  ?></td>
    <td valign='top'><?php echo $obj->getDays(substr($data->startdatetime,0,10),substr($data->enddatetime,0,10))." Days"; ?></td>
    <td valign='top'><?php echo $data->startpoint; ?></td>
    <td valign='top'><?php echo $data->destination; ?></td>
    <td valign='top'>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='50%' height='31' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong> 
        <br />
        <?php echo $obj->SelectAllByVal("employee","id",$data->supervisor,"name");
    
    if($data->supervisor==$input_by)
    {
        if($data->status==1)
        {
        ?> 
        <a href="<?php echo $obj->filename(); ?>?action=approve&AMP;id=<?php echo $id; ?>"> <span class="label label-sm label-warning"> Approve</span></a>
        <a href="<?php echo $obj->filename(); ?>?action=reject&AMP;id=<?php echo $id; ?>"> <span class="label label-sm label-danger"> Reject</span></a>
      <?php 
        }
        else{
        ?>
      <span class="label label-sm label-success"> <?php echo $obj->emp_leave_status($data->status); ?></span>
      <?php
        }
    }
    else
    {
         echo $obj->emp_leave_status($data->status); 
    }
    ?></td>
    <td width='50%' valign='top' align='right'><strong>Date &amp; Time :</strong> <?php echo $data->startdatetime; ?></td>
    </tr>
  <tr>
    <td valign='top' height='31'><strong>Driver Information : </strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td valign='top' height='31'><strong>Vehicle Information :</strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan="2" valign='top'><strong>Special Instruction to Driver : </strong></td>
  </tr>
  <tr>
    <td height='31' colspan="2" valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan="2" valign='top'>Note : </td>
    </tr>
  </table>
  
  


                            
                           
                            <?php endforeach; ?>    								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
    </body>
</html>
