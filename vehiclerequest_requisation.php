<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Vehicle Request Info</a></li><li class='active'>Add Vehicle Request - Form</li>";
$table="requisition_vehicle";
if(isset($_POST['submit']))
{
        $insert=array("emplid"=>$input_by,"startdatetime"=>$_POST['startdatetime'],"enddatetime"=>$_POST['enddatetime'],"startpoint"=>$_POST['startpoint'],"destination"=>$_POST['destination'],"passengerquantity"=>$_POST['passenger'],"passengernames"=>$_POST['passengernames'],"purpose"=>$_POST['purposeoftravel'],"phone"=>$_POST['contactphoneno'],"typeoftravel"=>$_POST['typeoftravel'],"budgetcode"=>$_POST['budgetcode'],"budgetholder"=>$_POST['budgetholder'],"supervisor"=>$_POST['supervisor'],"date"=>  date('Y-m-d'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                    $notify_detail="Vehicle Request Has Benn Created";                   
                    $obj->make_notification($table,$notify_detail,$input_by,$_POST['supervisor'],"2");
                    
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            } 
}

if(isset($_POST['preview']))
{
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <form action="" name="save" method="POST">
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Vehicle Request Form-<?php echo date('Y'); ?>
                        <span style="float: right;"><a target="_blank" href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a>
                        <button class="btn btn-info" type="submit" name="submit">Send for Approval</button>
                        </span></h3>
                    <input type="hidden" name="startdatetime" value="<?php echo $_POST['startdatetime']; ?>">
                    <input type="hidden" name="enddatetime" value="<?php echo $_POST['enddatetime']; ?>">
                    <input type="hidden" name="startpoint" value="<?php echo $_POST['startpoint']; ?>">
                    <input type="hidden" name="destination" value="<?php echo $_POST['destination']; ?>">
                    <input type="hidden" name="passenger" value="<?php echo $_POST['passenger']; ?>">
                    <input type="hidden" name="passengernames" value="<?php echo $_POST['passengernames']; ?>">
                    <input type="hidden" name="purposeoftravel" value="<?php echo $_POST['purposeoftravel']; ?>">
                    <input type="hidden" name="contactphoneno" value="<?php echo $_POST['contactphoneno']; ?>">
                    <input type="hidden" name="typeoftravel" value="<?php echo $_POST['typeoftravel']; ?>">
                    <input type="hidden" name="budgetcode" value="<?php echo $_POST['budgetcode']; ?>">
                    <input type="hidden" name="budgetholder" value="<?php echo $_POST['budgetholder']; ?>">
                    <input type="hidden" name="supervisor" value="<?php echo $_POST['supervisor']; ?>">
                        <?php include('class/esm.php'); ?>
                        <div class="row" id="printablediv">
                        
                        
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                                   <div>
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Preview Vehicle Request"); ?>
                                    </div>
                            
                          <h3>Serial #</h3>
                            <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                              <tr>
                                <td valign='top'><strong>Date : </strong><?php echo date('Y-m-d'); ?></td>
                                <td valign='top'>&nbsp;</td>
                                <td width='214' valign='top'>&nbsp;</td>
                                <td width='345' valign='top'><strong>Signature : </strong> </td>
                                </tr>
                              <tr>
                                <td valign='top' height='27'><strong>Person Requesting : </strong> <?php echo $obj->SelectAllByVal("employee","id",$input_by,"name"); ?></td>
                                <td valign='top' height='27'><strong>Passengers Quantity :</strong> <?php echo $_POST['passenger'];  ?></td>
                                <td colspan='2' valign='top'><strong>Passengers Name :</strong> <?php echo $_POST['passengernames'];  ?></td>
                              </tr>
                              <tr>
                                <td width='277' valign='top'><strong>Designation :</strong> <?php echo $obj->emp_designation($input_by); ?></td>
                                <td width='237' valign='top'><strong>Phone : </strong> <?php echo $_POST['contactphoneno'];  ?></td>
                                <td valign='top'><strong>Budget Code :</strong> <?php echo $_POST['budgetcode'];  ?></td>
                                <td valign='top'><strong>Budget Holder : </strong> <?php echo $_POST['budgetholder'];  ?></td>
                              </tr>
                              <tr>
                                <td valign='top' height='29'><strong>Type of Travel : </strong></td>
                                <td colspan='3' valign='top'><?php echo $_POST['typeoftravel'];  ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='21%' valign='top'><strong>Start Date - End Date</strong></td>
    <td width='12%' valign='top'><strong>Day</strong></td>
    <td width='12%' valign='top'><strong>Pick up point</strong></td>
    <td width='15%' valign='top'><strong>Destination</strong></td>
    <td width='21%' align='center' valign='top'><strong>Duration</strong></td>
  </tr>
  <tr>
    <td valign='top'><?php echo substr($_POST['startdatetime'],0,10);  ?> - <?php echo substr($_POST['enddatetime'],0,10);  ?></td>
    <td valign='top'><?php echo $obj->getDays(substr($_POST['startdatetime'],0,10),substr($_POST['enddatetime'],0,10))." Days"; ?></td>
    <td valign='top'><?php echo $_POST['startpoint']; ?></td>
    <td valign='top'><?php echo $_POST['destination']; ?></td>
    <td valign='top'>&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='50%' height='31' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong> 
        <br />
        <?php echo $obj->SelectAllByVal("employee","id",$_POST['supervisor'],"name"); ?></td>
    <td width='50%' valign='top' align='right'><strong>Date &amp; Time :</strong> <?php echo $_POST['startdatetime']; ?></td>
    </tr>
  <tr>
    <td valign='top' height='31'><strong>Driver Information : </strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td valign='top' height='31'><strong>Vehicle Information :</strong></td>
    <td valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan="2" valign='top'><strong>Special Instruction to Driver : </strong></td>
  </tr>
  <tr>
    <td height='31' colspan="2" valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan="2" valign='top'>Note : </td>
    </tr>
  </table>

                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>
</form>

                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
                
    </body>
</html>    
    <?php
        exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
<script>
function showUser(str)
{
    if (str=="")
      {
      document.getElementById("txtHint").innerHTML="";
      return;
      }
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
      }
    startdate = document.getElementById("datetimepicker").value;
    enddate = document.getElementById("datetimepicker_1").value;
    xmlhttp.open("GET","datetimecount.php?q="+str+"&sdate="+startdate+"&edate="+enddate,true);
    xmlhttp.send();
}
</script>
<link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Vehicle Request - Form 
                                <a target="_blank" style="float: right;" href="vehiclerequest_requisitionlist.php" class="btn btn-success">Vehicle Request List</a>
                            </h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Date From <font color="green">*</font> </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control" name="startdatetime" value="<?php echo date('Y-m-d H:i'); ?>" id="datetimepicker" type="text" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>

                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> Date To <font color="green">*</font> </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control" name="enddatetime"  value="2014-03-15 05:06" id="datetimepicker_1" type="text" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Point <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="startpoint" class="col-xs-10 col-sm-3">
                                    </div>
                                </div> 
                                                           
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Destination <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="destination" class="col-xs-10 col-sm-3">
                                    </div>
                                </div> 
                                                                                          
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> No. of Passenger <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="passenger" class="col-xs-10 col-sm-1">
                                    </div>
                                </div> 
                                                                                                                          
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Passenger Name(s) <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="passengernames" class="col-xs-10 col-sm-10">
                                    </div>
                                </div> 
                                                                                                                                                           
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Purpose of Travel <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="purposeoftravel" class="col-xs-10 col-sm-8">
                                    </div>
                                </div> 
                                                                                                                                                                                             
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact Phone No. <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="contactphoneno" class="col-xs-10 col-sm-3">
                                    </div>
                                </div> 
                                                                                                                                                                                                                             
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type of Travel <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="typeoftravel" class="col-xs-10 col-sm-4">
                                    </div>
                                </div> 
                                
                                                                                                                                                                                                                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Budget Code <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="budgetcode" class="col-xs-10 col-sm-4">
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Budget Holder <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="budgetholder" class="col-xs-10 col-sm-4">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Super Visor <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="supervisor" class="col-xs-10 col-sm-4">
                                            <?php 
                                            $empsql=$obj->SelectAllNotMe("employee",array("id"=>$input_by));
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>                                
                                
                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-danger" type="submit" name="preview"><i class="icon-zoom-in"></i>Preview</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn btn-info" type="submit" name="submit">Send for Approval</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
                <script src="./jquery.datetimepicker.js"></script>
                <script>

                $('#datetimepicker').datetimepicker();
                $('#datetimepicker').datetimepicker({value:'2014-04-15 05:03',step:10});
                
                $('#datetimepicker_1').datetimepicker();
                $('#datetimepicker_1').datetimepicker({value:'2014-04-15 07:03',step:10});

                </script>
    </body>
</html>
