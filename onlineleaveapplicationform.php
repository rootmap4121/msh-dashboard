<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Online Leave Application Info</a></li><li class='active'>Add Online Leave Application Form</li>";
$table="requisation_leaveapplication_form";
if(isset($_POST['submit']))
{
    if(!empty($_POST['supervisor']))
    {
        $insert=array("emplid"=>$input_by,"supervisor"=>$_POST['supervisor'],"leave_type"=>$_POST['leave_type'],"description"=>$_POST['description'],"datequantity"=>$obj->getDays($_POST['startdate'], $_POST['enddate']),"startdate"=>$_POST['startdate'],"enddate"=>$_POST['enddate'],"startdatetime"=>$_POST['startdatetime'],"enddatetime"=>$_POST['enddatetime'],"phone"=>$_POST['phone'],"incharge"=>$_POST['incharge'],"date"=>  date('Y-m-d'),"year"=>  date('Y'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                    $notify_detail="Leave Application Request Has Benn Created";                   
                    $obj->make_notification($table,$notify_detail,$input_by,$_POST['supervisor'],"4");
                    
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
            }
        } 
}


if(isset($_POST['preview']))
{
    ?>
    
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <form action="" name="save" method="POST">
        <?php include('class/header.php'); ?>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Individual Leave Application Form 
                         <span style="float: right;"><a href="#" target="_blank" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a>
                         &nbsp; &nbsp; &nbsp;
                             <button class="btn btn-info" type="submit" name="submit">Send For Approval</button>
                                        
                         </span></h3>
                           
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row" id="printablediv">
   
                    
                    <input type="hidden" name="supervisor" value="<?php echo $_POST['supervisor']; ?>">
                    <input type="hidden" name="leave_type" value="<?php echo $_POST['leave_type']; ?>">
                    <input type="hidden" name="description" value="<?php echo $_POST['description']; ?>">
                    <input type="hidden" name="startdate" value="<?php echo $_POST['startdate']; ?>">
                    <input type="hidden" name="enddate" value="<?php echo $_POST['enddate']; ?>">
                    <input type="hidden" name="startdatetime" value="<?php echo $_POST['startdatetime']; ?>">
                    <input type="hidden" name="enddatetime" value="<?php echo $_POST['enddatetime']; ?>">
                    <input type="hidden" name="phone" value="<?php echo $_POST['phone']; ?>">
                    <input type="hidden" name="incharge" value="<?php echo $_POST['incharge']; ?>">
                    
                    
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            <div>
                                
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Preview Leave Application"); ?>
                                
                            </div>
                          <h3>A. PERSONAL INFORMATION (For Applicant)</h3>
                            <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                              <tr>
                                <td valign='top' colspan='3'><strong>Name : </strong><?php echo $obj->SelectAllByVal("employee","id",$input_by,"name");
                                    ?></td>
                                <td valign='top' colspan='2'><strong>Position : </strong>
                                  <?php 
                                    $empsql=$obj->SelectAllByID("employee",array("id"=>$input_by));
                                    foreach($empsql as $emp):
										$empsql=$obj->SelectAllByID("designation",array("id"=>$emp->designation));
										foreach($empsql as $emp):
											echo $emp->name;
										endforeach;
                                    endforeach;								  

                                    
                                    
                                    ?>
                                </td>
                                </tr>
                              <tr>
                                <td valign='top' height='38' colspan='2'><strong>ID Number : </strong> <?php echo $input_by; ?></td>
                                <td valign='top' colspan='3'><strong>Purpose/Reason : </strong> <?php echo $_POST['description']; ?></td>
                                </tr>
                              <tr>
                                <td valign='top' width='335'><strong>Requested Leave </strong> </td>
                                <td valign='top' colspan='2'><strong>From : </strong> <?php echo $_POST['startdate']; ?></td>
                                <td valign='top' width='229'><strong>To :</strong> <?php echo $_POST['enddate']; ?></td>
                                <td valign='top' width='228'><strong>No. of Day/Hour: </strong>  <?php echo $obj->emp_leave_quantity_new($input_by); ?> Day</td>
                              </tr>
                              <tr>
                                <td valign='top'><strong>Type of Leave :</strong></td>
                                <td valign='top' colspan='4'><?php echo $obj->SelectAllByVal("leave_type","id",$_POST['leave_type'],"name");
                                    ?></td>
                              </tr>
                              <tr>
                                <td valign='top' height='42'><strong>Contact Address During <br>Leave with telephone No.</strong></td>
                                <td valign='top' colspan='4'><?php echo $_POST['phone'];  ?></td>
                                </tr>
                              <tr>
                                <td valign='top' colspan='4'><strong>Signature of the Application :</strong></td>
                                <td valign='top'><strong>Date :</strong><?php echo date('Y-m-d');  ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>B. OFFICIAL USE ONLY : </h3>
                            
<table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td valign='top' width='11%'><strong>Type of Leave</strong></td>
    <td valign='top' width='19%'><strong>Available Balance This Year (A)</strong></td>
    <td valign='top' width='18%'><strong>Leave Requested this time (B)</strong></td>
    <td valign='top' width='16%'><strong>Leave Available/Taken (C)</strong></td>
    <td valign='top' width='19%'><strong>Total Taken as of Date D=(B+C)</strong></td>
    <td valign='top' width='17%'><strong>Balance Till Today E=(A-D)</strong></td>
  </tr>
  
  <tr>
    <td valign='top'><?php echo $obj->SelectAllByVal("leave_type","id",$_POST['leave_type'],"name");
				?></td>
    <td valign='top'><?php echo $obj->leave_quantity($_POST['leave_type']); ?></td>
    <td valign='top'><?php echo $obj->emp_leave_quantity_new($input_by); ?></td>
    <td valign='top'><?php echo $obj->emp_leave_quantity($input_by); ?></td>
    <td valign='top'><?php echo $obj->emp_leave_quantity($input_by)+$obj->emp_leave_quantity_new($input_by); ?></td>
    <td valign='top'><?php echo $obj->leave_quantity($_POST['leave_type'])-($obj->emp_leave_quantity($input_by)+$obj->emp_leave_quantity_new($input_by)); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 
</table>

<br>

<h3>C. APPROVAL : </h3>
                            
<table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td valign='top' height='80'><strong>Recommended By ( Supervisor ) :</strong>
    <br /><?php echo $obj->SelectAllByVal("employee","id",$_POST['supervisor'],"name"); ?>
    </td>
    <td valign='top'><strong>Reviewed By ( Admin ) :</strong> <?php echo $obj->SelectAllByVal("employee","status","2","name");  ?></td>
    <td valign='top'><strong>Approved By ( Country Project Director ) :</strong></td>
    </tr>
  </table>
  
  
  <br>

<h3>D. GUIDELINES : </h3>
                            
<table width='100%' border='0'>
  <tr>
    <td height='80'>1. This form must be filled in by all staff &amp; must support any absense from work.
    <br> 2. For annual leave / any other planned leave, please apply 2 week's in advance. 
    <br>
    3. For Casual leave please apply minimum 2 days before. 
    <br>
    4. Sick leave for more than 2 days should be supported by a physician's certificate. 
    <br>
    5. Please delegate someone who will perform your current and most urgent desk work on behalf of you while on leave.</td>
    </tr>
  </table>								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>
            </form>
		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
    </body>
</html>
    

   <?php
       exit();
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
<script>
function showUser(str)
{
    if (str=="")
      {
      document.getElementById("txtHint").innerHTML="";
      return;
      }
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
      }
    startdate = document.getElementById("startdate").value;
    enddate = document.getElementById("enddate").value;
    xmlhttp.open("GET","datecount.php?q="+str+"&sdate="+startdate+"&edate="+enddate,true);
    xmlhttp.send();
}
</script>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Leave Application Form 
                            <a target="_blank" style="float: right;" href="onlineleaveapplicationform_list.php" class="btn btn-success">Leave List</a>
                            </h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Type </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="leave_type" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("leave_type");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> From </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="startdate" value="<?php echo date('Y-m-d'); ?>" id="startdate" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                                    <select name="startdatetime">
                                                        <option value="1">First Half</option>
                                                        <option value="2">2nd Half</option>
                                                    </select>
                                            </div>

                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> To </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control date-picker"  onchange="showUser(this.value)"  name="enddate" value="<?php echo date('Y-m-d'); ?>" id="enddate" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                                   <select name="enddatetime">
                                                        <option value="1">First Half</option>
                                                        <option value="2">2nd Half</option>
                                                    </select>
                                            </div>
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Counts </label>

                                    <div class="col-sm-9">
                                        <div id="txtHint" style="margin-top: 4px; color: #f00; border: 1px solid;" name="datequantity" class="col-xs-6 col-sm-2">0 Days</div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Super Visor<font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="supervisor" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("employee");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Incharge<font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="incharge" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("employee");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>                                 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="phone" class="col-xs-10 col-sm-5"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Reason </label>
                                    <div class="col-sm-9">
                                        <textarea type="text" id="form-field-1" name="description" class="col-xs-10 col-sm-5"></textarea>
                                    </div>
                                </div> 
                                
                                
                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-danger" type="submit" name="preview"><i class="icon-zoom-in"></i>Preview</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn btn-info" type="submit" name="submit">Send For Approval</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
                jQuery(function($) 
                {
                    $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
                             $(this).prev().focus();
                     });
                     $('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
                             $(this).next().focus();
                     });
                     

                })               
		</script>
    </body>
</html>
