<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Info</a></li><li class='active'>Product List</li>";
$table="order_detail";
$table2="order";
if (isset ($_GET['del'])=="delete") {
                    $delarray=array("orderid"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        
                        if($obj->delete($table2,$delarray)==1):
                            $errmsg_arr[]= 'Successfully Saved';
                            $errflag = true;
                            if ($errflag) 
                            {
                                $_SESSION['SMSG_ARR'] = $errmsg_arr;
                                session_write_close();
                                header("location: ./".$obj->filename());
                                exit();
                            }
                        endif;
                     
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          
                                <h3 class="header smaller lighter blue">Order List
                                        
                             
                                <span style="margin-left: 20px;"><a  href="#modal-tablesearch" role="button" data-toggle="modal" class="green"><i class="icon-camera-retro"></i> Search in Multiple Dates</a></span> <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span>
                               </h3>
                            
                            <div id="modal-tablesearch" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Report Using Multiple Date
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <br>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="strdate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ending Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="enddate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>                                      


                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="search"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->

                                <div class="row">

                                    <div class="col-xs-12">
                                        
                                        <div>
                                            <?php echo $obj->company_report_logo(); ?>
                                            <?php echo $obj->company_report_head(); ?>
                                            <?php echo $obj->company_report_name("Product Order List"); ?>
                                   
                                        </div>

                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Order Id</th>
                                                        <th>Total Price</th>
                                                        <th>Supplier</th>
                                                        <th>View </th>
                                                        <th>Stock In </th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                    if(!isset($_POST['search']))
                                                    {
                                                        $data=$obj->SelectAllorderBy($table);
                                                    }
                                                    else
                                                    {
                                                        $data=$obj->SelectAll_ddate($table,"date",$_POST['strdate'],$_POST['enddate']); 
                                                    }
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->orderid; ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->totalprice; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php $sup=$obj->SelectAllByID("supplier",array("id"=>$row->sid)); foreach($sup as $su): echo $su->name; endforeach;  ?></span></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="orderdetail.php?id=<?php echo $row->id; ?>"  class="green"><i class="icon-desktop bigger-130"></i> View</a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="orange" href="orderstockin.php?id=<?php echo $row->id; ?>"><i class="icon-exchange bigger-130"></i> Stock In</a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->orderid; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php 
                                                 $x++; endforeach;
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                   <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			                             $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>
    </body>
</html>
