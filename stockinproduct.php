<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Stock Detail</a></li><li class='active'>Product Stock In</li>";
$table="order_detail";
$table2="order";
if (isset ($_GET['del'])=="delete") {
                    $delarray=array("orderid"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        
                        if($obj->delete($table2,$delarray)==1):
                            $errmsg_arr[]= 'Successfully Saved';
                            $errflag = true;
                            if ($errflag) 
                            {
                                $_SESSION['SMSG_ARR'] = $errmsg_arr;
                                session_write_close();
                                header("location: ./".$obj->filename());
                                exit();
                            }
                        endif;
                     
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>
    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Stock In</h3>
                                        <div class="table-header">
                                            Results for "Total Order&rsquo;s" (<?php echo $obj->totalrows($table); ?>)
                                        </div>

                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Order Id</th>
                                                        <th>Order Placed By</th>
                                                        <th>Total Price</th>
                                                        <th>Supplier</th>
                                                        <th>Stock In </th>
                                                        <th>Order date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->orderid; ?></td>
                                                            <td>
                                                                <?php 
                                                                $getmp=$obj->SelectAllByID("employee",array("id"=>$row->emplid));
                                                                foreach($getmp as $mp):
                                                                    echo $mp->name;
                                                                endforeach;
                                                                
                                                                ?>
                                                            </td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->totalprice; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php $sup=$obj->SelectAllByID("supplier",array("id"=>$row->sid)); foreach($sup as $su): echo $su->name; endforeach;  ?></span></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="orange" href="orderstockin.php?id=<?php echo $row->id; ?>"><i class="icon-exchange bigger-130"></i> Stock In</a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <?php echo $row->date; ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php $x++; endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
