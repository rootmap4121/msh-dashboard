<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>User Info</a></li><li class='active'>Admin List</li>";
$table="employee";
if (isset ($_GET['action'])=="update") 
{
    $delarray=array("id"=>$_GET['id'],"status"=>2);
    if($obj->update($table,$delarray)==1)
    { 
        $errmsg_arr[]= 'Successfully Updated';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }

    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <?php //echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php //include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Employee List
                                            <a href="<?php echo $obj->filename(); ?>?action=pdf"><img src="images/pdf.png"></a> 
                                            <a href="<?php echo $obj->filename(); ?>?action=excel"><img src="images/excel.png"></a>
                                
                                            <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print Employee List</a></span>
                                        </h3>

                                        <div class="table-responsive" id="printablediv">
                                            <div>
                                
                                            <?php echo $obj->company_report_logo(); ?>
                                            <?php echo $obj->company_report_head(); ?>
                                            <?php echo $obj->company_report_name("Employee List"); ?>

                                            </div>
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th> Staff ID </th>
                                                        <th> Full Name </th>
                                                        <th> Ext. Number </th>
                                                        <th> Designation </th>
                                                        <th> Gender </th>
                                                        <th> Blood Group</th>
                                                        <th> Username </th>
                                                        <th colspan="2"> </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                $data=$obj->SelectAll($table);
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $row->id; ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->staff_id; ?></span></td>
                                                            <td><span class="label label-sm label-primary"><?php echo $row->name; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->ext_number; ?></span></td>
                                                            <td>
                                                            <?php  
                                                            $qdes=$obj->SelectAllByID("designation",array("id"=>$row->designation));
                                                            if(!empty($qdes))
                                                            foreach ($qdes as $dd):
                                                              echo $dd->name;
                                                            endforeach;
                                                            ?></td>
                                                            <td><span class="label label-sm label-warning"><?php echo $obj->sex($row->gender); ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->blood_group; ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php echo $row->username; ?></span></td>
                                                            
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-pencil bigger-130"></i></a>
                                                                </div>
								<div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Results for "Latest Registered Domains
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="name" value="<?php echo $row->name; ?>" class="col-xs-10 col-sm-5" />
                                                                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ext.Number </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="ext_number" value="<?php echo $row->ext_number; ?>" class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>
                                                                                            


                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Designation </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <select class="width-10" name="designation">
                                                                                                        <option value="">Select Designation</option>
                                                                                                        <?php  $datas=$obj->SelectAllorderBy("designation"); foreach ($datas as $rows): ?>
                                                                                                        <option <?php if($rows->id==$row->designation): ?> selected="selected" <?php endif; ?> value="<?php echo $rows->id; ?>"><?php echo $rows->name; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Branch </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <select class="width-10" name="branch">
                                                                                                        <option value="">Select Branch</option>
                                                                                                        <?php  $data=$obj->SelectAllorderBy("branch"); foreach ($data as $rowss): ?>
                                                                                                            <option <?php if($rowss->id==$row->branch): ?> selected="selected" <?php endif; ?>  value="<?php echo $rowss->id; ?>"><?php echo $rowss->name; ?></option>
                                                                                                        <?php endforeach; ?>
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Gender </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <select class="width-10" name="gender">
                                                                                                        <option value="">Select Sex</option>
                                                                                                        <option  <?php if($row->gender==1): ?> selected="selected" <?php endif; ?>  value="1">Male</option>
                                                                                                        <option <?php if($row->gender==2): ?> selected="selected" <?php endif; ?> value="2">Female</option>
                                                                                                     </select>
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Date of Birth </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="dob" placeholder="Year-Month-Date" value="<?php echo $row->dob; ?>" class="col-xs-10 col-sm-5" />
                                                                    
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact Number </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="contact" value="<?php echo $row->contactnumber; ?>"  placeholder="Contact Number " class="col-xs-10 col-sm-10" />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="address" value="<?php echo $row->address; ?>" placeholder="Address " class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Blood Group </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <select class="width-10" name="blood_group">
                                                                                                        <option <?php if($row->blood_group=="A+"): ?> selected="selected" <?php endif; ?> value="A+">A+</option>
                                                                                                        <option <?php if($row->blood_group=="AB+"): ?> selected="selected" <?php endif; ?> value="AB+">AB+</option>
                                                                                                        <option <?php if($row->blood_group=="B+"): ?> selected="selected" <?php endif; ?> value="B+">B+</option>
                                                                                                        <option <?php if($row->blood_group=="O+"): ?> selected="selected" <?php endif; ?> value="O+">O+</option>
                                                                                                        <option <?php if($row->blood_group=="A-"): ?> selected="selected" <?php endif; ?> value="A-">A-</option>
                                                                                                        <option <?php if($row->blood_group=="AB-"): ?> selected="selected" <?php endif; ?> value="AB-">AB-</option>
                                                                                                        <option <?php if($row->blood_group=="B-"): ?> selected="selected" <?php endif; ?> value="B-">B-</option>
                                                                                                        <option <?php if($row->blood_group=="O-"): ?> selected="selected" <?php endif; ?> value="O-">O-</option>
                                                                                                    </select>                                                                                                 
                                                                                                </div>
                                                                                            </div>

                                                                                            
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Staff ID </label>

                                                                                                <div class="col-sm-9">
                                                                                                        <input type="text" id="form-field-1" name="staff_id" value="<?php echo $row->staff_id; ?>" placeholder="Staff Id " class="col-xs-10 col-sm-5" />
                                                                                                
                                                                                                </div>
                                                                                            </div>







                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->


                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?action=update&AMP;id=<?php echo $row->id; ?>"><i class="icon-edit bigger-130"></i> Update </a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
