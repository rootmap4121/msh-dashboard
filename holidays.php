<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Holidays Info</a></li><li class='active'>Holidays List</li>";
$table="holiday_news";
if(isset($_POST['submit']))
{
    if(!empty($_POST['news']))
    {
        $exist_array=array("datepr"=>$_POST['datepr']);
        $month=substr($_POST['datepr'],5,2);
        $year=substr($_POST['datepr'],0,4);
        $insert=array("news"=>$_POST['news'],"holiday_date"=>$_POST['datepr'],"month"=>$month,"year"=>$year,"date"=>date('Y-m-d'),"status"=>1);
        if($obj->exists($table,$exist_array)==1)
        {
            $errmsg_arr[]= 'Already Exists';
            $errflag = true;
            if ($errflag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./".$obj->filename());
                exit();
            }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                $errmsg_arr[]= 'Successfully Saved';
                $errflag = true;
                if ($errflag) 
                {
                    $_SESSION['SMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./".$obj->filename());
                    exit();
                }
            }
            else 
             {
                $errmsg_arr[]= 'Failed';
                $errflag = true;
                if ($errflag) {
                    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./".$obj->filename());
                    exit();
                }
            }
        }
    }
 else {
            $errmsg_arr[]= 'Failed';
            $errflag = true;
            if ($errflag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./".$obj->filename());
                exit();
            }
    }
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $month=substr($_POST['datepr'],5,2);
                    $year=substr($_POST['datepr'],0,4);
                    $updatearray=array("id"=>$_POST['id'],"news"=>$_POST['news'],"holiday_date"=>$_POST['datepr'],"month"=>$month,"year"=>$year);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">This Month Holiday</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="branchadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Holiday Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="news" placeholder="Holidays news" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Select Holidays Date </label>

                                    <div class="col-xs-6 col-sm-2">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="datepr" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

								<div class="hr hr-18 dotted hr-double"></div>           

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Holidays List</h3>
                                        <div class="table-responsive">
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Holidays News</th>
                                                        <th>Holidays Date</th>
                                                        <th>Month</th>
                                                        <th>Year</th>
                                                        <th>Edit </th>
                                                        <th>Delete</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                if(!empty($data))
                                                    $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->news; ?></td>
                                                            <td><span class="label label-sm label-warning"><?php echo $row->holiday_date; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->month; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->year; ?></span></td>
                                                            
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-pencil bigger-130"></i> Edit</a>
                                                                </div>
								<div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Results for "Latest Registered Domains
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Holiday Name </label>

                                                                                                <div class="col-sm-8">
                                                                                                    <input type="text" id="form-field-1" name="news" placeholder="Holidays news" value="<?php echo $row->news; ?>" class="col-xs-10 col-sm-8" />
                                                                                                    <input type="hidden" id="form-field-1" name="id" placeholder="Holidays news" value="<?php echo $row->id; ?>" class="col-xs-10 col-sm-8" />
                                                                                                </div>
                                                                                            </div>

                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Holidays Date </label>

                                                                                                <div class="col-xs-6 col-sm-5">
                                                                                                        <div class="input-group">
                                                                                                            <input class="form-control date-picker" name="datepr" value="<?php echo $row->holiday_date; ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                                                                                <span class="input-group-addon">
                                                                                                                        <i class="icon-calendar bigger-110"></i>
                                                                                                                </span>
                                                                                                        </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->


                                                            </td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach;
                                                }
                                                else 
                                                {
                                                   echo "No Data"; 
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>

                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
                                
                             $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
        
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
			})
                        
		</script>
    </body>
</html>
