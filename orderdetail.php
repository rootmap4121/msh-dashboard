<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Order </a></li><li class='active'>Product Order Form</li>";
$table="order_detail";
$table2="order";
if(isset($_POST['submit']))
{
            
            $insert=array("orderid"=>$_POST['orderid'],"supid"=>$_POST['supid'],"emplid"=>$input_by,"note"=>$_POST['note'],"amount"=>$_POST['amount'],"date"=>  date('Y-m-d'),"status"=>1);
            if($obj->insert("payment",$insert)==1)
            {
                if($obj->amount_incre("supplier",array("id"=>$_POST['supid']),"paid",$_POST['amount'])==1)
                {
                    $errmsg_arr[]= 'Successfully Added, and Supplier Due Amount Has been Incresed';
                    $errflag = true;
                    if ($errflag) 
                    {
                    $_SESSION['SMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./".$obj->filename()."?id=".$_POST['id']);
                    exit();
                    }
                }
                else 
                {
                    $errmsg_arr[]= 'Failed Please Try Again';
                    $errflag = true;
                    if ($errflag) 
                    {
                    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./".$obj->filename()."?id=".$_POST['id']);
                    exit();
                    }                    
                }
            }
            else 
            {
                $errmsg_arr[]= 'Failed Please Try Again';
                $errflag = true;
                if ($errflag) 
                {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./".$obj->filename()."?id=".$_POST['id']);
                exit();
                }                
            }  
}
extract($_GET);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->

                            
                            
                            
                            
                            
     <form class="form-horizontal" name="addproduct" role="form" action="" method="POST">                       
     <?php $od=$obj->SelectAllByID("order_detail",array("id"=>$id)); foreach($od as $order_detail): ?>
    <div class="space-6"></div>

    <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                    <div class="widget-box transparent invoice-box">
                            <div class="widget-header widget-header-large">
                                    <h3 class="grey lighter pull-left position-relative">
                                            <i class="icon-leaf green"></i>
                                            Product Order Detail
                                    </h3>



                                    <div class="widget-toolbar hidden-480">
                                        <a href="#"  onclick="javascript:printDiv('printablediv')">
                                                    <i class="icon-print"></i>
                                            </a>
                                            <button class="btn btn-info" type="submit" name="submit"><i class="icon-fighter-jet bigger-110"></i>Save</button>
                                            
                                    </div>
                            </div>

                        <div class="widget-body" id="printablediv">
                                    <div class="widget-main padding-24">
                                            <div class="row">
                                                    <div class="col-sm-6">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
                                                                            <b>Company Info</b>
                                                                    </div>
                                                            </div>

                                                            <div class="row">
                                                                    <ul class="list-unstyled spaced">
                                                                            <li><i class="icon-caret-right blue"></i>
                                                                               <span class="invoice-info-label">Invoice:</span>
                                                                                <span class="red">#<?php echo $order_detail->orderid; ?> </span>
                                                                                <input type="hidden" value="<?php echo $order_detail->orderid; ?>" name="orderid">
                                                                                <input type="hidden" value="<?php echo $id; ?>" name="id">

                                                                            </li>
                                                                            <li>
                                                                                                                                                                <i class="icon-caret-right blue"></i>
                                                                                <span class="invoice-info-label">Date:</span>
                                                                                <span class="blue"><?php echo $order_detail->date; ?></span>
                                                                            </li>
                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    <input type="hidden" value="<?php echo $order_detail->emplid; ?>" name="emplid">
                                                                                    Admin Name : <?php $admin=$obj->SelectAllByID("employee",array("id"=>$order_detail->emplid)); foreach ($admin as $ad): echo $ad->name;  endforeach;  ?>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->

                                                    <div class="col-sm-6">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
                                                                            <b>Supplier Info</b>
                                                                    </div>
                                                            </div>

                                                            <div>
                                                                    <ul class="list-unstyled  spaced">
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    <input type="hidden" value="<?php echo $order_detail->sid; ?>" name="supid">
                                                                                    Supplier Name : <?php $supplier=$obj->SelectAllByID("supplier",array("id"=>$order_detail->sid)); foreach($supplier as $sup):  ?>
                                                                                        <?php  echo $sup->name;  ?>
                                                                                        <?php endforeach;  ?>
                                                                            </li>
                                                                            
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Contact Number : <?php $supplier=$obj->SelectAllByID("supplier",array("id"=>$order_detail->sid)); foreach($supplier as $sup):  ?>
                                                                                        <?php  echo $sup->contactnumber;  ?>
                                                                                        <?php endforeach;  ?>
                                                                            </li>
                                                                            
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Total Order Price : <?php echo $order_detail->totalprice;  ?>
                                                                            </li>                                                                            
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->
                                            </div><!-- row -->

                                            <div class="space"></div>

                                            <div>
                                                    <table class="table table-striped table-bordered">
                                                            <thead>
                                                                    <tr>
                                                                            <th class="center">#</th>
                                                                            <th>Product</th>
                                                                            <th class="hidden-xs">Description</th>
                                                                            <th class="hidden-480">Quantity</th>
                                                                            <th class="hidden-480">Unite Price</th>
                                                                            <th class="hidden-480">Product Price</th>
                                                                    </tr>
                                                            </thead>

                                                            <tbody>
                           <?php $i=1; $ord=$obj->SelectAllByID("order",array("orderid"=>$order_detail->orderid)); foreach($ord as $order): ?>
                            <tr>
                                    <td class="center"><?php echo $i; ?></td>
                                    <td>
                                        <?php $pro=$obj->SelectAllByID("product",array("id"=>$order->pid)); foreach($pro as $product): echo $product->name; endforeach; ?>
                                    </td>
                                    <td class="hidden-xs"><?php echo $order->description; ?></td>
                                    <td class="hidden-480"><?php echo $order->quantity; ?></td>
                                    <td class="hidden-480"><?php echo $order->price; ?></td>
                                    <td class="hidden-480"><?php echo $order->totalprice; ?></td>
                            
                            </tr>
                            <?php $i++; endforeach; ?>

                                                                    
                                                            </tbody>
                                                    </table>
                                            </div>

                                            <div class="hr hr8 hr-single hr-single"></div>

                                            <div class="row">
                                                    <!--<div class="col-sm-5 pull-right">
                                                            <h4 class="pull-right">
                                                                    Total amount :
                                                                    <span class="red">$395</span>
                                                            </h4>
                                                    </div>-->
                                            </div>

                                              
                                        
                                     &nbsp; &nbsp; &nbsp; 
                                        
                                            <div class="hr hr8 hr-single hr-single"></div>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>
    
    <!-- PAGE CONTENT ENDS -->
    <div class="row">
        <div class="col-sm-12">
            <div class="space-6"></div>
                                             <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Total Payable </label>
                                                                            
                                                    <div class="col-sm-9">
                                                        <input type="text" value="<?php echo $order_detail->totalprice; ?>" id="form-field-1" name="total" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Current Payable </label>

                                                    <div class="col-sm-9">
                                                        <input type="text" id="form-field-1" name="totalprice" value="<?php $su=$obj->SelectAllByID("supplier",array("id"=>$order_detail->sid)); foreach($su as $ss): echo ($ss->amount)-($ss->paid); endforeach; ?>" placeholder="Current Payable" class="col-xs-10 col-sm-5" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Amount </label>

                                                    <div class="col-sm-9">
                                                        <input type="text" id="form-field-1" name="amount" placeholder="amount" class="col-xs-10 col-sm-5" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Paid Amount</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" id="form-field-1" name="paid" placeholder="Paid Amount"  value="<?php $su=$obj->SelectAllByID("supplier",array("id"=>$order_detail->sid)); foreach($su as $ss): echo $ss->paid; endforeach; ?>" class="col-xs-10 col-sm-5" />
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> payment Notes</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" id="form-field-1" name="note" placeholder="Make A Payment Note"   class="col-xs-10 col-sm-5" />
                                                    </div>
                                            </div>
                                            
        </div>
    </div>
                      
                            
     </form>           
                            
                            <div class="row">
                                
                                

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Payment History</h3>
                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Order ID</th>
                                                        <th>Employee Name</th>
                                                        <th>Amount</th>
                                                        <th>Note</th>
                                                        <th></th>
                                                        <th>Payment Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                $data=$obj->SelectAllByID("payment",array("orderid"=>$order_detail->orderid));
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->orderid; ?></td>
                                                            <td><?php $getemp=$obj->SelectAllByID("employee",array("id"=>$row->emplid)); foreach($getemp as $emp): echo $emp->name; endforeach; ?></td>
                                                            <td><?php  echo $row->amount; ?></td>
                                                            <td><?php  echo $row->note; ?></td>
                                                            <td>
                                                                
                                                            </td>
                                                            <td><?php  echo $row->date; ?></td>
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach;
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            
                            
                            
        <?php endforeach; ?>                          
                            
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                    <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
