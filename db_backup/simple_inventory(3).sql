-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2014 at 03:48 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `simple_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `assetproduct`
--

CREATE TABLE IF NOT EXISTS `assetproduct` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `assettype` varchar(20) DEFAULT NULL,
  `name` text,
  `msn` varchar(255) DEFAULT NULL,
  `sid` int(255) DEFAULT NULL,
  `datepr` date DEFAULT NULL,
  `bdtcost` varchar(255) DEFAULT NULL,
  `uscost` varchar(255) DEFAULT NULL,
  `dollarrate` varchar(255) DEFAULT NULL,
  `voucher_po` varchar(255) DEFAULT NULL,
  `itemofficeyn` varchar(5) DEFAULT NULL,
  `location` text,
  `itemcondition` int(5) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `warranty` varchar(255) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `lpi` varchar(255) DEFAULT NULL,
  `bywho` int(20) DEFAULT NULL,
  `remarks` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `assetproduct`
--

INSERT INTO `assetproduct` (`id`, `barcode`, `assettype`, `name`, `msn`, `sid`, `datepr`, `bdtcost`, `uscost`, `dollarrate`, `voucher_po`, `itemofficeyn`, `location`, `itemcondition`, `project`, `warranty`, `disposition`, `lpi`, `bywho`, `remarks`, `date`, `status`) VALUES
(2, '1234', 'CE', 'Pack Size', 'L007031', 4, '2014-02-11', 'L007031', 'L007031', 'L007031', 'L007031', 'Y', 'L007031', 2, '1', 'N/A', 'L007031', '2014-02-12', 13, 'L007031', '2014-02-12', 1),
(3, 'sdfsdfsdf', 'OF', 'dsfdsf', 'sdfdsfsdf', 4, '2014-02-10', '43', '34', '34', '32d', 'Y', 'dfdfdsf', 3, '3', '17 Years', 'dfdsfsdfdsf', '2014-02-12', 13, 'dfdsfdsf', '2014-02-12', 1),
(4, 'fghgfh', 'ME', 'hfghfg', 'fghgfh', 4, '2014-02-12', 'fghgfh', 'fghgf', 'hfghgfh', 'fhgfh', 'Y', 'fghgfh', 1, '1', 'N/A', 'fghfghfgh', '2014-02-12', 13, 'fghgfh', '2014-02-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `assettype`
--

CREATE TABLE IF NOT EXISTS `assettype` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `legend` varchar(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `assettype`
--

INSERT INTO `assettype` (`id`, `name`, `legend`, `date`, `status`) VALUES
(1, 'Computer Accesories', 'CA', '2014-02-17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `barcode`
--

CREATE TABLE IF NOT EXISTS `barcode` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `contact` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `name`, `location`, `contact`, `date`, `status`) VALUES
(6, 'AMS IT', '44/P,Kazi Bhaban,New Road ,Dhanmondi,Dhaka', 1927608261, '2014-01-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `date`, `status`) VALUES
(1, 'Technological', '2014-01-16', 1),
(2, 'Matirials', '2014-01-16', 1),
(3, 'Office Furniture', '2014-01-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `name`, `date`, `status`) VALUES
(10, 'MPO', '2014-01-14', 1),
(23, 'Manager', '2014-01-15', 1),
(24, 'Serviceholder', '2014-01-15', 1),
(25, 'Incharge', '2014-01-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contactnumber` int(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `joiningdate` date DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `designation`, `branch`, `gender`, `dob`, `contactnumber`, `address`, `photo`, `joiningdate`, `username`, `password`, `date`, `status`) VALUES
(13, 'amsit', '23', '6', '1', '2014-01-01', 1927608261, 'qq', '', '2012-02-17', 'amsit', '8139be3698a555383168ec998c0e65bf', '2014-02-09', 2),
(17, 'ARony', '25', '6', '1', '2013-02-02', 1927608261, 'qq', '', '1998-02-19', 'rony', '202cb962ac59075b964b07152d234b70', '2014-02-11', 1),
(20, 'Fahad Bhuyian', '25', '6', '1', '2012-01-03', 1927608261, 'qq', 'big_1392089486_facebook-like-button-johor-malaysia-july-icon-voting-system-used-to-rate-user-comments-low-shu-ching-hand-holding-32860018.jpg', '1998-02-19', 'fahad', 'bfd59291e825b5f2bbf1eb76569f8fe7', '2014-02-11', 1),
(21, 'admin', '25', '6', '1', '2014-02-02', 1927608261, '44/P,Kazi Bhavan,New Road Zigatola Dhanmondi Dhaka', 'big_1392676082_429069_1896296584092_681278035_n.jpg', '2014-01-01', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2014-02-17', 3),
(22, 'Emon', '24', '6', '1', '2014-01-02', 1927608261, '44/P,Kazi Bhavan,New Road Zigatola Dhanmondi Dhaka', 'big_1392686381_Bangla-News.jpg', '1996-02-18', 'emon', 'b8cc4edba5145d41f9da01d85f459aef', '2014-02-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fieldproduct`
--

CREATE TABLE IF NOT EXISTS `fieldproduct` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `assettype` varchar(20) DEFAULT NULL,
  `name` text,
  `quantity` varchar(255) NOT NULL,
  `msn` varchar(255) DEFAULT NULL,
  `sid` int(255) DEFAULT NULL,
  `datepr` date DEFAULT NULL,
  `bdtcost` varchar(255) DEFAULT NULL,
  `uscost` varchar(255) DEFAULT NULL,
  `dollarrate` varchar(255) DEFAULT NULL,
  `marketvalueperunit` varchar(255) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `remarks` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fieldproduct`
--

INSERT INTO `fieldproduct` (`id`, `barcode`, `assettype`, `name`, `quantity`, `msn`, `sid`, `datepr`, `bdtcost`, `uscost`, `dollarrate`, `marketvalueperunit`, `disposition`, `remarks`, `date`, `status`) VALUES
(1, '1234', 'OE', 'Pack Size', '100', 'L0070313', 4, '2014-02-03', 'L007031', 'L007031', 'L007031', '120', 'L007031', 'dfdsfdsf', '2014-02-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `field_inventory_order`
--

CREATE TABLE IF NOT EXISTS `field_inventory_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `sid` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `field_inventory_order`
--

INSERT INTO `field_inventory_order` (`id`, `orderid`, `pid`, `description`, `quantity`, `price`, `totalprice`, `emplid`, `sid`, `status`, `date`) VALUES
(11, 31694, 1, 'Office Purpose', '30', '1200', '36000', 13, 4, 1, '2014-02-12'),
(12, 31694, 1, 'Office Purpose', '20', '1200', '24000', 13, 4, 1, '2014-02-12'),
(13, 1754, 1, 'Office Purpose', '10', '1300', '13000', 13, 4, 1, '2014-02-12');

-- --------------------------------------------------------

--
-- Table structure for table `field_inventory_order_detail`
--

CREATE TABLE IF NOT EXISTS `field_inventory_order_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `sid` int(20) NOT NULL,
  `emplid` int(25) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `field_inventory_order_detail`
--

INSERT INTO `field_inventory_order_detail` (`id`, `orderid`, `totalprice`, `sid`, `emplid`, `status`, `date`) VALUES
(8, 31694, '60000', 4, 13, 1, '2014-02-12'),
(9, 1754, '13000', 4, 13, 1, '2014-02-12');

-- --------------------------------------------------------

--
-- Table structure for table `field_inventory_stock`
--

CREATE TABLE IF NOT EXISTS `field_inventory_stock` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `reorder` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `field_inventory_stock`
--

INSERT INTO `field_inventory_stock` (`id`, `pid`, `quantity`, `price`, `reorder`, `date`, `status`) VALUES
(1, '1', 0, 1300, 0, '2014-02-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `field_inventory_stockoutreport`
--

CREATE TABLE IF NOT EXISTS `field_inventory_stockoutreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `emplidfor` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `field_inventory_stockoutreport`
--

INSERT INTO `field_inventory_stockoutreport` (`id`, `pid`, `quantity`, `price`, `emplid`, `emplidfor`, `date`, `status`) VALUES
(1, '1', 30, 1300, 13, 13, '2014-02-12', 1),
(2, '1', 0, 1300, 13, 13, '2014-02-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `field_inventory_stockreport`
--

CREATE TABLE IF NOT EXISTS `field_inventory_stockreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `oid` int(20) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `orderid` int(20) NOT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `sid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `field_inventory_stockreport`
--

INSERT INTO `field_inventory_stockreport` (`id`, `oid`, `pid`, `orderid`, `quantity`, `price`, `sid`, `emplid`, `date`, `status`) VALUES
(1, 13, '1', 1754, 10, 1300, 4, 13, '2014-02-12', 1),
(2, 11, '1', 31694, 30, 1200, 4, 13, '2014-02-12', 1),
(3, 12, '1', 31694, -10, 1200, 4, 13, '2014-02-12', 1),
(4, 11, '1', 31694, 10, 1200, 4, 13, '2014-02-12', 1),
(5, 12, '1', 31694, -10, 1200, 4, 13, '2014-02-12', 1),
(6, 11, '1', 31694, 10, 1200, 4, 13, '2014-02-12', 1),
(7, 12, '1', 31694, -10, 1200, 4, 13, '2014-02-12', 1),
(8, 11, '1', 31694, 10, 1200, 4, 13, '2014-02-12', 1),
(9, 12, '1', 31694, -10, 1200, 4, 13, '2014-02-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `itemcondition`
--

CREATE TABLE IF NOT EXISTS `itemcondition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `itemcondition`
--

INSERT INTO `itemcondition` (`id`, `name`, `date`, `status`) VALUES
(1, 'Good', '2014-02-17', 1),
(2, 'Bad', '2014-02-17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `leave_type`
--

CREATE TABLE IF NOT EXISTS `leave_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `leave_type`
--

INSERT INTO `leave_type` (`id`, `name`, `date`, `status`) VALUES
(26, 'Annual Leave', '2014-02-16', 1),
(27, 'Summer Vacation Leave', '2014-02-16', 1),
(28, 'Winter Vacation Leave', '2014-02-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `sid` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE IF NOT EXISTS `order_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `sid` int(20) NOT NULL,
  `emplid` int(25) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `orderid`, `totalprice`, `sid`, `emplid`, `status`, `date`) VALUES
(6, 15391, '24000', 4, 13, 1, '2014-02-10'),
(7, 4417, '36000', 4, 13, 1, '2014-02-11');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `supid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `note` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `orderid`, `supid`, `emplid`, `amount`, `note`, `date`, `status`) VALUES
(1, 15391, 4, 13, '30', 'ddd', '2014-02-10', 1),
(2, 15391, 4, 13, '70', 'ddd', '2014-02-10', 1),
(3, 15391, 4, 13, '900', '', '2014-02-10', 1),
(4, 31694, 4, 13, '50', 'ddd', '2014-02-12', 1),
(5, 31694, 4, 13, '50', '', '2014-02-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `mrp` varchar(20) NOT NULL,
  `cid` int(20) DEFAULT NULL,
  `scid` int(20) DEFAULT NULL,
  `pack` int(20) NOT NULL,
  `reorder` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `barcode`, `emplid`, `quantity`, `price`, `mrp`, `cid`, `scid`, `pack`, `reorder`, `date`, `status`) VALUES
(16, 'Ambala', '1', 13, 12, 1234, '1255', 1, 4, 1, 3, '2014-02-10', 1),
(17, 'Belcony', '2', 13, 12, 123, '125', 1, 1, 7, 3, '2014-02-10', 1),
(18, 'Product N', 'Npro', 13, 100, 6, '8.89', 1, 1, 1, 3, '2014-02-11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `productrate`
--

CREATE TABLE IF NOT EXISTS `productrate` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `flat_rate` varchar(20) DEFAULT NULL,
  `mrp` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE IF NOT EXISTS `product_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `size` varchar(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `size`, `status`, `date`) VALUES
(1, 'Metrozanidole', '100''s', 1, '2014-02-10'),
(7, 'Ciprasdfdsf', '200''s', 1, '2014-02-10'),
(8, 'Albensfdfsd', '300''s', 1, '2014-02-10');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `authorizedperson` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `name`, `location`, `authorizedperson`, `startdate`, `date`, `status`) VALUES
(1, 'Project(1)', 'dfdsf', 'Ahmed', '2014-02-12', '2014-02-17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisationlist`
--

CREATE TABLE IF NOT EXISTS `requisationlist` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `requisationlist`
--

INSERT INTO `requisationlist` (`id`, `empid`, `projectid`, `date`, `status`) VALUES
(6, 20, 1, '2014-02-15', 1),
(7, 13, 1, '2014-02-18', 1),
(8, 13, 1, '2014-02-18', 1),
(9, 22, 1, '2014-02-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisationlist_reservation`
--

CREATE TABLE IF NOT EXISTS `requisationlist_reservation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `requisationlist_reservation`
--

INSERT INTO `requisationlist_reservation` (`id`, `empid`, `projectid`, `date`, `status`) VALUES
(1, 13, 3, '2014-02-15', 1),
(2, 13, 3, '2014-02-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisationlist_vehicle`
--

CREATE TABLE IF NOT EXISTS `requisationlist_vehicle` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `requisationlist_vehicle`
--

INSERT INTO `requisationlist_vehicle` (`id`, `empid`, `projectid`, `date`, `status`) VALUES
(1, 13, 3, '2014-02-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisation_leaveapplication`
--

CREATE TABLE IF NOT EXISTS `requisation_leaveapplication` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `quantity` varchar(20) DEFAULT NULL,
  `year` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `requisation_leaveapplication`
--

INSERT INTO `requisation_leaveapplication` (`id`, `quantity`, `year`, `date`, `status`) VALUES
(1, '20', '2014', '2014-02-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisation_leaveapplication_form`
--

CREATE TABLE IF NOT EXISTS `requisation_leaveapplication_form` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `leave_type` int(20) DEFAULT NULL,
  `description` text,
  `datequantity` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `emplid_m` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `requisation_leaveapplication_form`
--

INSERT INTO `requisation_leaveapplication_form` (`id`, `emplid`, `title`, `leave_type`, `description`, `datequantity`, `startdate`, `enddate`, `phone`, `emplid_m`, `date`, `status`) VALUES
(1, 13, 'Hiiiiii Some text here', 27, 'Hiiiiii Some text here', '2', '2014-02-16', '2014-02-17', '1927608261', 20, '2014-02-16', 1),
(2, 17, 'zxcxzc', 27, 'sadsadsads', '1', '2014-02-18', '2014-02-18', '23423423', 17, '2014-02-18', 1),
(3, 13, 'fgdgdfg', 28, 'asaddasdsadsadsad', '1', '2014-02-18', '2014-02-18', '23423423', 13, '2014-02-18', 1),
(4, 22, 'fgdgdfg', 26, 'bnmnbmbnm', '1', '2014-02-18', '2014-02-18', '23423423', 13, '2014-02-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisition`
--

CREATE TABLE IF NOT EXISTS `requisition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `requisition`
--

INSERT INTO `requisition` (`id`, `req_id`, `pid`, `description`, `empid`, `projectid`, `quantity`, `quantityissued`, `to`, `date`, `status`) VALUES
(10, 6, 17, 'for Office Purpose', 20, 1, '20', '', '10', '2014-02-15', 1),
(11, 7, 17, 'ewrwrwer', 13, 1, '23', '', '23', '2014-02-18', 1),
(12, 8, 16, 'ewrwrwer', 13, 1, '23', '', '23', '2014-02-18', 1),
(13, 9, 17, 'ewrwrwer', 22, 1, 'dxzCXzcxzc', '', '23', '2014-02-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisition_reservation`
--

CREATE TABLE IF NOT EXISTS `requisition_reservation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` text,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `requisition_reservation`
--

INSERT INTO `requisition_reservation` (`id`, `req_id`, `pid`, `description`, `empid`, `projectid`, `quantity`, `quantityissued`, `to`, `date`, `status`) VALUES
(1, 1, 'fddsfsdfsdf', 'for Office Purpose', 13, 3, '20', '', '23', '2014-02-15', 1),
(2, 1, 'tyuytuty', 'for Office Purpose', 13, 3, '20', '', '23', '2014-02-18', 1),
(3, 2, 'tyuytuty', 'for Office Purpose', 13, 3, '30', '', '23', '2014-02-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `requisition_vehicle`
--

CREATE TABLE IF NOT EXISTS `requisition_vehicle` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` text,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `requisition_vehicle`
--

INSERT INTO `requisition_vehicle` (`id`, `req_id`, `pid`, `description`, `empid`, `projectid`, `quantity`, `quantityissued`, `to`, `date`, `status`) VALUES
(1, 1, 'fddsfsdfsdf', 'for Office Purpose', 13, 3, '20', '', '23', '2014-02-15', 1),
(2, 1, 'tyuytuty', 'for Office Purpose', 13, 3, '20', '', '23', '2014-02-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `reorder` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stockoutreport`
--

CREATE TABLE IF NOT EXISTS `stockoutreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `emplidfor` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stockreport`
--

CREATE TABLE IF NOT EXISTS `stockreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `oid` int(20) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `orderid` int(20) NOT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `sid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `name`, `cid`, `date`, `status`) VALUES
(1, 'Table', 3, '2014-01-16', 1),
(2, 'AMS IT', 2, '2014-01-16', 1),
(3, 'rrrr', 2, '2014-01-16', 1),
(4, 'ASD', 2, '2014-02-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `paid` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `contactnumber`, `companyname`, `amount`, `paid`, `date`, `status`) VALUES
(4, 'Charukola', '1927608261', 'DU', '133000', '1100', '2014-02-10', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
