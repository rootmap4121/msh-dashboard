DROP TABLE assetproduct;

CREATE TABLE `assetproduct` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `assettype` varchar(20) DEFAULT NULL,
  `name` text,
  `msn` varchar(255) DEFAULT NULL,
  `sid` int(255) DEFAULT NULL,
  `datepr` date DEFAULT NULL,
  `bdtcost` varchar(255) DEFAULT NULL,
  `uscost` varchar(255) DEFAULT NULL,
  `dollarrate` varchar(255) DEFAULT NULL,
  `voucher_po` varchar(255) DEFAULT NULL,
  `itemofficeyn` varchar(5) DEFAULT NULL,
  `location` text,
  `itemcondition` int(5) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `warranty` varchar(255) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `lpi` varchar(255) DEFAULT NULL,
  `bywho` int(20) DEFAULT NULL,
  `remarks` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE assettype;

CREATE TABLE `assettype` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `legend` varchar(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Computer Accesories","CA","2014-02-17","1");



DROP TABLE barcode;

CREATE TABLE `barcode` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE branch;

CREATE TABLE `branch` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `contact` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Dhaka Office","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("2","Dhaka Central Warehouse","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("3","Chittagong Warehouse","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("4","Bogra Warehouse","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("5","Khulna Warehouse","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("6","Sylhet Warehouse","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("7","Dhaka Office & CMSD","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("8","Dhaka Office & MOHFW","","0","2014-03-06","1");
INSERT INTO `$table` VALUES("9","Dhaka Office & DGHS","","0","2014-03-06","1");



DROP TABLE category;

CREATE TABLE `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE designation;

CREATE TABLE `designation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Country Project  Director","2014-03-06","1");
INSERT INTO `$table` VALUES("2","Team Lead-Procurement","2014-03-06","1");
INSERT INTO `$table` VALUES("3","Team Lead-Health Systems Strengthening (HSS)","2014-03-06","1");
INSERT INTO `$table` VALUES("4","Senior Technical Advisor-Logistics","2014-03-06","1");
INSERT INTO `$table` VALUES("5","Senior Technical Advisor - Quantification & MIS","2014-03-06","1");
INSERT INTO `$table` VALUES("6","Senior Technical Advisor - Procurement","2014-03-06","1");
INSERT INTO `$table` VALUES("7","Senior Technical Advisor-MNCH","2014-03-06","1");
INSERT INTO `$table` VALUES("8","Finance & Operation Manager","2014-03-06","1");
INSERT INTO `$table` VALUES("9","Technical Advisor- Logistics","2014-03-06","1");
INSERT INTO `$table` VALUES("10","Technical Advisor-Tuberculosis","2014-03-06","1");
INSERT INTO `$table` VALUES("11","Senior Technical Advisor, Procurement","2014-03-06","1");
INSERT INTO `$table` VALUES("12","Senior Technical Advisor - Tuberculosis","2014-03-06","1");
INSERT INTO `$table` VALUES("13","Senior Technical Advisor-PLMC","2014-03-06","1");
INSERT INTO `$table` VALUES("14","Senior Technical Adviso-Monitoring & Evaluation (M&E)","2014-03-06","1");
INSERT INTO `$table` VALUES("16","IS -Specialist","2014-03-06","1");
INSERT INTO `$table` VALUES("17","Project Associate","2014-03-06","1");
INSERT INTO `$table` VALUES("18","Office Manager","2014-03-06","1");
INSERT INTO `$table` VALUES("19","Finance Associate","2014-03-06","1");
INSERT INTO `$table` VALUES("20","Accounting Assistant","2014-03-06","1");
INSERT INTO `$table` VALUES("21","Office Assistant","2014-03-06","1");
INSERT INTO `$table` VALUES("22","Administrative Assistant","2014-03-06","1");
INSERT INTO `$table` VALUES("23","Drivers ","2014-03-06","1");



DROP TABLE employee;

CREATE TABLE `employee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `staff_id` varchar(20) NOT NULL,
  `ext_number` varchar(20) NOT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) NOT NULL,
  `dob` date DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `joiningdate` date DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("13","amsit","0","000","22","6","1","A+","2014-01-01","1927608261","qq","big_1393870348__dsc4412.jpg","0000-00-00","amsit","8139be3698a555383168ec998c0e65bf","2014-02-09","2");
INSERT INTO `$table` VALUES("21","admin","0","","22","6","1","A+","2014-02-02","1927608261","44/P,Kazi Bhavan,New Road Zigatola Dhanmondi Dhaka","big_1392676082_429069_1896296584092_681278035_n.jpg","0000-00-00","admin","21232f297a57a5a743894a0e4a801fc3","2014-02-17","2");
INSERT INTO `$table` VALUES("22","Emon","0","","22","6","1","A+","2014-01-02","1927608261","44/P,Kazi Bhavan,New Road Zigatola Dhanmondi Dhaka","big_1393835751__dsc3031-cropped.jpg","0000-00-00","emon","b8cc4edba5145d41f9da01d85f459aef","2014-02-18","1");
INSERT INTO `$table` VALUES("23","Dr. Zubayer Hussain","","","1","1","1","","0000-00-00","1730450053","zhussain@msh.org","","0000-00-00","zubayerhussain","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("24","Dr. S. M. Abu Zahid ","","","2","1","1","","0000-00-00","1711886886","azahid@msh.org Â Â ","","0000-00-00","abuzahid ","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("25","Dr. Sheikh Asiruddin","","","3","1","1","","0000-00-00","1714088401","sasiruddin@msh.org Â Â ","","0000-00-00","sheikhasiruddin","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("26","Md. Abdullah","","","4","1","1","","0000-00-00","1749309504","mabdullah@msh.org","","0000-00-00","abdullah","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("27","Mohammad Golam Kibria Madhurza ","","","5","1","1","","0000-00-00","1753717355","mkibria@msh.org","","0000-00-00","mohammadgolamkibriamadhurza ","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("28","AKM. Abdullah Imam Khan","","","6","1","1","","0000-00-00","1749309502","akhan@msh.org","","0000-00-00","abdullahimamkhan","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("29","Dr. Javedur Rahman ","","","7","1","1","","0000-00-00","1730021549","jrahman@msh.orgÂ Â ","","0000-00-00","javedurrahman ","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("30","Mohammed Akter Hossain","","","8","1","1","","0000-00-00","1741679204","mahossain@msh.org","","0000-00-00","mohammedakterhossain","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("31","Md. Azim Uddin","","","9","2","1","","0000-00-00","1749309503","muddin@msh.org","","0000-00-00","azimuddin","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("32","Mian  Abdul Kader","","","9","3","1","","0000-00-00","1749309505","","","0000-00-00","mian  Abdul Kader","d41d8cd98f00b204e9800998ecf8427e","2014-03-06","1");
INSERT INTO `$table` VALUES("33","Md. Abu Shah Jamal Molla","","","9","4","1","","0000-00-00","1749309500","amolla@msh.org","","0000-00-00","abushahjamalmolla","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("34","Mohammed Hossain","","","9","5","1","","0000-00-00","1749309501","mhossain@msh.org","","0000-00-00","mohammedhossain","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("35","Syed Ahmmed Mostafa Al Amin","","","9","6","1","","0000-00-00","1749309507","salamin@msh.org","","0000-00-00","syedahmmedmostafaalamin","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("36","Dr. Kamal Hossain","","","10","1","1","","0000-00-00","1766935817","mkhossain@msh.org","","0000-00-00","kamalhossain","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("37","Fatema Samdani Roshni","","","11","7","2","","0000-00-00","1766573208","froshni@msh.org","","0000-00-00","fatemasamdaniroshni","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("38","Dr. A.T. M. Sanaul Bashar","","","12","1","1","","0000-00-00","1766573168","atmbashar@msh.org","","0000-00-00","sanaulbashar","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("39","Md. Shamsul Arefin Arif","","","13","8","1","","0000-00-00","1712556296","sarif@msh.org","","0000-00-00","shamsularefinarif","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("40","Zafor Imam ","","","13","8","1","","0000-00-00","1753","izafor@msh.org","","0000-00-00","zaforimam ","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("41","Mr. Fazle Karim","","","14","1","1","","0000-00-00","1776465003","fkarim@msh.org","","0000-00-00","fazlekarim","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("42","Nurul Kader ","","","9","9","1","","0000-00-00","1776465001","nkader@msh.org","","0000-00-00","nurulkader ","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("43","Md. Anwarul Islam ","","","6","7","1","","0000-00-00","1711","mislam@msh.org Â Â ","","0000-00-00","anwarulislam ","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("44","Luthful Haque Protik","","","16","","1","","0000-00-00","1711","mlhaque@msh.org","","0000-00-00","luthfulhaqueprotik","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("45","Loren Marceline Gomes","","","17","1","1","","0000-00-00","1753717357","lgomes@msh.org","","0000-00-00","lorenmarcelinegomes","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("46","Md. Ashraful Alam (Khokon)","","","18","1","1","","0000-00-00","1766573164","amollah@msh.org ","","0000-00-00","ashrafulalam","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("47","Sultana Yeasmin","","","19","","2","","0000-00-00","1768","syeasmin@msh.org","","0000-00-00","sultanayeasmin","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("48","Md.Rabiul Islam","","","20","1","1","","0000-00-00","1741","mrislam@msh.org","","0000-00-00","rabiulislam","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("49","Farhana Afroj","","","22","1","2","","0000-00-00","1717","fafroj@msh.org","","0000-00-00","farhanaafroj","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("50","Md. Akul Hossain","","","21","1","1","","0000-00-00","1753717356","ahossain@msh.org","","0000-00-00","akulhossain","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("51","Md. Bazlur Rahman","","","23","","1","","0000-00-00","1743","","","0000-00-00","bazlurrahman","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");
INSERT INTO `$table` VALUES("52","Md. Kanchan","","","23","","1","","0000-00-00","01716-523007","","","0000-00-00","kanchan","e10adc3949ba59abbe56e057f20f883e","2014-03-06","1");



DROP TABLE employee_basic_info;

CREATE TABLE `employee_basic_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `father_husbandname` varchar(255) DEFAULT NULL,
  `mothers_name` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `marrige_date` date DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `home_district` varchar(255) DEFAULT NULL,
  `relative_in_sc` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `tin_number` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `date_of_expire` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","22","Rofiqur","Rokia","A+","Single","2012-01-01","islam","Dhaka","Yes","Friend","BD32432432434","AP21321312321","CPA3432498324","2020-01-01","","");
INSERT INTO `$table` VALUES("11","13","rofiqur","rokia","A+","Single","0000-00-00","islam","Dhaka","Yes","Friend","4455556666666666","4444444444","444444444444488","0000-00-00","","");



DROP TABLE employee_educational_info;

CREATE TABLE `employee_educational_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `board` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `gpa` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("11","22","SSC","comilla","2006","A","4.25","","");
INSERT INTO `$table` VALUES("12","22","HSC","comilla","2008","A+","5.00","","");
INSERT INTO `$table` VALUES("13","22","Diploma","comilla","2010","A","3.75","","");
INSERT INTO `$table` VALUES("20","13","","","","","","","");



DROP TABLE employee_emergency_contact;

CREATE TABLE `employee_emergency_contact` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","22","Md Mahamod","address","phones","Me","","1");
INSERT INTO `$table` VALUES("2","22","Md Mahamod","address","phones","Me","","2");
INSERT INTO `$table` VALUES("19","13","","","","","","1");
INSERT INTO `$table` VALUES("20","13","","","","","","2");



DROP TABLE employee_present_address;

CREATE TABLE `employee_present_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","22","dfsd","sds","dsdsd","","1");
INSERT INTO `$table` VALUES("2","22","dfsd","sds","dsdsd","","2");
INSERT INTO `$table` VALUES("275","13","","","","","1");
INSERT INTO `$table` VALUES("276","13","","","","","2");



DROP TABLE field_inventory_order;

CREATE TABLE `field_inventory_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `sid` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE field_inventory_order_detail;

CREATE TABLE `field_inventory_order_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `sid` int(20) NOT NULL,
  `emplid` int(25) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE field_inventory_stock;

CREATE TABLE `field_inventory_stock` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `reorder` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE field_inventory_stockoutreport;

CREATE TABLE `field_inventory_stockoutreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `emplidfor` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE field_inventory_stockreport;

CREATE TABLE `field_inventory_stockreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `oid` int(20) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `orderid` int(20) NOT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `sid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE fieldproduct;

CREATE TABLE `fieldproduct` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `assettype` varchar(20) DEFAULT NULL,
  `name` text,
  `quantity` varchar(255) NOT NULL,
  `msn` varchar(255) DEFAULT NULL,
  `sid` int(255) DEFAULT NULL,
  `datepr` date DEFAULT NULL,
  `bdtcost` varchar(255) DEFAULT NULL,
  `uscost` varchar(255) DEFAULT NULL,
  `dollarrate` varchar(255) DEFAULT NULL,
  `marketvalueperunit` varchar(255) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `remarks` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE holiday_news;

CREATE TABLE `holiday_news` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `news` text,
  `holiday_date` date NOT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("4","New Years Day","2014-01-01","01","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("5","Eidâ€eâ€Meladonnabi *","2014-01-14","01","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("6","Martyrâ€™s Day","2014-02-21","02","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("7","Independence Day","2014-03-26","03","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("8","Bengali New years Day","2014-04-14","04","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("9","Labor Day","2014-05-01","05","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("10","Buddha Purnima *","2014-05-13","05","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("11","Shabâ€eâ€Borat *","2014-06-14","06","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("12","Shabâ€eâ€Quadr *","2014-07-26","07","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("13","Eidâ€ulâ€Fitr *","2014-07-28","07","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("14","Eidâ€ulâ€Fitr *","2014-07-29","07","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("15","Eidâ€ulâ€Fitr *","2014-07-30","07","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("16","National Mourning Day","2014-08-15","08","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("17","Shubo Janmastmi*","2014-08-17","08","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("18","Durga Puja","2014-10-04","10","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("19","Eidâ€ulâ€Azha *","2014-10-05","10","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("20","Eidâ€ulâ€Azha *","2014-10-06","10","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("21","Eidâ€ulâ€Azha *","2014-10-07","10","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("22","Moharram *","2014-11-04","11","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("23","Victory Day","2014-12-16","12","2014","2014-03-05","1");
INSERT INTO `$table` VALUES("24","Christmas Day","2014-12-25","12","2014","2014-03-05","1");



DROP TABLE itemcondition;

CREATE TABLE `itemcondition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("3","New","2014-03-06","1");
INSERT INTO `$table` VALUES("4","Good","2014-03-06","1");
INSERT INTO `$table` VALUES("5","Bad","2014-03-06","1");



DROP TABLE leave_type;

CREATE TABLE `leave_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE notice;

CREATE TABLE `notice` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `news` text,
  `holiday_date` date NOT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("2","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("3","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("4","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("5","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("6","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("7","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");
INSERT INTO `$table` VALUES("8","this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice this is a sample notice ","2014-03-06","03","2014","2014-03-06","1");



DROP TABLE notification;

CREATE TABLE `notification` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `f_emplid` int(20) NOT NULL,
  `nid` int(20) DEFAULT NULL,
  `detail` text,
  `s_status` int(20) DEFAULT NULL,
  `a_status` int(2) NOT NULL,
  `status` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE order;

CREATE TABLE `order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `sid` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE order_detail;

CREATE TABLE `order_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `sid` int(20) NOT NULL,
  `emplid` int(25) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE password_history;

CREATE TABLE `password_history` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("6","13","F@h@d555ooo","2014-03-06","1");
INSERT INTO `$table` VALUES("7","22","emon","2014-03-06","2");
INSERT INTO `$table` VALUES("8","22","emon","2014-03-06","1");



DROP TABLE payment;

CREATE TABLE `payment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `supid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `note` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE product;

CREATE TABLE `product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `mrp` varchar(20) NOT NULL,
  `cid` int(20) DEFAULT NULL,
  `scid` int(20) DEFAULT NULL,
  `pack` int(20) NOT NULL,
  `reorder` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE product_type;

CREATE TABLE `product_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `size` varchar(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE productrate;

CREATE TABLE `productrate` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `flat_rate` varchar(20) DEFAULT NULL,
  `mrp` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE project;

CREATE TABLE `project` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `authorizedperson` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Project 1","dfdsf","Ahmed","2014-03-12","2014-03-06","1");



DROP TABLE requisation_leaveapplication;

CREATE TABLE `requisation_leaveapplication` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `quantity` varchar(20) DEFAULT NULL,
  `leave_type` int(20) NOT NULL,
  `year` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisation_leaveapplication_form;

CREATE TABLE `requisation_leaveapplication_form` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `supervisor` int(255) DEFAULT NULL,
  `leave_type` int(20) DEFAULT NULL,
  `description` text,
  `datequantity` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `startdatetime` int(2) NOT NULL,
  `enddate` date DEFAULT NULL,
  `enddatetime` int(2) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `incharge` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `year` varchar(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisationlist;

CREATE TABLE `requisationlist` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `supervisor` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisationlist_reservation;

CREATE TABLE `requisationlist_reservation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` text NOT NULL,
  `startdatetime` varchar(255) NOT NULL,
  `enddatetime` varchar(255) NOT NULL,
  `supervisor` int(11) NOT NULL,
  `detail` text NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisationlist_vehicle;

CREATE TABLE `requisationlist_vehicle` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisition;

CREATE TABLE `requisition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisition_reservation;

CREATE TABLE `requisition_reservation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` text,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE requisition_vehicle;

CREATE TABLE `requisition_vehicle` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `startdatetime` varchar(255) NOT NULL,
  `enddatetime` varchar(255) NOT NULL,
  `startpoint` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `passengerquantity` varchar(255) NOT NULL,
  `passengernames` text NOT NULL,
  `purpose` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `typeoftravel` varchar(255) NOT NULL,
  `budgetcode` varchar(255) NOT NULL,
  `budgetholder` varchar(255) NOT NULL,
  `supervisor` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE stock;

CREATE TABLE `stock` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `reorder` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE stockoutreport;

CREATE TABLE `stockoutreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `emplidfor` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE stockreport;

CREATE TABLE `stockreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `oid` int(20) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `orderid` int(20) NOT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `sid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE subcategory;

CREATE TABLE `subcategory` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE supplier;

CREATE TABLE `supplier` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `paid` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




