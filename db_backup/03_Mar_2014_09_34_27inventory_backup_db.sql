DROP TABLE assetproduct;

CREATE TABLE `assetproduct` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `assettype` varchar(20) DEFAULT NULL,
  `name` text,
  `msn` varchar(255) DEFAULT NULL,
  `sid` int(255) DEFAULT NULL,
  `datepr` date DEFAULT NULL,
  `bdtcost` varchar(255) DEFAULT NULL,
  `uscost` varchar(255) DEFAULT NULL,
  `dollarrate` varchar(255) DEFAULT NULL,
  `voucher_po` varchar(255) DEFAULT NULL,
  `itemofficeyn` varchar(5) DEFAULT NULL,
  `location` text,
  `itemcondition` int(5) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `warranty` varchar(255) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `lpi` varchar(255) DEFAULT NULL,
  `bywho` int(20) DEFAULT NULL,
  `remarks` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("2","1234","CE","Pack Size","L007031","4","2014-02-11","L007031","L007031","L007031","L007031","Y","L007031","2","1","N/A","L007031","2014-02-12","13","L007031","2014-02-12","1");
INSERT INTO `$table` VALUES("3","sdfsdfsdf","OF","dsfdsf","sdfdsfsdf","4","2014-02-10","43","34","34","32d","Y","dfdfdsf","3","3","17 Years","dfdsfsdfdsf","2014-02-12","13","dfdsfdsf","2014-02-12","1");
INSERT INTO `$table` VALUES("4","fghgfh","ME","hfghfg","fghgfh","4","2014-02-12","fghgfh","fghgf","hfghgfh","fhgfh","Y","fghgfh","1","1","N/A","fghfghfgh","2014-02-12","13","fghgfh","2014-02-12","1");



DROP TABLE assettype;

CREATE TABLE `assettype` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `legend` varchar(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Computer Accesories","CA","2014-02-17","1");



DROP TABLE barcode;

CREATE TABLE `barcode` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Napa","2014-02-19","1");



DROP TABLE branch;

CREATE TABLE `branch` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `contact` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("6","AMS IT","44/P,Kazi Bhaban,New Road ,Dhanmondi,Dhaka","1927608261","2014-01-14","1");



DROP TABLE category;

CREATE TABLE `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Technological","2014-01-16","1");
INSERT INTO `$table` VALUES("2","Matirials","2014-01-16","1");
INSERT INTO `$table` VALUES("3","Office Furniture","2014-01-16","1");



DROP TABLE designation;

CREATE TABLE `designation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("10","MPO","2014-01-14","1");
INSERT INTO `$table` VALUES("23","Manager","2014-01-15","1");
INSERT INTO `$table` VALUES("24","Serviceholder","2014-01-15","1");
INSERT INTO `$table` VALUES("25","Incharge","2014-01-15","1");
INSERT INTO `$table` VALUES("26","CEO","2014-02-20","1");
INSERT INTO `$table` VALUES("27","Accountant","2014-02-20","1");
INSERT INTO `$table` VALUES("28","Admin","2014-02-20","1");
INSERT INTO `$table` VALUES("29","MLS","2014-02-20","1");



DROP TABLE employee;

CREATE TABLE `employee` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contactnumber` int(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `joiningdate` date DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("13","amsit","23","6","1","2014-01-01","1927608261","qq","","2012-02-17","amsit","8139be3698a555383168ec998c0e65bf","2014-02-09","2");
INSERT INTO `$table` VALUES("17","ARony","25","6","1","2013-02-02","1927608261","qq","","1998-02-19","rony","202cb962ac59075b964b07152d234b70","2014-02-11","1");
INSERT INTO `$table` VALUES("20","Fahad Bhuyian","25","6","1","2012-01-03","1927608261","qq","big_1392089486_facebook-like-button-johor-malaysia-july-icon-voting-system-used-to-rate-user-comments-low-shu-ching-hand-holding-32860018.jpg","1998-02-19","fahad","bfd59291e825b5f2bbf1eb76569f8fe7","2014-02-11","1");
INSERT INTO `$table` VALUES("21","admin","25","6","1","2014-02-02","1927608261","44/P,Kazi Bhavan,New Road Zigatola Dhanmondi Dhaka","big_1392676082_429069_1896296584092_681278035_n.jpg","2014-01-01","admin","21232f297a57a5a743894a0e4a801fc3","2014-02-17","3");
INSERT INTO `$table` VALUES("22","Emon","24","6","1","2014-01-02","1927608261","44/P,Kazi Bhavan,New Road Zigatola Dhanmondi Dhaka","big_1393835751__dsc3031-cropped.jpg","1996-02-18","emon","b8cc4edba5145d41f9da01d85f459aef","2014-02-18","1");



DROP TABLE employee_basic_info;

CREATE TABLE `employee_basic_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `father_husbandname` varchar(255) DEFAULT NULL,
  `mothers_name` varchar(255) DEFAULT NULL,
  `blood_group` varchar(20) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `marrige_date` date DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `home_district` varchar(255) DEFAULT NULL,
  `relative_in_sc` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `tin_number` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `passport_number` varchar(255) DEFAULT NULL,
  `date_of_expire` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","22","Rofiqur","Rokia","A+","Single","2012-01-01","islam","Dhaka","Yes","Friend","4455556666666666","4444444444","444444444444488","2020-01-01","","");



DROP TABLE employee_educational_info;

CREATE TABLE `employee_educational_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `board` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `division` varchar(255) DEFAULT NULL,
  `gpa` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("11","22","SSC","comilla","2006","A","4.25","","");
INSERT INTO `$table` VALUES("12","22","HSC","comilla","2008","A+","5.00","","");
INSERT INTO `$table` VALUES("13","22","Diploma","comilla","2010","A","3.75","","");



DROP TABLE employee_emergency_contact;

CREATE TABLE `employee_emergency_contact` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","22","Md Mahamod","address","phones","Me","","1");
INSERT INTO `$table` VALUES("2","22","Md Mahamod","address","phones","Me","","2");



DROP TABLE employee_present_address;

CREATE TABLE `employee_present_address` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","22","dfsd","sds","dsdsd","","1");
INSERT INTO `$table` VALUES("2","22","dfsd","sds","dsdsd","","2");



DROP TABLE field_inventory_order;

CREATE TABLE `field_inventory_order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `sid` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("11","31694","1","Office Purpose","30","1200","36000","13","4","1","2014-02-12");
INSERT INTO `$table` VALUES("12","31694","1","Office Purpose","20","1200","24000","13","4","1","2014-02-12");
INSERT INTO `$table` VALUES("13","1754","1","Office Purpose","10","1300","13000","13","4","1","2014-02-12");



DROP TABLE field_inventory_order_detail;

CREATE TABLE `field_inventory_order_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `sid` int(20) NOT NULL,
  `emplid` int(25) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("8","31694","60000","4","13","1","2014-02-12");
INSERT INTO `$table` VALUES("9","1754","13000","4","13","1","2014-02-12");



DROP TABLE field_inventory_stock;

CREATE TABLE `field_inventory_stock` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `reorder` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","1","0","1300","0","2014-02-12","1");



DROP TABLE field_inventory_stockoutreport;

CREATE TABLE `field_inventory_stockoutreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `emplidfor` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","1","30","1300","13","13","2014-02-12","1");
INSERT INTO `$table` VALUES("2","1","0","1300","13","13","2014-02-12","1");



DROP TABLE field_inventory_stockreport;

CREATE TABLE `field_inventory_stockreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `oid` int(20) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `orderid` int(20) NOT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `sid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","13","1","1754","10","1300","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("2","11","1","31694","30","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("3","12","1","31694","-10","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("4","11","1","31694","10","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("5","12","1","31694","-10","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("6","11","1","31694","10","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("7","12","1","31694","-10","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("8","11","1","31694","10","1200","4","13","2014-02-12","1");
INSERT INTO `$table` VALUES("9","12","1","31694","-10","1200","4","13","2014-02-12","1");



DROP TABLE fieldproduct;

CREATE TABLE `fieldproduct` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) DEFAULT NULL,
  `assettype` varchar(20) DEFAULT NULL,
  `name` text,
  `quantity` varchar(255) NOT NULL,
  `msn` varchar(255) DEFAULT NULL,
  `sid` int(255) DEFAULT NULL,
  `datepr` date DEFAULT NULL,
  `bdtcost` varchar(255) DEFAULT NULL,
  `uscost` varchar(255) DEFAULT NULL,
  `dollarrate` varchar(255) DEFAULT NULL,
  `marketvalueperunit` varchar(255) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `remarks` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","1234","OE","Pack Size","100","L0070313","4","2014-02-03","L007031","L007031","L007031","120","L007031","dfdsfdsf","2014-02-12","1");



DROP TABLE holiday_news;

CREATE TABLE `holiday_news` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `news` text,
  `holiday_date` date NOT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Summer Vacation ","2014-02-19","02","2014","2014-02-19","1");
INSERT INTO `$table` VALUES("2","Winte Vacation","2014-02-26","02","2014","2014-02-19","1");
INSERT INTO `$table` VALUES("3","This Month Annual Conference","2014-02-22","02","2014","2014-02-22","1");



DROP TABLE itemcondition;

CREATE TABLE `itemcondition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Good","2014-02-17","1");
INSERT INTO `$table` VALUES("2","Bad","2014-02-17","1");



DROP TABLE leave_type;

CREATE TABLE `leave_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("26","Annual Leave","2014-02-16","1");
INSERT INTO `$table` VALUES("27","Summer Vacation Leave","2014-02-16","1");
INSERT INTO `$table` VALUES("28","Winter Vacation Leave","2014-02-16","1");



DROP TABLE notice;

CREATE TABLE `notice` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `news` text,
  `holiday_date` date NOT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Summer Vacation ","2014-02-19","02","2014","2014-02-19","1");
INSERT INTO `$table` VALUES("2","Winte Vacation","2014-02-26","02","2014","2014-02-19","1");
INSERT INTO `$table` VALUES("3","This Month Annual Conference","2014-02-22","02","2014","2014-02-22","1");



DROP TABLE order;

CREATE TABLE `order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `sid` int(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","7436","19","","10000","1","10000","13","4","1","2014-02-19");
INSERT INTO `$table` VALUES("2","3593","17","dsdasdsad","34","1","34","13","4","1","2014-02-20");



DROP TABLE order_detail;

CREATE TABLE `order_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `totalprice` varchar(20) NOT NULL,
  `sid` int(20) NOT NULL,
  `emplid` int(25) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("6","15391","24000","4","13","1","2014-02-10");
INSERT INTO `$table` VALUES("7","4417","36000","4","13","1","2014-02-11");
INSERT INTO `$table` VALUES("8","7436","10000","4","13","1","2014-02-19");
INSERT INTO `$table` VALUES("9","3593","34","4","13","1","2014-02-20");



DROP TABLE payment;

CREATE TABLE `payment` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `orderid` int(20) DEFAULT NULL,
  `supid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `note` text,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","15391","4","13","30","ddd","2014-02-10","1");
INSERT INTO `$table` VALUES("2","15391","4","13","70","ddd","2014-02-10","1");
INSERT INTO `$table` VALUES("3","15391","4","13","900","","2014-02-10","1");
INSERT INTO `$table` VALUES("4","31694","4","13","50","ddd","2014-02-12","1");
INSERT INTO `$table` VALUES("5","31694","4","13","50","","2014-02-12","1");
INSERT INTO `$table` VALUES("6","7436","4","13","41900","invoice payment","2014-02-19","1");



DROP TABLE product;

CREATE TABLE `product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) NOT NULL,
  `emplid` int(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `mrp` varchar(20) NOT NULL,
  `cid` int(20) DEFAULT NULL,
  `scid` int(20) DEFAULT NULL,
  `pack` int(20) NOT NULL,
  `reorder` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("16","Ambala","1","13","12","1234","1255","1","4","1","3","2014-02-10","1");
INSERT INTO `$table` VALUES("17","Belcony","2","13","12","123","125","1","1","7","3","2014-02-10","1");
INSERT INTO `$table` VALUES("18","Product N","Npro","13","100","6","8.89","1","1","1","3","2014-02-11","1");
INSERT INTO `$table` VALUES("19","Napa","Napa","13","1000","2","4","1","1","1","100","2014-02-19","1");



DROP TABLE product_type;

CREATE TABLE `product_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `size` varchar(20) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Metrozanidole","100\'s","1","2014-02-10");
INSERT INTO `$table` VALUES("7","Ciprasdfdsf","200\'s","1","2014-02-10");
INSERT INTO `$table` VALUES("8","Albensfdfsd","300\'s","1","2014-02-10");



DROP TABLE productrate;

CREATE TABLE `productrate` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) DEFAULT NULL,
  `flat_rate` varchar(20) DEFAULT NULL,
  `mrp` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE project;

CREATE TABLE `project` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `authorizedperson` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Project(1)","dfdsf","Ahmed","2014-02-12","2014-02-17","1");



DROP TABLE requisation_leaveapplication;

CREATE TABLE `requisation_leaveapplication` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `quantity` varchar(20) DEFAULT NULL,
  `year` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("2","20","2014","2014-02-23","1");



DROP TABLE requisation_leaveapplication_form;

CREATE TABLE `requisation_leaveapplication_form` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `supervisor` int(255) DEFAULT NULL,
  `leave_type` int(20) DEFAULT NULL,
  `description` text,
  `datequantity` varchar(255) DEFAULT NULL,
  `startdate` date DEFAULT NULL,
  `startdatetime` int(2) NOT NULL,
  `enddate` date DEFAULT NULL,
  `enddatetime` int(2) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `incharge` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("6","13","21","26","cxzxcxzczxcxzc","1","2014-02-26","1","2014-02-26","1","01927608261","22","2014-02-26","2");
INSERT INTO `$table` VALUES("7","13","17","27","zxfzfsafsadf","1","2014-03-12","1","2014-03-28","1","01927608261","20","2014-03-01","1");
INSERT INTO `$table` VALUES("8","22","13","26","jlkkkklklklkl","1","2014-03-03","1","2014-03-18","1","01927608261","17","2014-03-02","1");



DROP TABLE requisationlist;

CREATE TABLE `requisationlist` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `supervisor` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("6","20","1","0","2014-02-15","1");
INSERT INTO `$table` VALUES("7","13","1","0","2014-02-18","1");
INSERT INTO `$table` VALUES("8","13","1","0","2014-02-18","1");
INSERT INTO `$table` VALUES("9","22","1","0","2014-02-18","1");
INSERT INTO `$table` VALUES("10","13","1","21","2014-03-02","2");
INSERT INTO `$table` VALUES("11","22","1","13","2014-03-02","2");



DROP TABLE requisationlist_reservation;

CREATE TABLE `requisationlist_reservation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `title` text NOT NULL,
  `startdatetime` varchar(255) NOT NULL,
  `enddatetime` varchar(255) NOT NULL,
  `supervisor` int(11) NOT NULL,
  `detail` text NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("3","13","Meeting In Sheraton","2014-04-07 05:03","2014-04-23 07:03","21","Meeting In Sheraton","2014-02-27","1");
INSERT INTO `$table` VALUES("4","13","Meeting In Sheraton2","2014-02-26 05:40","2014-02-26 07:40","21","Meeting In Sheraton 2","2014-02-27","2");



DROP TABLE requisationlist_vehicle;

CREATE TABLE `requisationlist_vehicle` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","13","3","2014-02-15","1");



DROP TABLE requisition;

CREATE TABLE `requisition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` int(20) DEFAULT NULL,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("10","6","17","for Office Purpose","20","1","20","","10","2014-02-15","1");
INSERT INTO `$table` VALUES("11","7","17","ewrwrwer","13","1","23","","23","2014-02-18","1");
INSERT INTO `$table` VALUES("12","8","16","ewrwrwer","13","1","23","","23","2014-02-18","1");
INSERT INTO `$table` VALUES("13","9","17","ewrwrwer","22","1","dxzCXzcxzc","","23","2014-02-18","1");
INSERT INTO `$table` VALUES("14","10","17","ewrwrwer","13","1","23","20","21","2014-03-02","1");
INSERT INTO `$table` VALUES("15","10","18","ewrwrwerssss","13","1","20","10","21","2014-03-02","1");
INSERT INTO `$table` VALUES("16","11","17","ewrwrwer","22","1","12","","13","2014-03-02","1");
INSERT INTO `$table` VALUES("17","11","16","ewrwrwer","22","1","22","","13","2014-03-02","1");
INSERT INTO `$table` VALUES("18","11","16","ewrwrwer","22","1","33","","13","2014-03-02","1");
INSERT INTO `$table` VALUES("19","11","16","ewrwrwer","22","1","44","","13","2014-03-02","1");



DROP TABLE requisition_reservation;

CREATE TABLE `requisition_reservation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `req_id` int(20) NOT NULL,
  `pid` text,
  `description` text NOT NULL,
  `empid` int(20) DEFAULT NULL,
  `projectid` int(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantityissued` varchar(20) NOT NULL,
  `to` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","1","fddsfsdfsdf","for Office Purpose","13","3","20","","23","2014-02-15","1");
INSERT INTO `$table` VALUES("2","1","tyuytuty","for Office Purpose","13","3","20","","23","2014-02-18","1");
INSERT INTO `$table` VALUES("3","2","tyuytuty","for Office Purpose","13","3","30","","23","2014-02-15","1");



DROP TABLE requisition_vehicle;

CREATE TABLE `requisition_vehicle` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `emplid` int(20) DEFAULT NULL,
  `startdatetime` varchar(255) NOT NULL,
  `enddatetime` varchar(255) NOT NULL,
  `startpoint` varchar(255) NOT NULL,
  `destination` varchar(255) NOT NULL,
  `passengerquantity` varchar(255) NOT NULL,
  `passengernames` text NOT NULL,
  `purpose` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `typeoftravel` varchar(255) NOT NULL,
  `budgetcode` varchar(255) NOT NULL,
  `budgetholder` varchar(255) NOT NULL,
  `supervisor` int(20) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("2","13","2014-04-13 05:50","2014-04-30 07:50","Dhanmondi","Chittagong,GSC Circle","20","fahad,Mahabur Rahman,Kazi Emon","Official Work","01927608261","Official Work","CA","Mr.Shohel Rana","21","2014-02-28","1");



DROP TABLE stock;

CREATE TABLE `stock` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `reorder` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","19","9000","1","100","2014-02-19","1");
INSERT INTO `$table` VALUES("2","17","33","1","3","2014-02-20","1");



DROP TABLE stockoutreport;

CREATE TABLE `stockoutreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `emplidfor` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","19","1000","1","13","20","2014-02-19","1");
INSERT INTO `$table` VALUES("2","17","1","1","13","13","2014-02-20","1");



DROP TABLE stockreport;

CREATE TABLE `stockreport` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `oid` int(20) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `orderid` int(20) NOT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `sid` int(20) DEFAULT NULL,
  `emplid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","1","19","7436","10000","1","4","13","2014-02-19","1");
INSERT INTO `$table` VALUES("2","2","17","3593","4","1","4","13","2014-02-20","1");
INSERT INTO `$table` VALUES("3","2","17","3593","20","1","4","13","2014-02-20","1");
INSERT INTO `$table` VALUES("4","2","17","3593","10","1","4","13","2014-02-20","1");



DROP TABLE subcategory;

CREATE TABLE `subcategory` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cid` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("1","Table","3","2014-01-16","1");
INSERT INTO `$table` VALUES("2","AMS IT","2","2014-01-16","1");
INSERT INTO `$table` VALUES("3","rrrr","2","2014-01-16","1");
INSERT INTO `$table` VALUES("4","ASD","2","2014-02-10","1");



DROP TABLE supplier;

CREATE TABLE `supplier` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `paid` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `$table` VALUES("4","Charukola","1927608261","DU","143034","43000","2014-02-10","1");



