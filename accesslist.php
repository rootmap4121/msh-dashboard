<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Access Log Info</a></li><li class='active'>Access Log List</li>";
$table="notification";
if (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
     
                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Access Log List</h3>


                                        <div class="table-responsive">
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Employee </th>
                                                        <th>Super Visor </th>
                                                        <th>Requisation ID</th>
                                                        <th>Detail</th>
                                                        <th>Status </th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($_SESSION['SESS_AMSIT_EMP_STATUS']!=1)
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                }
                                                else
                                                {
                                                $data=$obj->SelectAllByID($table,array("emplid"=>$input_by));    
                                                }
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row):
                                                    ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $obj->SelectAllByVal("employee","id",$row->emplid,"name"); ?></td>
                                                            <td><?php echo $obj->SelectAllByVal("employee","id",$row->f_emplid,"name"); ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->nid; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->detail; ?></span></td>
                                                            <td><?php echo $obj->empreq_status($row->status); ?></td>
                                                            <td><?php echo $row->date; ?></td>
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
