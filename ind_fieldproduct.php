<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Asset Product List</a></li><li class='active'>Add Asset Product</li>";
$table="fieldproduct";

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
		
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <h3 class="header smaller lighter blue"><span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                    <div class="row" id="printablediv">
                        <div class="col-xs-12">
                            <?php 
                            $dsf=$obj->SelectAllByID($table,array("id"=>$_GET['id'])); 
                            foreach($dsf as $row):
                            ?>
                            <!-- PAGE CONTENT BEGINS -->
                            <h4 align="center">
                                MSH Field Office Inventory
                                                <br>
                                                <small>
                                                    Office and Country: SIAPS Bangladesh
                                                </small>
                                                <br>
                                                <small>
                                                    As of (Date): <?php echo date('M d, Y'); ?>
                                                </small>
                                                <br>
                                                
                                                <small>person who prepared report : 
                                                    <?php 
                                                    $prepar=$obj->SelectAllByID("employee",array("id"=>$input_by));  
                                                    foreach($prepar as $pre):
                                                        echo $pre->name;
                                                    endforeach;
                                                    ?>
                                                </small> 
                            </h4>
                            <h4 class="header smaller blue">Barcode - <?php echo $row->barcode; ?> <span style="float: right;">Creation Date : <?php echo date('Y-m-d'); ?></span></h4>
                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                                                                                             
                                                                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                        
                                                                                            
                                                                                            <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Product Name </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="name" readonly="readonly"  value="<?php echo $row->name; ?>" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Barcode </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="barcode" readonly="readonly" value="<?php echo $row->barcode; ?>" placeholder="Barcode Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Quantity </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="quantity" readonly="readonly" value="<?php echo $row->quantity; ?>" placeholder="Barcode Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Asset Type </label>

                                    <div class="col-sm-8">
                                        <select name="assettype" readonly="readonly" >
                                            <?php 
                                            $ast=$obj->SelectAllByID("assettype",array("legend"=>$row->assettype));
                                            foreach($ast as $st):
                                            ?>
                                            <option <?php if($row->assettype==$st->legend): ?> selected="selected"  <?php endif; ?> value="<?php echo $st->legend; ?>"><?php echo $st->name; ?></option>
                                            <?php 
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Manufacturer's Serial Number </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" readonly="readonly"  value="<?php echo $row->msn; ?>" name="msn" placeholder="Manufacturer's Serial Number" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Supplier/Vendor </label>

                                    <div class="col-sm-8">
                                        <select name="sid">
                                            <?php 
                                            $ast=$obj->SelectAll("supplier");
                                            foreach($ast as $st):
                                            ?>
                                            <option <?php if($row->sid==$st->id): ?> selected="selected"  <?php endif; ?>  value="<?php echo $st->id; ?>"><?php echo $st->name; ?></option>
                                            <?php 
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Recive/Purchase Date </label>

                                    <div class="col-xs-6 col-sm-4">
                                            <div class="input-group">
                                                <input class="form-control date-picker" readonly="readonly"  name="datepr" value="<?php echo $row->datepr; ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> BDT Cost </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" readonly="readonly"  name="bdtcost" value="<?php echo $row->bdtcost; ?>" placeholder="BDT Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> US Cost </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" readonly="readonly"  name="uscost" value="<?php echo $row->uscost; ?>" placeholder="US Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Dollar Rate </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" readonly="readonly"  name="dollarrate" value="<?php echo $row->dollarrate; ?>" placeholder="Dollar Rate" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Market Value Per Unit </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" readonly="readonly"  name="marketvalueperunit" value="<?php echo $row->marketvalueperunit; ?>"  placeholder="Voucher/PO #" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                                                
                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Disposition </label>

                                    <div class="col-sm-8">
                                        <input type="text" readonly="readonly"  id="form-field-1" value="<?php echo $row->disposition; ?>"   name="disposition" placeholder="Item Disposition" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Remarks </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" readonly="readonly"  name="remarks" value="<?php echo $row->remarks; ?>"  placeholder="Remarks" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
              
                                <div class="space-4"></div>
                                
                                
                                
                            </form>
                                        

                            <?php endforeach; ?>  
                            <h3 class="header smaller lighter blue"> Approved By : </h3>
                            
                            						
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>
		

		<!-- inline scripts related to this page -->

    </body>
</html>
