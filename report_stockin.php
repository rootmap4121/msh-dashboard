<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Stock Info</a></li>";
$table="stock";
$table3="stockreport";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Stock in Detail Report <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print Stock Detail</a></span></h3>
                                        <div class="table-header">
                                            Results for "Total Stock in Product&rsquo;s" (<?php echo $obj->totalrows($table3); ?>)
                                        </div>

                                        <div class="table-responsive" id="printablediv">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>Product Name</th>
                                                        <th>Quantity </th>
                                                        <!--<th>Date</th>-->
                                                        <th>Price</th>
                                                        <th>Supplier Name</th>
                                                        <th>Employee Name</th>
                                                        <th>Stock In Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllorderBy($table3);
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $row->orderid; ?></td>
                                                            
                                                            <td><i class="icon-caret-right green"></i> <?php $pro=$obj->SelectAllByID("product",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?></td>
                                                            
                                                            <td><i class="icon-exchange  blue"></i> <?php echo $row->quantity; ?></td>
                                                            <td><i class="icon-bookmark-empty blue"></i>  <?php echo $row->price; ?></td> 
                                                            <td><i class="icon-group blue"></i> <?php $pro=$obj->SelectAllByID("supplier",array("id"=>$row->sid)); foreach($pro as $product): echo $product->name; endforeach; ?> </td>
                                                            <td> <i class="icon-lightbulb red"></i> <?php $pro=$obj->SelectAllByID("employee",array("id"=>$row->emplid)); foreach($pro as $product): echo $product->name; endforeach; ?> </td>
                                                            <td><i class="icon-bar-chart green"></i> <?php echo $row->date; ?></td>
                                                        </tr>
                                                 <?php $x++; endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                							
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
