<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Stock Info</a></li>";
$table="stock";
$table2="stockoutreport";
$table3="stockreport";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Stock Detail  </h3>
                                        <div class="table-header">
                                            Results for "Total Stock Product&rsquo;s" (<?php echo $obj->totalrows($table); ?>)
                                        </div>

                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Product Name</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Re-Order </th>
                                                        <!--<th>Date</th>-->
                                                        <th>Stock In Report</th>
                                                        <th>Stock Out Report</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><i class="icon-caret-right green"></i> <?php $pro=$obj->SelectAllByID("product",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?></td>
                                                            <td><i class="icon-bar-chart orange"></i> <?php echo $row->quantity; ?></td>
                                                            <td><i class="icon-bookmark-empty blue"></i> <?php echo $row->price; ?></td>
                                                            <td><span class="label label-sm label-<?php if($row->reorder<$row->quantity){ ?>success<?php }else{ ?>warning<?php } ?> "><i class="icon-beaker dark"></i> <?php echo $row->reorder; ?></span></td>
                                                            <!--<td><i class="icon-calendar gray"></i> <?php //echo $row->date; ?></td>-->
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-reportin<?php echo $row->id; ?>" role="button" data-toggle="modal"><i class="icon-exchange bigger-130"></i> Stock In Report</a>
                                                                </div>
<div id="modal-reportin<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header no-padding">
                                <div class="table-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <span class="white">&times;</span>
                                        </button>
                                        Stock In Report
                                </div>
                        </div>
                        <!-- /.modal-content -->

                        <div class="widget-toolbar hidden-480">
                            <a href="ind_stockin.php?pid=<?php echo $row->pid; ?>">
                                                    <i class="icon-print"></i>  Print Report
                            </a>
                        </div>
                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Product</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Stock In By</th>
                                                        <th>Vendor</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllByID($table3,array("pid"=>$row->pid));
                                                $d=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $d; ?></td>
                                                            <td><span class="label label-sm label-info"><?php $pro=$obj->SelectAllByID("product",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->quantity; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->price; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php $em=$obj->SelectAllByID("employee",array("id"=>$row->emplid)); foreach($em as $emp): echo $emp->name; endforeach; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php $su=$obj->SelectAllByID("supplier",array("id"=>$row->sid)); foreach($su as $sup): echo $sup->name; endforeach; ?></span></td>
                                                            <td><?php echo $row->date; ?></td>
                                                        </tr>
                                                 <?php $d++; endforeach; ?>


                                                </tbody>
                                            </table>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- end modal form -->
                                                                
                                                            </td>
                                                            <td>
                                                             
                                                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-reportout<?php echo $row->id; ?>" role="button" data-toggle="modal"><i class="icon-exchange bigger-130"></i> Stock Out Report</a>
                                                                </div>
<div id="modal-reportout<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header no-padding">
                                <div class="table-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                <span class="white">&times;</span>
                                        </button>
                                        Stock Out Report
                                </div>
                        </div>
                        <!-- /.modal-content -->

                        <div class="widget-toolbar hidden-480">
                            <a href="ind_stockout.php?pid=<?php echo $row->pid; ?>">
                                                    <i class="icon-print"></i>  Print Report
                                            </a>
                        </div>
                        <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Product</th>
                                                        <th>Quantity</th>
                                                        <th>Price</th>
                                                        <th>Stock Out By</th>
                                                        <th>Product Accepted by</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllByID($table2,array("pid"=>$row->pid));
                                                $d=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $d; ?></td>
                                                            <td><span class="label label-sm label-info"><?php $pro=$obj->SelectAllByID("product",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->quantity; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->price; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php $em=$obj->SelectAllByID("employee",array("id"=>$row->emplid)); foreach($em as $emp): echo $emp->name; endforeach; ?></span></td>
                                                            <td><span class="label label-sm label-success"><?php $su=$obj->SelectAllByID("employee",array("id"=>$row->emplidfor)); foreach($su as $sup): echo $sup->name; endforeach; ?></span></td>
                                                            <td><?php echo $row->date; ?></td>
                                                        </tr>
                                                 <?php $d++; endforeach; ?>


                                                </tbody>
                                            </table>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- end modal form -->
                                                            
                                                                
                                                            </td>
                                                        </tr>
                                                 <?php $x++; endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                							
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
