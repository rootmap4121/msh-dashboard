<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>User Info</a></li><li class='active'>Admin List</li>";
$table="employee";
$w=80; $h=100; $thumb="emp/";
if(isset($_POST['submit']))
{
    if(!empty($_POST['name']) && !empty($_POST['designation']))
    {
        $exist_array=array("name"=>$_POST['name']);
        if(!empty($_FILES['image']['name']))
        {
        $files =$obj->image_bigcaption($w,$h,$thumb);
        $photo=substr($files,4,1600);
        }
        else 
        {
            $photo="";
        }
        
        $insert=array("name"=>$_POST['name'],"staff_id"=>$_POST['staff_id'],"ext_number"=>$_POST['ext_number'],"blood_group"=>$_POST['blood_group'],"designation"=>$_POST['designation'],"branch"=>$_POST['branch'],"photo"=>$photo,"gender"=>$_POST['gender'],"contactnumber"=>$_POST['contact'],"address"=>$_POST['address'],"username"=>$_POST['username'],"password"=>md5($_POST['password']),"date"=>  date('Y-m-d'),"status"=>1);
        
        if($obj->exists($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exist';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        }
    }
    else 
    {
                        $errmsg_arr[]= 'Failed,Some Fields is Empty';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
     }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Employee</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="empadd"  enctype="multipart/form-data" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Employee Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="name" placeholder="Employee Full Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Designation </label>

                                    <div class="col-sm-9">
                                        <select class="width-10" name="designation">
                                            <option value="">Select Designation</option>
                                            <?php  $data=$obj->SelectAllorderBy("designation"); foreach ($data as $row): ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Branch </label>

                                    <div class="col-sm-9">
                                        <select class="width-10" name="branch">
                                            <option value="">Select Branch</option>
                                            <?php  $data=$obj->SelectAllorderBy("branch"); foreach ($data as $row): ?>
                                                <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Gender </label>

                                    <div class="col-sm-9">
                                        <select class="width-10" name="gender">
                                            <option value="">Select Sex</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                         </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Staff ID </label>

                                    <div class="col-sm-9">
                                        <input type="text" name="staff_id" placeholder="Staff id Number">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ext. Number </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="ext_number" placeholder="Ext. Number " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Contact Number </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="contact" placeholder="Contact Number " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email-Address </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="address" placeholder="Email-Address " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Blood Group </label>

                                    <div class="col-sm-9">
                                        <select class="width-10" name="blood_group">
                                            <option value="">Select Blood Group</option>
                                                <option value="A+">A+</option>
                                                <option value="AB+">AB+</option>
                                                <option value="B+">B+</option>
                                                <option value="O+">O+</option>
                                                <option value="A-">A-</option>
                                                <option value="AB-">AB-</option>
                                                <option value="B-">B-</option>
                                                <option value="O-">O-</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Photo </label>

                                    <div class="col-sm-9">
                                        <input type="file" id="form-field-1" name="image" placeholder="Address " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Name </label>

                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="username" placeholder="User Name " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password </label>

                                    <div class="col-sm-9">
                                        <input type="password" id="form-field-1" name="password" placeholder="Password " class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                

                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
