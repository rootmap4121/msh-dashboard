<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Order </a></li><li class='active'>Product Order Form</li>";
$table="field_inventory_order_detail";
$table2="field_inventory_order";
$table3="field_inventory_stockreport";
extract($_GET);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->

                            
                            
                            
                            
                                                   
     <?php $od=$obj->SelectAllByID($table,array("id"=>$id)); foreach($od as $order_detail): ?>
    <div class="space-6"></div>

    <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                    <div class="widget-box transparent invoice-box">
                            <div class="widget-header widget-header-large">
                                    <h3 class="grey lighter pull-left position-relative">
                                            <i class="icon-leaf green"></i>
                                            Product Field Inventory Order Detail
                                    </h3>

                                    <div class="widget-toolbar no-border invoice-info">
                                            <span class="invoice-info-label">Invoice:</span>
                                            <span class="red">#<?php echo $order_detail->orderid; ?> </span>
                                            <input type="hidden" value="<?php echo $order_detail->orderid; ?>" name="orderid">
                                            <br />
                                            <span class="invoice-info-label">Date:</span>
                                            <span class="blue"><?php echo $order_detail->date; ?></span>
                                    </div>

                                    <div class="widget-toolbar hidden-480">
                                            <a href="#">
                                                    <i class="icon-print"></i>
                                            </a>
                                    </div>
                            </div>

                            <div class="widget-body">
                                    <div class="widget-main padding-24">
                                            <div class="row">
                                                    <div class="col-sm-6">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
                                                                            <b>Company Info</b>
                                                                    </div>
                                                            </div>

                                                            <div class="row">
                                                                    <ul class="list-unstyled spaced">
                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    MSH
                                                                            </li>

                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    Admin Name : <?php $admin=$obj->SelectAllByID("employee",array("id"=>$order_detail->emplid)); foreach ($admin as $ad): echo $ad->name;  endforeach;  ?>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->

                                                    <div class="col-sm-6">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
                                                                            <b>Supplier Info</b>
                                                                    </div>
                                                            </div>

                                                            <div>
                                                                    <ul class="list-unstyled  spaced">
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Supplier Name : <?php $supplier=$obj->SelectAllByID("supplier",array("id"=>$order_detail->sid)); foreach($supplier as $sup):  ?>
                                                                                        <?php  echo $sup->name;  ?>
                                                                                        <?php endforeach;  ?>
                                                                            </li>
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Total Order Price : <?php echo $order_detail->totalprice;  ?>
                                                                            </li>                                                                            
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->
                                            </div><!-- row -->

                                            <div class="space"></div>

                                            <div>
                                                    <table class="table table-striped table-bordered">
                                                            <thead>
                                                                    <tr>
                                                                            <th class="center">#</th>
                                                                            <th>Product</th>
                                                                            <th class="hidden-xs">Description</th>
                                                                            <th class="hidden-480">Quantity</th>
                                                                            <th class="hidden-480">Unite Price</th>
                                                                            <th class="hidden-480">Product Price</th>
                                                                            <th class="hidden-480">Action</th>
                                                                            <th class="hidden-480">Report</th>
                                                                    </tr>
                                                            </thead>

                                                            <tbody>
                           <?php
                           if($obj->totalrows($table2)!=0)
                           {
                           $i=1; 
                           $ord=$obj->SelectAllByID($table2,array("orderid"=>$order_detail->orderid)); 
                           foreach($ord as $order): ?>
                            <tr>
                                    <td class="center"><?php echo $i; ?></td>
                                    <td>
                                        <?php $pro=$obj->SelectAllByID("fieldproduct",array("id"=>$order->pid)); foreach($pro as $product): echo $product->name; endforeach; ?>
                                    </td>
                                    <td class="hidden-xs"><?php echo $order->description; ?></td>
                                    <td class="hidden-480"><?php echo $order->quantity; ?></td>
                                    <td class="hidden-480"><?php echo $order->price; ?></td>
                                    
                                    <td class="hidden-480"><?php echo $order->totalprice; ?></td>
                                    <td class="hidden-480">
                                    <?php 
                                    if($obj->existsnewtotal($table3,array("oid"=>$order->id,"pid"=>$order->pid,"orderid"=>$order->orderid))==0)
                                    {
                                        $xp=0;
                                    }
                                    else
                                    {
                                       $exp=$obj->SelectAllByIDDouble($table3,array("oid"=>$order->id,"pid"=>$order->pid,"orderid"=>$order->orderid));
                                        $xp=0;
                                       foreach($exp as $ex):
                                         $xp+=$ex->quantity;
                                       endforeach;
                                    }
                                    ?>
                                        
                                    <?php 
                                    $pendingzero=$order->quantity-$xp;
                                    if($pendingzero!=0)
                                    {
                                    ?>
                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                       <a href="#modal-table<?php echo $order->id; ?>" role="button" data-toggle="modal"><i class="icon-exchange bigger-130"></i> Stock In</a>
                                    </div>
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                    Stock-IN Complete    
                                    <?php } ?>



                                                                <div id="modal-table<?php echo $order->id; ?>" class="modal fade" tabindex="-1">
                                                                    <div class="modal-dialog">
                                                                                <div class="modal-content">
                                                                                        <div class="modal-header no-padding">
                                                                                                <div class="table-header">
                                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                                                                <span class="white">&times;</span>
                                                                                                        </button>
                                                                                                        Stock IN #<?php echo $order->orderid; ?>
                                                                                                </div>
                                                                                        </div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="field_inventory_stockin.php" method="GET">
                                                                                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                                                                                            <input type="hidden" name="orderid" value="<?php echo $order->orderid; ?>">
                                                                                            <input type="hidden" name="sid" value="<?php echo $order->sid; ?>">
                                                                                            <input type="hidden" name="emplid" value="<?php echo $order->emplid; ?>">
                                                                                            <input type="hidden" name="oid" value="<?php echo $order->id; ?>">
                                                                                            <?php $ror=$obj->SelectAllByID("fieldproduct",array("id"=>$order->pid)); foreach($ror as $or): ?>                   
                                                                                            <input type="hidden" name="reorder" value="<?php echo $or->reorder; ?>">
                                                                                            <?php endforeach; ?>                   
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" disabled="disabled" id="form-field-1" name="name" value="<?php $pro=$obj->SelectAllByID("fieldproduct",array("id"=>$order->pid)); foreach($pro as $product): echo $product->name; endforeach; ?>" class="col-xs-10 col-sm-5" />
                                                                                                    <input type="hidden" name="pid" value="<?php echo $order->pid; ?>"  />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <input type="hidden" name="pquantity" value="<?php echo $order->quantity-$xp; ?>">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Pending Quantity </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text"  id="form-field-1" name="quantity" value="<?php echo $order->quantity-$xp; ?>"  class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Unite Price </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" readonly="readonly"  id="form-field-1" name="price" value="<?php echo $order->price; ?>"  class="col-xs-10 col-sm-5" />
                                                                                                </div>
                                                                                            </div>


                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Stock In Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                </div><!-- /.modal-content -->
                                                                        </div><!-- /.modal-dialog -->
                                                                </div><!-- end modal form -->
                                    
                                    
                                    
                                    
                                    
                                    
                                    </td>
                                                                        <td class="hidden-480">
                                        
                                         <?php 
                                         if($obj->existsnewtotal($table3,array("oid"=>$order->id,"pid"=>$order->pid,"orderid"=>$order->orderid))==0)
                                         {
                                         ?>
                                         No Report
                                         <?php
                                         }
                                         else
                                         {
                                         ?>
                                         <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                         <a href="#modal-report<?php echo $order->id; ?>" role="button" data-toggle="modal"><i class="icon-exchange bigger-130"></i> Stock In Report</a>
                                         </div>  
                                         <?php } ?>
                                       
                                       
                                       
                                    
                                    
                                    
                                    
                                                                    <div id="modal-report<?php echo $order->id; ?>" class="modal fade" tabindex="-1">
                                                                        <div class="modal-dialog">
                                                                                    <div class="modal-content">
                                                                                            <div class="modal-header no-padding">
                                                                                                    <div class="table-header">
                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                                                                    <span class="white">&times;</span>
                                                                                                            </button>
                                                                                                            Order Stock In Report#<?php echo $order->orderid; ?>
                                                                                                    </div>
                                                                                            </div>
                                                                                            <!-- /.modal-content -->

                                                                                            <div class="widget-toolbar hidden-480">
                                                                                                                <a href="#">
                                                                                                                        <i class="icon-print"></i>  Print Report
                                                                                                                </a>
                                                                                            </div>
                                                                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                                                                                    <thead>
                                                                                                                        <tr>
                                                                                                                            <th class="center">S/N</th>
                                                                                                                            <th>Product</th>
                                                                                                                            <th>Quantity</th>
                                                                                                                            <th>Price</th>
                                                                                                                            <th>Stock In By</th>
                                                                                                                            <th>Date</th>
                                                                                                                        </tr>
                                                                                                                    </thead>

                                                                                                                    <tbody id="status">
                                                                                                                    <?php
                                                                                                                    $data=$obj->SelectAllByID($table3,array("oid"=>$order->id));
                                                                                                                    $d=1;
                                                                                                                    foreach ($data as $row): ?>
                                                                                                                            <tr>
                                                                                                                                <td class="center"><?php echo $d; ?></td>
                                                                                                                                <td><span class="label label-sm label-info"><?php $pro=$obj->SelectAllByID("fieldproduct",array("id"=>$row->pid)); foreach($pro as $product): echo $product->name; endforeach; ?></span></td>
                                                                                                                                <td><span class="label label-sm label-success"><?php echo $row->quantity; ?></span></td>
                                                                                                                                <td><span class="label label-sm label-info"><?php echo $row->price; ?></span></td>
                                                                                                                                <td><span class="label label-sm label-success"><?php $em=$obj->SelectAllByID("employee",array("id"=>$row->emplid)); foreach($em as $emp): echo $emp->name; endforeach; ?></span></td>
                                                                                                                                <td><?php echo $row->date; ?></td>
                                                                                                                            </tr>
                                                                                                                     <?php $d++; endforeach; ?>


                                                                                                                    </tbody>
                                                                                                                </table>
                                                                                    </div><!-- /.modal-content -->
                                                                            </div><!-- /.modal-dialog -->
                                                                    </div><!-- end modal form -->
                                    
                                    
                                    
                                    
                                    
                                    
                                    </td>
                            
                            </tr>
                            <?php 
                            $i++; 
                            endforeach;
                           }
                            ?>

                                                                    
                                                            </tbody>
                                                    </table>
                                            </div>

                                            
                                    </div>
                            </div>
                    </div>
            </div>
    </div>

    <!-- PAGE CONTENT ENDS -->

    <?php endforeach; ?>                        
                                               
                            
                            
                            
                            
                            
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
