<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Profile Info</a></li><li class='active'>User Profile</li>";
$table="employee";
$table1="employee_basic_info";
$table2="employee_present_address";
$table3="employee_emergency_contact";
$table4="employee_educational_info";
$existemployee=array("emplid"=>$input_by);

if($obj->exists($table1,$existemployee)!=1)
{
	$obj->insert($table1,$existemployee);	
	
	$obj->insert($table2,array("emplid"=>$input_by,"status"=>1));
	$obj->insert($table2,array("emplid"=>$input_by,"status"=>2));	
	
	$obj->insert($table3,array("emplid"=>$input_by,"status"=>1));
	$obj->insert($table3,array("emplid"=>$input_by,"status"=>2));
	
	$obj->insert($table4,$existemployee);		
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
          <style type="text/css">
              table
              {
                 border-radius: 8px;
              }
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
                border-radius: 8px;
	}
	.TFtable td{ 
		padding:3px; border:#dddddd 1px solid;
                                border-radius: 8px;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{

	}
	/*  Define the background color for all the ODD background rows background:#999; */
	.TFtable tr:nth-child(odd){ 
		background: #f9f9f9;
	}
	/*  Define the background color for all the EVEN background rows background:#CCC; */
	.TFtable tr:nth-child(even){
		background: #f5f5f5;
	}
	
	.headtd{
		height:30px;
		line-height:30px;
		font-size:18px;
                background:#428bca;
                color: #FFF;
		}
		
		.subheadtd{
			background: #5a6364;
                        color: #FFF;
			}
			.onlyhead{
				font-weight:bold;
				
				}
</style>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    
                    
          <div class="row">
                <div class="col-xs-12">
                <?php
				$sqlprofile=$obj->SelectAllByID("employee",array("id"=>$input_by));
				if($obj->exists("employee",array("id"=>$input_by))!=0)
				foreach($sqlprofile as $profile):
				?>
				<h1 class="header green">Profile Information</h1>
				<table class="TFtable" width="100%" border="1">
                  <tr>
    <td colspan="5" class="headtd">Basic Information :</td>
    </tr>
    <?php
	$sqlbasic=$obj->SelectAllByID($table1,$existemployee);
	foreach($sqlbasic as $basic):
	?>
  <tr>
    <td width="21%" class="onlyhead">Name :</td>
    <td width="14%"><?php echo $profile->name; ?></td>
    <td width="14%" class="onlyhead">Employee ID :</td>
    <td width="12%"><?php echo $profile->id; ?></td>
    <td width="39%" rowspan="7" align="center" valign="middle"><img style="height:180px; width:190px;" class="btn btn-primary" src="emp/<?php echo $profile->photo; ?>"></td>
  </tr>
  <tr>
    <td class="onlyhead">Fathers / Husband Name :</td>
    <td><?php echo $basic->father_husbandname; ?></td>
    <td class="onlyhead">Date of Birth :</td>
    <td><?php echo $profile->dob; ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Mothers Name :</td>
    <td><?php echo $basic->mothers_name; ?></td>
    <td class="onlyhead">Blood Group :</td>
    <td><?php echo $basic->blood_group; ?></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="onlyhead">Marital Status :</td>
    <td><?php echo $basic->marital_status; ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Religion :</td>
    <td><?php echo $basic->religion; ?></td>
    <td class="onlyhead">Marrige Date :</td>
    <td><?php echo $basic->marrige_date;?></td>
    </tr>
  <tr>
    <td class="onlyhead">Home District :</td>
    <td><?php echo $basic->home_district; ?></td>
    <td class="onlyhead">Relative in SC :</td>
    <td><?php echo $basic->relative_in_sc; ?></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="onlyhead">Relation :</td>
    <td><?php echo $basic->relation; ?></td>
    </tr>
  <tr>
    <td class="onlyhead">TIN Number :</td>
    <td><?php echo $basic->tin_number; ?></td>
    <td class="onlyhead">Driving Licence :</td>
    <td><?php echo $basic->driving_licence; ?></td>
    <td rowspan="2" align="center" valign="middle"><a href="profile_basic_update.php" class="btn btn-primary"> Update Basic Info... </a></td>
    </tr>
  <tr>
    <td class="onlyhead">Passport Number :</td>
    <td><?php echo $basic->passport_number; ?></td>
    <td class="onlyhead">Date of Expire :</td>
    <td><?php echo $basic->date_of_expire; ?></td>
    </tr>
  <?php
  endforeach;
  ?>
  </table>
  <br>
  <table class="TFtable" width="100%" border="1">
  <tr>
    <td colspan="2" class="headtd">Present Address </td>
    <td colspan="3" class="headtd">Permanent Address </td>
  </tr>

  <tr>
    <td width="21%" class="onlyhead">Address :</td>
    <td width="29%"><?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","1","address"); ?></td>
    <td width="23%" class="onlyhead">Address : </td>
    <td width="27%" colspan="2"><?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","2","address"); ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Phone : </td>
    <td><?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","1","phone"); ?></td>
    <td class="onlyhead">Phone : </td>
    <td colspan="2"><?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","2","phone"); ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Fax :</td>
    <td><?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","1","fax"); ?></td>
    <td class="onlyhead">Fax : </td>
    <td colspan="2"><?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","2","phone"); ?></td>
    </tr>
    <tr>
      <td colspan="3"></td>
      <td align="center" valign="middle"><a href="profile_pp_address.php" class="btn btn-primary"> Update Present / Permanent Info... </a></td>
    </tr>
        </table>
  <br>
  <table class="TFtable" width="100%" border="1">
  <tr>
    <td colspan="4" class="headtd">Emergency Contact</td>
    </tr>
  <tr>
    <td colspan="2" class="subheadtd">Contact One.</td>
    <td colspan="2" class="subheadtd">Contact Two.</td>
    </tr>
  <tr>
    <td width="21%" class="onlyhead">Name :</td>
    <td width="29%"><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","1","name"); ?></td>
    <td width="23%" class="onlyhead">Name : </td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","2","name"); ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Address : </td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","1","address"); ?></td>
    <td class="onlyhead">Address : </td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","2","address"); ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Phone : </td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","1","phone"); ?></td>
    <td class="onlyhead">Phone : </td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","2","phone"); ?></td>
    </tr>
  <tr>
    <td class="onlyhead">Relation</td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","1","relation"); ?></td>
    <td class="onlyhead">Relation</td>
    <td><?php echo $obj->SelectAllByVal2($table3,"emplid",$input_by,"status","2","relation"); ?></td>
    </tr>
      <tr>
        <td colspan="3"></td>
        <td align="center" valign="middle"><a href="profile_emergency.php" class="btn btn-primary"> Update Emergency Info... </a></td>
      </tr>
      </table>
<br>
  <table class="TFtable" width="100%" border="1">
  <tr>
    <td colspan="5" class="headtd">Educational Information...</td>
    </tr>
  <tr>
    <td width="25%" height="27" class="subheadtd">Degree Title</td>
    <td width="23%" class="subheadtd">Board</td>
    <td width="11%" class="subheadtd">Year</td>
    <td width="20%" class="subheadtd">Division / Class</td>
    <td width="21%" class="subheadtd">GPA / Grade</td>
    </tr>
    <?php
	$sqledu=$obj->SelectAllByID($table4,$existemployee);
	foreach($sqledu as $edu):
	?>
  <tr>
    <td><?php echo $edu->title; ?></td>
    <td><?php echo $edu->board; ?></td>
    <td><?php echo $edu->year; ?></td>
    <td><?php echo $edu->division; ?></td>
    <td><?php echo $edu->gpa; ?></td>
    </tr>
    <?php
	endforeach;
	 ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="profile_education_info.php" class="btn btn-primary"> Update Educational Info... </a></td>
        </tr>
      </table>
                
                
				<?php
				endforeach;
				?>
            </div>
          </div>  
                    
                    
                    
                    
                    
                    <!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>


		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.hotkeys.min.js"></script>
		

		<!-- ace scripts -->
		<script type="text/javascript">
			jQuery(function($) {
                            
                                                                                        $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			
				$('.easy-pie-chart.percentage').each(function(){
				var barColor = $(this).data('color') || '#555';
				var trackColor = '#E2E2E2';
				var size = parseInt($(this).data('size')) || 72;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate:false,
					size: size
				}).css('color', barColor);
				});
			  
				///////////////////////////////////////////
	
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
			});
                        
                                            function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
		</script>
	</body>

<!-- Mirrored from 192.69.216.111/themes/preview/ace/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Sun, 10 Nov 2013 12:57:55 GMT -->
</html>
