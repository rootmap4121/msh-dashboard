<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Online Leave Application Info</a></li><li class='active'>Add Online Leave Application Form</li>";
$table="requisation_leaveapplication_form";
extract($_GET);
?>
<?php
if(@$_GET['action']=='pdf')
{
include("pdf/MPDF57/mpdf.php");
$html .=$obj->company_report_logo()." ".$obj->company_report_head();
$html .=$obj->company_report_name("Leave Application ");
    $datas=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
    foreach ($datas as $data):
       $html .="<h5>A. PERSONAL INFORMATION (For Applicant)</h5>
                            <table  id='sample-table-2' class='table table-hover' border='1'>
                              <tr>
                                <td valign='top' colspan='3'><strong>Name : </strong>".$obj->SelectAllByVal("employee","id",$data->emplid,"name")."</td>
                                <td valign='top' colspan='2'><strong>Position : </strong>".$obj->emp_designation($data->emplid)."</td>
                                </tr>
                              <tr>
                                <td valign='top' height='38' colspan='2'><strong>ID Number : </strong> ".$data->emplid."</td>
                                <td valign='top' colspan='3'><strong>Purpose/Reason : </strong> ".$data->description."</td>
                                </tr>
                              <tr>
                                <td valign='top' width='335'><strong>Requested Leave </strong> </td>
                                <td valign='top' colspan='2'><strong>From : </strong> ".$data->startdate."</td>
                                <td valign='top' width='229'><strong>To :</strong> ".$data->enddate."</td>
                                <td valign='top' width='228'><strong>No. of Day/Hour: </strong> ".$data->datequantity." Day</td>
                              </tr>
                              <tr>
                                <td valign='top'><strong>Type of Leave :</strong></td>
                                <td valign='top' colspan='4'>".$obj->SelectAllByVal("leave_type","id",$data->leave_type,"name")."</td>
                              </tr>
                              <tr>
                                <td valign='top' height='42'><strong>Contact Address During <br>Leave with telephone No.</strong></td>
                                <td valign='top' colspan='4'>".$data->phone."</td>
                                </tr>
                              <tr>
                                <td valign='top' colspan='4'><strong>Signature of the Application :</strong></td>
                                <td valign='top'><strong>Date :</strong>".$data->date."</td>
                              </tr>
                            </table>";

    $d=$obj->emp_leave_quantity($data->emplid)+$obj->emp_leave_quantity_new($data->emplid);
    $e=$obj->leave_quantity($data->leave_type)-$d;
$html .="<h5>B. OFFICIAL USE ONLY : </h5>
                            
<table  id='sample-table-2' class='table table-hover' border='1'>
  <tr>
    <td valign='top' width='11%'><strong>Type of Leave</strong></td>
    <td valign='top' width='19%'><strong>Available Balance This Year (A)</strong></td>
    <td valign='top' width='18%'><strong>Leave Requested this time (B)</strong></td>
    <td valign='top' width='16%'><strong>Leave Available/Taken (C)</strong></td>
    <td valign='top' width='19%'><strong>Total Taken as of Date D=(B+C)</strong></td>
    <td valign='top' width='17%'><strong>Balance Till Today E=(A-D)</strong></td>
  </tr>
  
  <tr>
    <td valign='top'>".$obj->SelectAllByVal("leave_type","id",$data->leave_type,"name")."</td>
    <td valign='top'>".$obj->leave_quantity($data->leave_type)."</td>
    <td valign='top'>".$obj->emp_leave_quantity_new($data->emplid)."</td>
    <td valign='top'>".$obj->emp_leave_quantity($data->emplid)."</td>
    <td valign='top'>".$d."</td>
    <td valign='top'>".$e."</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>";

$html .="<h5>C. APPROVAL : </h5>
                            
<table  id='sample-table-2' class='table table-hover' border='1'>
  <tr>
    <td valign='top' height='80'><strong>Recommended By ( Supervisor ) :</strong>".$obj->SelectAllByVal("employee","id",$data->emplid,"name")."<br>".$obj->emp_leave_status($data->status)."</td>
    <td valign='top'><strong>Reviewed By ( Admin ) :</strong> ".$obj->SelectAllByVal("employee","status","2","name")."</td>
    <td valign='top' align='left'><strong>Approved By ( Country Project Director ) :</strong></td>
    </tr>
  </table>";
  
$html .="<h5>D. GUIDELINES : </h5>
                            
<table width='100%' border='0'>
  <tr>
    <td height='80'>1. This form must be filled in by all staff &amp; must support any absense from work.
    <br> 2. For annual leave / any other planned leave, please apply 2 week's in advance. 
    <br>
    3. For Casual leave please apply minimum 2 days before. 
    <br>
    4. Sick leave for more than 2 days should be supported by a physician's certificate. 
    <br>
    5. Please delegate someone who will perform your current and most urgent desk work on behalf of you while on leave.</td>
    </tr>
  </table>";

endforeach;

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('leave.pdf','I');
}
elseif(@$_GET['action']=='excel')
{

header('Content-type: application/excel');
$filename = 'leaveapplicationlist.xls';
header('Content-Disposition: attachment; filename='.$filename);

$html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Leave Application List</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$html .="<body>";
$html .=$obj->company_report_head()."Report Creation Date : ".date('d-m-Y H:i:s');
$html .=$obj->company_report_name("Leave Application");

//start table
    $datas=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
    foreach ($datas as $data):
       $html .="<h5>A. PERSONAL INFORMATION (For Applicant)</h5>
                            <table  id='sample-table-2' class='table table-hover' border='1'>
                              <tr>
                                <td valign='top' colspan='3'><strong>Name : </strong>".$obj->SelectAllByVal("employee","id",$data->emplid,"name")."</td>
                                <td valign='top' colspan='2'><strong>Position : </strong>".$obj->emp_designation($data->emplid)."</td>
                                </tr>
                              <tr>
                                <td valign='top' height='38' colspan='2'><strong>ID Number : </strong> ".$data->emplid."</td>
                                <td valign='top' colspan='3'><strong>Purpose/Reason : </strong> ".$data->description."</td>
                                </tr>
                              <tr>
                                <td valign='top' width='335'><strong>Requested Leave </strong> </td>
                                <td valign='top' colspan='2'><strong>From : </strong> ".$data->startdate."</td>
                                <td valign='top' width='229'><strong>To :</strong> ".$data->enddate."</td>
                                <td valign='top' width='228'><strong>No. of Day/Hour: </strong> ".$data->datequantity." Day</td>
                              </tr>
                              <tr>
                                <td valign='top'><strong>Type of Leave :</strong></td>
                                <td valign='top' colspan='4'>".$obj->SelectAllByVal("leave_type","id",$data->leave_type,"name")."</td>
                              </tr>
                              <tr>
                                <td valign='top' height='42'><strong>Contact Address During <br>Leave with telephone No.</strong></td>
                                <td valign='top' colspan='4'>".$data->phone."</td>
                                </tr>
                              <tr>
                                <td valign='top' colspan='4'><strong>Signature of the Application :</strong></td>
                                <td valign='top'><strong>Date :</strong>".$data->date."</td>
                              </tr>
                            </table>";

    $d=$obj->emp_leave_quantity($data->emplid)+$obj->emp_leave_quantity_new($data->emplid);
    $e=$obj->leave_quantity($data->leave_type)-$d;
$html .="<h5>B. OFFICIAL USE ONLY : </h5>
                            
<table  id='sample-table-2' class='table table-hover' border='1'>
  <tr>
    <td valign='top' width='11%'><strong>Type of Leave</strong></td>
    <td valign='top' width='19%'><strong>Available Balance This Year (A)</strong></td>
    <td valign='top' width='18%'><strong>Leave Requested this time (B)</strong></td>
    <td valign='top' width='16%'><strong>Leave Available/Taken (C)</strong></td>
    <td valign='top' width='19%'><strong>Total Taken as of Date D=(B+C)</strong></td>
    <td valign='top' width='17%'><strong>Balance Till Today E=(A-D)</strong></td>
  </tr>
  
  <tr>
    <td valign='top'>".$obj->SelectAllByVal("leave_type","id",$data->leave_type,"name")."</td>
    <td valign='top'>".$obj->leave_quantity($data->leave_type)."</td>
    <td valign='top'>".$obj->emp_leave_quantity_new($data->emplid)."</td>
    <td valign='top'>".$obj->emp_leave_quantity($data->emplid)."</td>
    <td valign='top'>".$d."</td>
    <td valign='top'>".$e."</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>";

$html .="<h5>C. APPROVAL : </h5>
                            
<table  id='sample-table-2' class='table table-hover' border='1'>
  <tr>
    <td valign='top' height='80'><strong>Recommended By ( Supervisor ) :</strong>".$obj->SelectAllByVal("employee","id",$data->emplid,"name")."<br>".$obj->emp_leave_status($data->status)."</td>
    <td valign='top'><strong>Reviewed By ( Admin ) :</strong> ".$obj->SelectAllByVal("employee","status","2","name")."</td>
    <td valign='top'><strong>Approved By ( Country Project Director ) :</strong></td>
    </tr>
  </table>";
  
$html .="<h5>D. GUIDELINES : </h5>
                            
<table width='100%' border='0'>
  <tr>
    <td height='80'>1. This form must be filled in by all staff &amp; must support any absense from work.
    <br> 2. For annual leave / any other planned leave, please apply 2 week's in advance. 
    <br>
    3. For Casual leave please apply minimum 2 days before. 
    <br>
    4. Sick leave for more than 2 days should be supported by a physician's certificate. 
    <br>
    5. Please delegate someone who will perform your current and most urgent desk work on behalf of you while on leave.</td>
    </tr>
  </table>";

endforeach;
//end table

$html .="</tbody></table>";

$html .='</body></html>';

echo $html;

}
elseif(@$_GET['action']=='approve')
{
    $array=array("id"=>$_GET['id'],"status"=>2);
    if($obj->update($table,$array)==1)
    {
		$obj->notification_check_user_update($_GET['id'],"4",$input_by);
                    $errmsg_arr[]= 'Successfully Approved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
elseif(@$_GET['action']=='reject')
{
    $array=array("id"=>$_GET['id'],"status"=>3);
    if($obj->update($table,$array)==1)
    {
		
		$obj->notification_check_user_update($_GET['id'],"4",$input_by);
                    $errmsg_arr[]= 'Successfully Rejected';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Individual Leave Application Form 
                         <a target="_blank" href="<?php echo $obj->filename(); ?>?action=pdf&AMP;id=<?php echo $id; ?>"><img src="images/pdf.png"></a> 
                                <a target="_blank" href="<?php echo $obj->filename(); ?>?action=excel&AMP;id=<?php echo $id; ?>"><img src="images/excel.png"></a>
                                
                         <span style="float: right;"><a href="#" target="_blank" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                           
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row" id="printablediv">
                        
                        
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                                                                <div>
                                
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Leave Application"); ?>
                                
                                    </div>
                            <?php
                            $datas=$obj->SelectAllByID($table,array("id"=>$id));
                            foreach ($datas as $data):
                            ?>
                          <h3>A. PERSONAL INFORMATION (For Applicant)</h3>
                            <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                              <tr>
                                <td valign='top' colspan='3'><strong>Name : </strong><?php echo $obj->SelectAllByVal("employee","id",$data->emplid,"name");
                                    ?></td>
                                <td valign='top' colspan='2'><strong>Position : </strong>
                                  <?php 
                                    $empsql=$obj->SelectAllByID("employee",array("id"=>$data->emplid));
                                    foreach($empsql as $emp):
										$empsql=$obj->SelectAllByID("designation",array("id"=>$emp->designation));
										foreach($empsql as $emp):
											echo $emp->name;
										endforeach;
                                    endforeach;								  

                                    
                                    
                                    ?>
                                </td>
                                </tr>
                              <tr>
                                <td valign='top' height='38' colspan='2'><strong>ID Number : </strong> <?php echo $data->emplid; ?></td>
                                <td valign='top' colspan='3'><strong>Purpose/Reason : </strong> <?php echo $data->description; ?></td>
                                </tr>
                              <tr>
                                <td valign='top' width='335'><strong>Requested Leave </strong> </td>
                                <td valign='top' colspan='2'><strong>From : </strong> <?php echo $data->startdate; ?></td>
                                <td valign='top' width='229'><strong>To :</strong> <?php echo $data->enddate; ?></td>
                                <td valign='top' width='228'><strong>No. of Day/Hour: </strong>  <?php echo $data->datequantity; ?> Day</td>
                              </tr>
                              <tr>
                                <td valign='top'><strong>Type of Leave :</strong></td>
                                <td valign='top' colspan='4'><?php echo $obj->SelectAllByVal("leave_type","id",$data->leave_type,"name");
                                    ?></td>
                              </tr>
                              <tr>
                                <td valign='top' height='42'><strong>Contact Address During <br>Leave with telephone No.</strong></td>
                                <td valign='top' colspan='4'><?php echo $data->phone;  ?></td>
                                </tr>
                              <tr>
                                <td valign='top' colspan='4'><strong>Signature of the Application :</strong></td>
                                <td valign='top'><strong>Date :</strong><?php echo $emp->date;  ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>B. OFFICIAL USE ONLY : </h3>
                            
<table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td valign='top' width='11%'><strong>Type of Leave</strong></td>
    <td valign='top' width='19%'><strong>Available Balance This Year (A)</strong></td>
    <td valign='top' width='18%'><strong>Leave Requested this time (B)</strong></td>
    <td valign='top' width='16%'><strong>Leave Available/Taken (C)</strong></td>
    <td valign='top' width='19%'><strong>Total Taken as of Date D=(B+C)</strong></td>
    <td valign='top' width='17%'><strong>Balance Till Today E=(A-D)</strong></td>
  </tr>
  
  <tr>
    <td valign='top'><?php echo $obj->SelectAllByVal("leave_type","id",$data->leave_type,"name");
				?></td>
    <td valign='top'><?php echo $obj->leave_quantity($data->leave_type); ?></td>
    <td valign='top'><?php echo $obj->emp_leave_quantity_new($data->emplid); ?></td>
    <td valign='top'><?php echo $obj->emp_leave_quantity($data->emplid); ?></td>
    <td valign='top'><?php echo $obj->emp_leave_quantity($data->emplid)+$obj->emp_leave_quantity_new($data->emplid); ?></td>
    <td valign='top'><?php echo $obj->leave_quantity($data->leave_type)-($obj->emp_leave_quantity($data->emplid)+$obj->emp_leave_quantity_new($data->emplid)); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 
</table>

<br>

<h3>C. APPROVAL : </h3>
                            
<table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td valign='top' height='80'><strong>Recommended By ( Supervisor ) :</strong>
    <br /><?php echo $obj->SelectAllByVal("employee","id",$data->supervisor,"name");
    
    if($data->supervisor==$input_by)
    {
        if($data->status==1)
        {
        ?>
        <a href="<?php echo $obj->filename(); ?>?action=approve&AMP;id=<?php echo $id; ?>">   <span class="label label-sm label-warning"> Approve </span></a>
        <a href="<?php echo $obj->filename(); ?>?action=reject&AMP;id=<?php echo $id; ?>">   <span class="label label-sm label-danger"> Reject </span></a>
        <?php 
        }
        elseif($data->status==2){
        ?>
        <span class="label label-sm label-success"> <?php echo $obj->emp_leave_status($data->status); ?></span>
        <?php
        }
    }
    else
    {
         echo $obj->emp_leave_status($data->status); 
    }
    ?>
    </td>
    <td valign='top'><strong>Reviewed By ( Admin ) :</strong> <?php echo $obj->SelectAllByVal("employee","status","2","name");  ?></td>
    <td valign='top'><strong>Approved By ( Country Project Director ) :</strong></td>
    </tr>
  </table>
  
  
  <br>

<h3>D. GUIDELINES : </h3>
                            
<table width='100%' border='0'>
  <tr>
    <td height='80'>1. This form must be filled in by all staff &amp; must support any absense from work.
    <br> 2. For annual leave / any other planned leave, please apply 2 week's in advance. 
    <br>
    3. For Casual leave please apply minimum 2 days before. 
    <br>
    4. Sick leave for more than 2 days should be supported by a physician's certificate. 
    <br>
    5. Please delegate someone who will perform your current and most urgent desk work on behalf of you while on leave.</td>
    </tr>
  </table>

                            
                           
                            <?php endforeach; ?>    								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
    </body>
</html>
