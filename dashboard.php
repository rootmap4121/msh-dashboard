<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Dashboard</a></li>";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
  <style type="text/css">
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:3px; border:#4e95f4 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{
		background: #D19500;
	}
	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		background: #f1da36;
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background: #EFEACB;
	}
</style>
</head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
 if($_SESSION['SESS_AMSIT_EMP_STATUS']==3){
	 
	  ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            
                                
                            
                                <div class="row">
                                    

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body darkpink topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/1.jpg" height="140" width="284">
                                                            </div>
                                                    </div>
                                                <div class="widget-body darkred" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> This Month Holidays</h3>
                                                             <ul class="list-unstyled spaced2 lighter black">
                                                                           <?php
                                                                           $datas=$obj->SelectAllByID("holiday_news",array("month"=>date('m')));
																		   if($obj->exists("holiday_news",array("month"=>date('m')))!=0)
                                                                           foreach($datas as $data):
                                                                           ?> 
                                                                            <li>
                                                                                <i class="icon-calendar-empty balck"></i>
                                                                                <?php echo $data->news; ?>-<?php echo $data->holiday_date; ?>
                                                                            </li>
                                                                            <?php endforeach; ?>
                                                                    </ul>
                                                        </div>
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> Notice </h3>
                                                             <ul class="list-unstyled spaced2 lighter black">
                                                                           <?php
                                                                           $datas=$obj->SelectAllByID("notice",array("month"=>date('m')));
																		   if($obj->exists("notice",array("month"=>date('m')))!=0)
                                                                           foreach($datas as $data):
                                                                           ?> 
                                                                            <li>
                                                                                <i class="icon-calendar-empty balck"></i>
                                                                                <?php echo $data->news; ?>-<?php echo $data->holiday_date; ?>
                                                                            </li>
                                                                            <?php endforeach; ?>
                                                                    </ul>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body dark_orange topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/2.jpg" height="140" width="290">
                                                            </div>
                                                    </div>
                                                <div class="widget-body orange_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black">HR Designation Wise</h3>
                                                             <table class="TFtable">
                                                                 <tr><td style="background: #D19500;">Designation</td>
                                                                     <td style="background: #D19500;">Male</td>
                                                                     <td style="background: #D19500;">Female</td>
                                                                     <td style="background: #D19500;">Total</td>
                                                                 </tr>
                                                                 <?php
                                                                 $depsql=$obj->SelectAll("designation");
                                                                 $x=1;
                                                                 foreach($depsql as $dep):

                                                                 ?>
                                                                 <tr>
                                                                     <td><?php echo $x.". "; echo $dep->name; ?></td>
                                                                     <td><?php
                                                                     $sqlmalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",1);
                                                                     if($sqlmalecount!=0)
                                                                     {
                                                                        $sqlmale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",1);
                                                                        $male=0;
                                                                        foreach($sqlmale as $mal):
                                                                            $male+=1;
                                                                        endforeach;
                                                                     }
                                                                     else
                                                                     {
                                                                         $male=0;
                                                                     }
                                                                     echo $male;
                                                                     ?></td>
                                                                     <td>
                                                                     <?php
                                                                     $sqlfemalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",2);
                                                                     if($sqlfemalecount!=0)
                                                                     {
                                                                        $sqlfemale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",2);
                                                                        $female=0;
                                                                        foreach($sqlfemale as $femal):
                                                                            $female+=1;
                                                                        endforeach;
                                                                     }
                                                                     else 
                                                                     {
                                                                         $female=0;
                                                                     }
                                                                     echo $female;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE designation='".$dep->id."'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                                 <?php
                                                                 $x++;
                                                                 endforeach; 
                                                                 ?>
                                                                 <tr>
                                                                     <td>Grand Total </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='1'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='2'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                            </table>
                                                      </div>
                                                    </div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-sm-6 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                <div class="widget-body dark_green topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/3.jpeg" height="140" width="589">
                                                            </div>
                                                    </div>
                                                <div class="widget-body green_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> 
                                                                 <?php echo date('D, d M Y H:i');  ?>
                                                             </h3>
                                                             <h3 class="green"> 
                                                                 Welcome To Dashboard , Mr. 
                                                                     <?php 
                                                                     $sqlemp=$obj->SelectAllByID("employee",array("id"=>$input_by));
                                                                     foreach($sqlemp as $emp):
                                                                         echo $emp->name;
                                                                     endforeach;
                                                                     ?> What You Like to do Today... 
                                                             </h3>
                                                             <ul class="list-unstyled green">
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Leave Status :
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Mediacal &AMP; Hospital Benefits
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Other Benefits Status :
                                                                                </a>
                                                                            </li>                                                                              
                                                            </ul>
                                                            <br>
                                                            <a href="dashboard_employee.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/cansolate.png" class="dash_img">
                                                                <div class="dash_label">Employee</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_requisation.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/preferences_contact_list.png" class="dash_img">
                                                                <div class="dash_label"> Requisition </div>
                                                            </a>
                                                            
                                                            <a href="dashboard_product.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/Products.png" class="dash_img">
                                                                <div class="dash_label">Products</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_order.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/Order_form.png" class="dash_img">
                                                                <div class="dash_label">Order</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_stock.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/database_active.png" class="dash_img">
                                                                <div class="dash_label">Stock</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_report.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/pentest-report-icon.png" class="dash_img">
                                                                <div class="dash_label">Report</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_setting.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/system_preferences.png" class="dash_img">
                                                                <div class="dash_label">Setting Up</div>
                                                            </a>
                                                            
                                                            <a href="contacts.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/apple_festival_app_address.png" class="dash_img">
                                                                <div class="dash_label">Contacts</div>
                                                            </a>
                                                            <a href="export_db.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/online_backup.png" class="dash_img">
                                                                <div class="dash_label">Backup</div>
                                                            </a>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <?php }elseif($_SESSION['SESS_AMSIT_EMP_STATUS']==2){ ?>
                        <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            
                                
                            
                                <div class="row">
                                    

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body darkpink topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/1.jpg" height="140" width="284">
                                                            </div>
                                                    </div>
                                                <div class="widget-body darkred" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> This Month Holidays</h3>
                                                             <ul class="list-unstyled spaced2 lighter black">
                                                                           <?php
                                                                           $datas=$obj->SelectAllByID("holiday_news",array("month"=>date('m')));
																		   if($obj->exists("holiday_news",array("month"=>date('m')))!=0)
                                                                           foreach($datas as $data):
                                                                           ?> 
                                                                            <li>
                                                                                <i class="icon-calendar-empty balck"></i>
                                                                                <?php echo $data->news; ?>-<?php echo $data->holiday_date; ?>
                                                                            </li>
                                                                            <?php endforeach; ?>
                                                                    </ul>
                                                        </div>
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> Notice </h3>
                                                             <ul class="list-unstyled spaced2 lighter black">
                                                                           <?php
                                                                           $datas=$obj->SelectAllByID("notice",array("month"=>date('m')));
																		   if($obj->exists("notice",array("month"=>date('m')))!=0)
                                                                           foreach($datas as $data):
                                                                           ?> 
                                                                            <li>
                                                                                <i class="icon-calendar-empty balck"></i>
                                                                                <?php echo $data->news; ?>-<?php echo $data->holiday_date; ?>
                                                                            </li>
                                                                            <?php endforeach; ?>
                                                                    </ul>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body dark_orange topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/2.jpg" height="140" width="290">
                                                            </div>
                                                    </div>
                                                <div class="widget-body orange_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black">HR Designation Wise</h3>
                                                             <table class="TFtable">
                                                                 <tr><td style="background: #D19500;">Designation</td>
                                                                     <td style="background: #D19500;">Male</td>
                                                                     <td style="background: #D19500;">Female</td>
                                                                     <td style="background: #D19500;">Total</td>
                                                                 </tr>
                                                                 <?php
                                                                 $depsql=$obj->SelectAll("designation");
                                                                 $x=1;
                                                                 foreach($depsql as $dep):

                                                                 ?>
                                                                 <tr>
                                                                     <td><?php echo $x.". "; echo $dep->name; ?></td>
                                                                     <td><?php
                                                                     $sqlmalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",1);
                                                                     if($sqlmalecount!=0)
                                                                     {
                                                                        $sqlmale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",1);
                                                                        $male=0;
                                                                        foreach($sqlmale as $mal):
                                                                            $male+=1;
                                                                        endforeach;
                                                                     }
                                                                     else
                                                                     {
                                                                         $male=0;
                                                                     }
                                                                     echo $male;
                                                                     ?></td>
                                                                     <td>
                                                                     <?php
                                                                     $sqlfemalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",2);
                                                                     if($sqlfemalecount!=0)
                                                                     {
                                                                        $sqlfemale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",2);
                                                                        $female=0;
                                                                        foreach($sqlfemale as $femal):
                                                                            $female+=1;
                                                                        endforeach;
                                                                     }
                                                                     else 
                                                                     {
                                                                         $female=0;
                                                                     }
                                                                     echo $female;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE designation='".$dep->id."'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                                 <?php
                                                                 $x++;
                                                                 endforeach; 
                                                                 ?>
                                                                 <tr>
                                                                     <td>Grand Total </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='1'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='2'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                            </table>
                                                      </div>
                                                    </div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-sm-6 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                <div class="widget-body dark_green topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/3.jpeg" height="140" width="589">
                                                            </div>
                                                    </div>
                                                <div class="widget-body green_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> 
                                                                 <?php echo date('D, d M Y H:i');  ?>
                                                             </h3>
                                                             <h3 class="green"> 
                                                                 Welcome To Dashboard , Mr. 
                                                                     <?php 
                                                                     $sqlemp=$obj->SelectAllByID("employee",array("id"=>$input_by));
                                                                     foreach($sqlemp as $emp):
                                                                         echo $emp->name;
                                                                     endforeach;
                                                                     ?> What You Like to do Today... 
                                                             </h3>
                                                             <ul class="list-unstyled green">
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Leave Status :
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Mediacal &AMP; Hospital Benefits
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Other Benefits Status :
                                                                                </a>
                                                                            </li>                                                                              
                                                            </ul>
                                                            <br>
                                                            <a href="dashboard_employee.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/cansolate.png" class="dash_img">
                                                                <div class="dash_label">Employee</div>
                                                                <span class="notification"><?php echo $obj->totalnotification("notification"); ?></span>
                                                            </a>
                                                            
                                                            <a href="dashboard_requisation.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/preferences_contact_list.png" class="dash_img">
                                                                <div class="dash_label"> Requisition </div>
                                                                <span class="notification"><?php echo $obj->totalnotification("notification"); ?></span>
                                                            </a>
                                                            
                                                            <a href="dashboard_product.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/Products.png" class="dash_img">
                                                                <div class="dash_label">Products</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_order.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/Order_form.png" class="dash_img">
                                                                <div class="dash_label">Order</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_stock.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/database_active.png" class="dash_img">
                                                                <div class="dash_label">Stock</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_report.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/pentest-report-icon.png" class="dash_img">
                                                                <div class="dash_label">Report</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_setting.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/system_preferences.png" class="dash_img">
                                                                <div class="dash_label">Setting Up</div>
                                                            </a>
                                                            
                                                            <a href="contacts.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/apple_festival_app_address.png" class="dash_img">
                                                                <div class="dash_label">Contacts</div>
                                                            </a>
                                                            <a href="export_db.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/online_backup.png" class="dash_img">
                                                                <div class="dash_label">Backup</div>
                                                            </a>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <?php }elseif($_SESSION['SESS_AMSIT_EMP_STATUS']==1){ ?>
                        
                        <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            
                                
                            
                                <div class="row">
                                    

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body darkpink topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/1.jpg" height="140" width="284">
                                                            </div>
                                                    </div>
                                                <div class="widget-body darkred" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> This Month Holidays</h3>
                                                             <ul class="list-unstyled spaced2 lighter black">
                                                                           <?php
                                                                           $datas=$obj->SelectAllByID("holiday_news",array("month"=>date('m')));
														if($obj->exists("holiday_news",array("month"=>date('m')))!=0)
                                                                           foreach($datas as $data):
                                                                           ?> 
                                                                            <li>
                                                                                <i class="icon-calendar-empty balck"></i>
                                                                                <?php echo $data->news; ?>-<?php echo $data->holiday_date; ?>
                                                                            </li>
                                                                            <?php endforeach; ?>
                                                                    </ul>
                                                        </div>
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> Notice </h3>
                                                             <ul class="list-unstyled spaced2 lighter black">
                                                                           <?php
                                                                           $datas=$obj->SelectAllByID("notice",array("month"=>date('m')));
													if($obj->exists("notice",array("month"=>date('m')))!=0)
                                                                           foreach($datas as $data):
                                                                           ?> 
                                                                            <li>
                                                                                <i class="icon-calendar-empty balck"></i>
                                                                                <?php echo $data->news; ?>-<?php echo $data->holiday_date; ?>
                                                                            </li>
                                                                            <?php endforeach; ?>
                                                                    </ul>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body dark_orange topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/2.jpg" height="140" width="290">
                                                            </div>
                                                    </div>
                                                <div class="widget-body orange_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black">HR Designation Wise</h3>
                                                             <table class="TFtable">
                                                                 <tr><td style="background: #D19500;">Designation</td>
                                                                     <td style="background: #D19500;">Male</td>
                                                                     <td style="background: #D19500;">Female</td>
                                                                     <td style="background: #D19500;">Total</td>
                                                                 </tr>
                                                                 <?php
                                                                 $depsql=$obj->SelectAll("designation");
                                                                 $x=1;
                                                                 foreach($depsql as $dep):

                                                                 ?>
                                                                 <tr>
                                                                     <td><?php echo $x.". "; echo $dep->name; ?></td>
                                                                     <td><?php
                                                                     $sqlmalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",1);
                                                                     if($sqlmalecount!=0)
                                                                     {
                                                                        $sqlmale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",1);
                                                                        $male=0;
                                                                        foreach($sqlmale as $mal):
                                                                            $male+=1;
                                                                        endforeach;
                                                                     }
                                                                     else
                                                                     {
                                                                         $male=0;
                                                                     }
                                                                     echo $male;
                                                                     ?></td>
                                                                     <td>
                                                                     <?php
                                                                     $sqlfemalecount=$obj->SelectAllByCountMDI("employee","designation",$dep->id,"gender",2);
                                                                     if($sqlfemalecount!=0)
                                                                     {
                                                                        $sqlfemale=$obj->SelectAllByMDI("employee","designation",$dep->id,"gender",2);
                                                                        $female=0;
                                                                        foreach($sqlfemale as $femal):
                                                                            $female+=1;
                                                                        endforeach;
                                                                     }
                                                                     else 
                                                                     {
                                                                         $female=0;
                                                                     }
                                                                     echo $female;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE designation='".$dep->id."'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                                 <?php
                                                                 $x++;
                                                                 endforeach; 
                                                                 ?>
                                                                 <tr>
                                                                     <td>Grand Total </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='1'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee WHERE gender='2'";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $con=$obj->open();
                                                                     $sql="SELECT * FROM employee";
                                                                     $sqlcount=  mysqli_num_rows(mysqli_query($con, $sql));
                                                                     $obj->close($con);
                                                                     $deps=$sqlcount;                                                                       
                                                                     echo $deps;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                            </table>
                                                      </div>
                                                    </div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-sm-6 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                <div class="widget-body dark_green topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/3.jpeg" height="140" width="589">
                                                            </div>
                                                    </div>
                                                <div class="widget-body green_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> 
                                                                 <?php echo date('D, d M Y H:i');  ?>
                                                             </h3>
                                                             <h3 class="green"> 
                                                                 Welcome To Dashboard , Mr. 
                                                                     <?php 
                                                                     $sqlemp=$obj->SelectAllByID("employee",array("id"=>$input_by));
                                                                     foreach($sqlemp as $emp):
                                                                         echo $emp->name;
                                                                     endforeach;
                                                                     ?> What You Like to do Today... 
                                                             </h3>
                                                             <ul class="list-unstyled green">
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Leave Status :
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Mediacal &AMP; Hospital Benefits
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#" class="bolder balck item-black default" style="text-decoration: none;">
                                                                                    <i class="icon-plus-sign-alt balck"></i> 
                                                                                    My Other Benefits Status :
                                                                                </a>
                                                                            </li>                                                                              
                                                            </ul>
                                                            <br>
                                                            <a href="dashboard_employee.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/cansolate.png" class="dash_img">
                                                                <div class="dash_label">Employee</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_requisation.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/preferences_contact_list.png" class="dash_img">
                                                                <div class="dash_label"> Requisition </div>
                                                            </a>
                                                            
                                                            <a href="dashboard_product.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/Products.png" class="dash_img">
                                                                <div class="dash_label">Products</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_order.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/Order_form.png" class="dash_img">
                                                                <div class="dash_label">Order</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_stock.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/database_active.png" class="dash_img">
                                                                <div class="dash_label">Stock</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_report.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/pentest-report-icon.png" class="dash_img">
                                                                <div class="dash_label">Report</div>
                                                            </a>
                                                            
                                                            <a href="dashboard_setting.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/system_preferences.png" class="dash_img">
                                                                <div class="dash_label">Setting Up</div>
                                                            </a>
                                                            
                                                            <a href="contacts.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/apple_festival_app_address.png" class="dash_img">
                                                                <div class="dash_label">Contacts</div>
                                                            </a>
                                                            <a href="export_db.php" class="col-sm-3 dash_panel">
                                                                <img src="images/icons/online_backup.png" class="dash_img">
                                                                <div class="dash_label">Backup</div>
                                                            </a>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="space-6"></div>


                                <div class="col-sm-6">
                                        <div class="widget-box">
                                                <div class="widget-header widget-header-flat widget-header-small">
                                                        <h5>
                                                                <i class="icon-signal"></i>
                                                                Activity
                                                        </h5>

                                                </div>

                                                <div class="widget-body">
                                                        <div class="widget-main">
                                                                <div id="piechart-placeholder"></div>

                                                                <div class="hr hr8 hr-double"></div>
                                                        </div><!-- /widget-main -->
                                                </div><!-- /widget-body -->
                                        </div><!-- /widget-box -->
                                </div><!-- /span -->
                    <div class="col-sm-6">
                            <div class="widget-box transparent">
                                    <div class="widget-header widget-header-flat">
                                            <h4 class="lighter">
                                                    <i class="icon-signal"></i>
                                                    Sale Stats
                                            </h4>

                                            <div class="widget-toolbar">
                                                    <a href="#" data-action="collapse">
                                                            <i class="icon-chevron-up"></i>
                                                    </a>
                                            </div>
                                    </div>

                                    <div class="widget-body">
                                            <div class="widget-main padding-4">
                                                    <div id="sales-charts"></div>
                                            </div><!-- /widget-main -->
                                    </div><!-- /widget-body -->
                            </div><!-- /widget-box -->
                    </div>
                        </div><!-- /row -->
                        <?php } ?>
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->

<?php
//include('class/colornnavsetting.php');
include('class/footer.php');
?>


                <?php //echo $obj->bodyfooter(); ?>

		<!--[if !IE]> -->

		<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
						size: size
					});
				})
			
				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html', {tagValuesAttribute:'data-values', type: 'bar', barColor: barColor , chartRangeMin:$(this).data('min') || 0} );
				});
			
			
			
			
			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "Leave Application",  data: <?php echo $obj->emp_leave_quantity($input_by); ?>, color: "#68BC31"},
				{ label: "Store Requisation",  data: <?php echo $obj->totalrowsbyDID("requisationlist","empid",$input_by); ?>, color: "#2091CF"},
				{ label: "Vehecle Requisation",  data: <?php echo $obj->totalrowsbyDID("requisationlist_vehicle","empid",$input_by); ?>, color: "#AF4E96"},
				{ label: "Total Stock Out",  data: <?php echo $obj->totalrowsbyDID("stockoutreport","emplid",$input_by); ?>, color: "#DA5430"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
			
			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);
			
			
			
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}
				
			 });
			
			
			
			
			
			
				var d1 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d1.push([i, Math.sin(i)]);
				}
			
				var d2 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d2.push([i, Math.cos(i)]);
				}
			
				var d3 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.2) {
					d3.push([i, Math.tan(i)]);
				}
				
			
				var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
				$.plot("#sales-charts", [
					{ label: "Domains", data: d1 },
					{ label: "Hosting", data: d2 },
					{ label: "Services", data: d3 }
				], {
					hoverable: true,
					shadowSize: 0,
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						tickLength: 0
					},
					yaxis: {
						ticks: 10,
						min: -2,
						max: 2,
						tickDecimals: 3
					},
					grid: {
						backgroundColor: { colors: [ "#fff", "#fff" ] },
						borderWidth: 1,
						borderColor:'#555'
					}
				});
			
			
				$('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('.tab-content')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			
			
				$('.dialogs,.comments').slimScroll({
					height: '300px'
			    });
				
				
				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				});
			
				$('#tasks').sortable({
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'draggable-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					stop: function( event, ui ) {//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
						$(ui.item).css('z-index', 'auto');
					}
					}
				);
				$('#tasks').disableSelection();
				$('#tasks input:checkbox').removeAttr('checked').on('click', function(){
					if(this.checked) $(this).closest('li').addClass('selected');
					else $(this).closest('li').removeClass('selected');
				});
				
			
			})
		</script>
                </body>
                </html>
