<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>User Security</a></li><li class='active'>Change Password</li>";
$table="password_history";
if(isset($_POST['submit']))
{
    if(!empty($_POST['oldpassword']) && !empty($_POST['newpassword']) && !empty($_POST['retypepassword']))
    {
        if($_POST['newpassword']==$_POST['retypepassword'])
        {

           if($obj->existsnewtotal("employee",array("password"=>  md5($_POST['oldpassword']),"id"=>$input_by))!=0)
            {
                    
               $insertarray=array("emplid"=>$input_by,"password"=>$_POST['newpassword'],"date"=>  date('Y-m-d'),"status"=>1);
               $updatearray=array("id"=>$input_by,"password"=>md5($_POST['newpassword']));
               $obj->update($table,array("emplid"=>$input_by,"status"=>2));
               $obj->insert($table,$insertarray);
               if($obj->update("employee",$updatearray)==1)
               {
               $errmsg_arr[]= 'ok Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
               }
               else
               {
                   $errmsg_arr[]= 'Failed,Try Again';
                   $errflag = true;
                   if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
               }
            }
            else 
            {
                   $errmsg_arr[]= 'Failed';
                   $errflag = true;
                   if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
           }
        }
        else
        {
                        $errmsg_arr[]= 'Password Mismatch';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
                    
        }
 else {
                        $errmsg_arr[]= 'Fields is Empty';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
    }   
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Change Password</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Current Password </label>

                                    <div class="col-sm-9">
                                        <input type="password" id="form-field-1" name="oldpassword" placeholder="Current Password" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> New Password </label>

                                    <div class="col-sm-9">
                                        <input type="password" id="form-field-1" name="newpassword" placeholder="New Password" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Re-Type Password </label>

                                    <div class="col-sm-9">
                                        <input type="password" id="form-field-1" name="retypepassword" placeholder="Re-Type Password" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>

                                
                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-key bigger-110"></i>Change Password </button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    				<div class="hr hr-18 dotted hr-double"></div>           

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Password Change History List</h3>
                                        <div class="table-responsive">
                                            <table id="sample-table-2" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Password</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                $data=$obj->SelectAllByID($table,array("emplid"=>$input_by));
                                                if(!empty($data))
                                                    $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php $lenth=strlen($row->password); echo substr($row->password,0,1); for($i=1; $i<=$lenth-1; $i++): echo "*"; endfor; ?></td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->date; ?></span></td>
                                                            <td><span class="label label-sm label-info"><?php if($row->status==1){ echo "Current Password"; }else{ echo "Prevous Password"; } ?></span></td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                   
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
