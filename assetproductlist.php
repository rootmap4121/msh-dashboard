<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Asset Product List</a></li><li class='active'>Add Asset Product</li>";
$table="assetproduct";

if (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $updatearray=array("id"=>$_POST['id'],"name"=>$_POST['name'],
                    "barcode"=>$_POST['barcode'],
                    "assettype"=>$_POST['assettype'],
                    "msn"=>$_POST['msn'],
                    "sid"=>$_POST['sid'],
                    "datepr"=>$_POST['datepr'],
                    "bdtcost"=>$_POST['bdtcost'],
                    "uscost"=>$_POST['uscost'],
                    "dollarrate"=>$_POST['dollarrate'],
                    "voucher_po"=>$_POST['voucher_po'],
                    "itemofficeyn"=>$_POST['itemofficeyn'],
                    "location"=>$_POST['location'],
                    "itemcondition"=>$_POST['itemcondition'],
                    "project"=>$_POST['project'],
                    "warranty"=>$_POST['warranty'],
                    "disposition"=>$_POST['disposition'],
                    "lpi"=>$_POST['lpi'],
                    "bywho"=>$_POST['bywho'],"remarks"=>$_POST['remarks'],"date"=>date('Y-m-d'),"status"=>1);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[] = 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                    } 
                    else 
                    { 
                            $errmsg_arr[]= 'Field is Empty';
                            $errflag = true;
                            if ($errflag) 
                            {
                                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                                session_write_close();
                                header("location: ./".$obj->filename());
                                exit();
                            } 
                        
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[] = 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                    } 
                    else 
                    { 
                            $errmsg_arr[]= 'Field is Empty';
                            $errflag = true;
                            if ($errflag) 
                            {
                                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                                session_write_close();
                                header("location: ./".$obj->filename());
                                exit();
                            } 
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
		
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    
                    
                    
                    
                    
                    
                    
                    
                    <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Asset Product List <span style="margin-left: 20px;"><a  href="#modal-tablesearch" role="button" data-toggle="modal" class="green"><i class="icon-camera-retro"></i> Search in Multiple Dates</a></span> <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                                        <div id="modal-tablesearch" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Report Using Multiple Date
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <br>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="strdate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ending Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="enddate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>                                      


                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="search"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->

                                        
                                        <div class="table-header">
                                          <fieldset>
                                                                        <input type="text" class="text-input" style="width: 200px;" id="topsix"  placeholder="Please Search Anything.."  />
                                                                        <span id="topsix-count"></span>
                                                            </fieldset>
                                        </div>

                                        <div class="table-responsive" id="printablediv">
                                            <div>
                                            <?php echo $obj->company_report_logo(); ?>
                                            <?php echo $obj->company_report_head(); ?>
                                            <?php echo $obj->company_report_name("Asset Product List"); ?>
                                            </div>
                                            <table id="sample-table-2" class="table commentlist table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">MSH Tag/Bar Code #</th>
                                                        <th>Asset Type</th>
                                                        <th>Description of Equipment</th>
                                                        <th>Manufacturer's Serial Number</th>
                                                        <th>Vendor or Source</th>
                                                        <th>Date of Purchase/Receving Date</th>
                                                        <th>Cost BDT</th>
                                                        <th>US$</th>
                                                        <th>$ Rate</th>
                                                        <th>Voucher/PO #</th>
                                                        <th>Item is Intended to be Outside the Office (Y/N)?</th>
                                                        <th>Location of Equipment</th>
                                                        <th>Condition of Equiptment</th>
                                                        <th>Project Code</th>
                                                        <th>Warranty information </th>
                                                        <th>Disposition</th>
                                                        <th>Last physical inspection date</th>
                                                        <th>Who conducted the last physical inspection</th>
                                                        <th>Remarks </th>
                                                        <th> Action </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                if(!isset($_POST['search']))
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                }
                                                else 
                                                { 
                                                 $data=$obj->SelectAll_ddate($table,"datepr",$_POST['strdate'],$_POST['enddate']);   
                                                }
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                    <tr class="topsix">
                                                        <td class="center"><a href="ind_assetproduct.php?id=<?php echo $row->id; ?>"><?php echo $row->barcode; ?></a></td>
                                                            <td class="center"><?php echo $row->assettype; ?></td>
                                                            <td class="center"><?php echo $row->name; ?></td>
                                                            <td class="center"><?php echo $row->msn; ?></td>
                                                            <td class="center">
                                                                <?php 
                                                                $ast=$obj->SelectAllByID("supplier",array("id"=>$row->sid));
                                                                foreach($ast as $st): echo $st->name;  endforeach;   
                                                                ?>
                                                            </td>
                                                            <td class="center"><?php echo $row->datepr; ?></td>
                                                            <td class="center"><?php echo $row->bdtcost; ?></td>
                                                            <td class="center"><?php echo $row->uscost; ?></td>
                                                            <td class="center"><?php echo $row->dollarrate; ?></td>
                                                            <td class="center"><?php echo $row->voucher_po; ?></td>
                                                            <td class="center"><?php echo $row->itemofficeyn; ?></td>
                                                            <td class="center"><?php echo $row->location; ?></td>
                                                            <td class="center">
                                                                <?php 
                                                                $ast=$obj->SelectAllByID("itemcondition",array("id"=>$row->itemcondition));
                                                                foreach($ast as $st): echo $st->name;  endforeach;   
                                                                ?>
                                                            </td>
                                                            <td class="center">
                                                                <?php echo $row->project;  ?>
                                                            </td>
                                                            <td class="center"><?php echo $row->warranty; ?></td>
                                                            <td class="center"><?php echo $row->disposition; ?></td>
                                                            <td class="center"><?php echo $row->lpi; ?></td>
                                                            <td class="center">
                                                                <?php 
                                                                $ast=$obj->SelectAllByID("employee",array("id"=>$row->bywho));
                                                                foreach($ast as $st): echo $st->name;  endforeach;   
                                                                ?>
                                                            </td>
                                                            <td class="center"><?php echo $row->remarks; ?></td>
                                                            <td>
                                                                <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-edit bigger-130"></i></a>
                                                                    
                                                                    <div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Edit Product Detail
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                           
                                                                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                        
                                                                                            
                                                                                            <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Add New Product Name </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="name" value="<?php echo $row->name; ?>" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> New Barcode </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="barcode" readonly="readonly" value="<?php echo $row->barcode; ?>" placeholder="Barcode Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Asset Type </label>

                                    <div class="col-sm-8">
                                        <select name="assettype">
                                            <?php 
                                            $ast=$obj->SelectAll("assettype");
                                            foreach($ast as $st):
                                            ?>
                                            <option <?php if($row->assettype==$st->legend): ?> selected="selected"  <?php endif; ?> value="<?php echo $st->legend; ?>"><?php echo $st->name; ?></option>
                                            <?php 
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Manufacturer's Serial Number </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" value="<?php echo $row->msn; ?>" name="msn" placeholder="Manufacturer's Serial Number" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Supplier/Vendor </label>

                                    <div class="col-sm-8">
                                        <select name="sid">
                                            <?php 
                                            $ast=$obj->SelectAll("supplier");
                                            foreach($ast as $st):
                                            ?>
                                            <option <?php if($row->sid==$st->id): ?> selected="selected"  <?php endif; ?>  value="<?php echo $st->id; ?>"><?php echo $st->name; ?></option>
                                            <?php 
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Recive/Purchase Date </label>

                                    <div class="col-xs-6 col-sm-4">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="datepr" value="<?php echo $row->datepr; ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> BDT Cost </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="bdtcost" value="<?php echo $row->bdtcost; ?>" placeholder="BDT Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> US Cost </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="uscost" value="<?php echo $row->uscost; ?>" placeholder="US Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Dollar Rate </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="dollarrate" value="<?php echo $row->dollarrate; ?>" placeholder="Dollar Rate" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Voucher/PO # </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="voucher_po" value="<?php echo $row->voucher_po; ?>"  placeholder="Voucher/PO #" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Item is Intended to be Outside the Office (Y/N)? </label>

                                    <div class="col-sm-8">
                                        <select name="itemofficeyn">
                                            <option <?php if($row->itemofficeyn=='Y'): ?> selected="selected"  <?php endif; ?> value="Y">Yes</option>
                                            <option <?php if($row->itemofficeyn=='N'): ?> selected="selected"  <?php endif; ?> value="N">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Item Location </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="location" value="<?php echo $row->location; ?>"  placeholder="Item Location" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Item Condition </label>

                                    <div class="col-sm-8">
                                        <select name="itemcondition">
                                            <?php 
                                            $icsql=$obj->SelectAll("itemcondition");
                                            foreach($icsql as $ic):
                                            ?>
                                                <option <?php if($row->itemcondition==$ic->id): ?> selected="selected"  <?php endif; ?> value="<?php echo $ic->id; ?>"><?php echo $ic->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Project </label>

                                    <div class="col-sm-8">
                                        <input name="project" type="text" value="<?php echo $row->project; ?>"/>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Warranty </label>

                                    <div class="col-sm-8">
                                        <select name="warranty">
                                            <option <?php if($row->warranty=="N/A"): ?> selected="selected"  <?php endif; ?>   value="N/A"> N/A </option>
                                            <?php 
                                            for($i=1; $i<=30; $i++):
                                            ?>
                                                <option <?php if($row->warranty==$i.' Years'): ?> selected="selected"  <?php endif; ?>  value="<?php echo $i; ?> Years"><?php echo $i; ?> Years</option>
                                            <?php
                                            endfor;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Item Disposition </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" value="<?php echo $row->disposition; ?>"   name="disposition" placeholder="Item Disposition" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Last physical inspection date </label>

                                    <div class="col-xs-8 col-sm-2">
                                            <div class="input-group">
                                                <input class="form-control date-picker" value="<?php echo $row->lpi; ?>"    name="lpi"  id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>
                                
                                
                               <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Who conducted the last physical inspection </label>

                                    <div class="col-sm-8">
                                        <select name="bywho">
                                            <?php 
                                            $icsql=$obj->SelectAll("employee");
                                            foreach($icsql as $ic):
                                            ?>
                                                <option <?php if($row->bywho==$ic->id): ?> selected="selected"  <?php endif; ?>  value="<?php echo $ic->id; ?>"><?php echo $ic->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Remarks </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="remarks" value="<?php echo $row->remarks; ?>"  placeholder="Remarks" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                

                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->

                                                                                                                                     
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i></a>
                                                                </div>
                                                                
                                                            </td>
                                                        </tr>
                                                 <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                            <br>
                                            <br>
                                            <h4 class="header smaller lighter blue">Approved By <span style="float: right;"> Created <i class="icon-calendar"></i> <?php echo date('Y-m-d'); ?></a></span></h4>
                                        </div>
                                        
                                        
                                        

                                        
                                    </div>

                                </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>
		
                    <script type="text/javascript">
			jQuery(function($){
	
                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
        
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });     

                                        });
                    </script>
		<!-- inline scripts related to this page -->
    </body>
</html>
