<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Dashboard</a></li>";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
  <style type="text/css">
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:3px; border:#4e95f4 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{
		background: #D19500;
	}
	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		background: #f1da36;
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background: #EFEACB;
	}
</style>
</head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
 
	  ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                            
                                
                            
                                <div class="row">
                                    

                                    <div class="col-xs-6 col-sm-3 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                    <div class="widget-body darkpink topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                                    <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/1.jpg" height="140" width="284">
                                                            </div>
                                                    </div>
                                                <div class="widget-body darkred" style="height: 660px;">
                                                       <div class="widget-main">
                                                             <h3 class="white"> 
                                                                This Year Leave Assign
                                                             </h3>
                                                             <table class="TFtable">
                                                                 <tr><td style="background: #D19500;">S/L</td>
                                                                     <td style="background: #D19500;">Leave Type</td>
                                                                     <td style="background: #D19500;">Quantity</td>
                                                                 </tr>
                                                                 <?php
                                                                 $depsql=$obj->SelectAllByID("requisation_leaveapplication",array("year"=>date('Y')));
                                                                 $x=1;
                                                                 $tt=0;
                                                                 if(!empty($depsql))
                                                                 foreach($depsql as $leaveto):

                                                                 ?>
                                                                 <tr>
                                                                     <td><?php echo $x;  ?></td>
                                                                     <td><?php echo $obj->SelectAllByVal("leave_type","id",$leaveto->leave_type,"name"); ?></td>
                                                                     <td><?php echo $leaveto->quantity;
                                                                     $tt+=$leaveto->quantity;
                                                                     ?></td>
                                                                 </tr>
                                                                 <?php
                                                                 $x++;
                                                                 endforeach; 
                                                                 ?>
                                                                <tr>
                                                                    <td style="background: none;"></td>
                                                                     <td>Total Leave</td>
                                                                     <td><?php echo $tt; ?></td>
                                                                 </tr>
                                                                 
                                                            </table>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                    
                                    <div class="col-xs-6 col-sm-9 pricing-box" style="overflow: hidden;">
                                            <div class="widget-box">
                                                <div class="widget-body dark_green topradious" style="overflow: hidden;">
                                                        <div class="widget-main" style="padding-left: 0;">
                                                            <img style="margin-left: 0; margin-top: 8px;" src="images/thumb/3_1_1.jpg" height="140" width="889">
                                                            </div>
                                                    </div>
                                                <div class="widget-body green_gradient" style="height: 660px;">
                                                        <div class="widget-main">
                                                             <h3 class="header smaller lighter black"> 
                                                                 My Leave Status
                                                             </h3>
                                                             <table class="TFtable">
                                                                 <tr>
                                                                     <td style="background: #D19500;">S/L</td>
                                                                     <td style="background: #D19500;">Leave Type</td>
                                                                     <td style="background: #D19500;">Limit</td>
                                                                     <td style="background: #D19500;">Utilized</td>
                                                                     <td style="background: #D19500;">Balance</td>
                                                                 </tr>
                                                                 <?php
                                                                 $depsql=$obj->SelectAll("leave_type");
                                                                 $x=1;
                                                                 if(!empty($depsql))
                                                                 foreach($depsql as $dep):

                                                                 ?>
                                                                 <tr>
                                                                     <td><?php echo $x; ?></td>
                                                                     <td><?php echo $dep->name; ?></td>
                                                                     <td><?php 
                                                                            $sqllimits=$obj->SelectAllByID2("requisation_leaveapplication",array("year"=>date('Y')),"leave_type",$dep->id);
                                                                            $limit=0;
                                                                            foreach($sqllimits as $li):
                                                                                $limit+=$li->quantity;
                                                                            endforeach;
                                                                            
                                                                            echo $limit;
                                                                     ?></td>
                                                                     <td>
                                                                     <?php
                                                                     $utiarray=array("emplid"=>$input_by);
                                                                     $sqlutilized=$obj->SelectAllByID3("requisation_leaveapplication_form",$utiarray,"leave_type",$dep->id,"year",date('Y'));
                                                                     $utilized=0;
                                                                        if(!empty($sqlutilized))
                                                                        {
                                                                           foreach($sqlutilized as $uti):
                                                                               $utilized+=$uti->datequantity;
                                                                           endforeach;
                                                                        }
                                                                     echo $utilized;
                                                                     ?>
                                                                     </td>
                                                                     <td>
                                                                     <?php
                                                                     $balance=$limit-$utilized;
                                                                     echo $balance;
                                                                     ?>
                                                                     </td>
                                                                 </tr>
                                                                 <?php
                                                                 $x++;
                                                                 endforeach; 
                                                                 ?>
                                                                 
                                                            </table>
                                                        </div>
                                                    </div>
                                            </div>
                                    </div>
                                </div>
                            <!-- PAGE CONTENT ENDS -->
                            
                            </div><!-- /.col -->
                        </div><!-- /.row -->
 
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->

<?php
//include('class/colornnavsetting.php');
include('class/footer.php');
?>


                <?php //echo $obj->bodyfooter(); ?>

		<!--[if !IE]> -->

		<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.sparkline.min.js"></script>
		<script src="assets/js/flot/jquery.flot.min.js"></script>
		<script src="assets/js/flot/jquery.flot.pie.min.js"></script>
		<script src="assets/js/flot/jquery.flot.resize.min.js"></script>

		<!-- ace scripts -->

		<script src="assets/js/ace-elements.min.js"></script>
		<script src="assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				$('.easy-pie-chart.percentage').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
					var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
					var size = parseInt($(this).data('size')) || 50;
					$(this).easyPieChart({
						barColor: barColor,
						trackColor: trackColor,
						scaleColor: false,
						lineCap: 'butt',
						lineWidth: parseInt(size/10),
						animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
						size: size
					});
				})
			
				$('.sparkline').each(function(){
					var $box = $(this).closest('.infobox');
					var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
					$(this).sparkline('html', {tagValuesAttribute:'data-values', type: 'bar', barColor: barColor , chartRangeMin:$(this).data('min') || 0} );
				});
			
			
			
			
			  var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "Leave Application",  data: <?php echo $obj->emp_leave_quantity($input_by); ?>, color: "#68BC31"},
				{ label: "Store Requisation",  data: <?php echo $obj->totalrowsbyDID("requisationlist","empid",$input_by); ?>, color: "#2091CF"},
				{ label: "Vehecle Requisation",  data: <?php echo $obj->totalrowsbyDID("requisationlist_vehicle","empid",$input_by); ?>, color: "#AF4E96"},
				{ label: "Total Stock Out",  data: <?php echo $obj->totalrowsbyDID("stockoutreport","emplid",$input_by); ?>, color: "#DA5430"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
			
			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);
			
			
			
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}
				
			 });
			
			
			
			
			
			
				var d1 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d1.push([i, Math.sin(i)]);
				}
			
				var d2 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d2.push([i, Math.cos(i)]);
				}
			
				var d3 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.2) {
					d3.push([i, Math.tan(i)]);
				}
				
			
				var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
				$.plot("#sales-charts", [
					{ label: "Domains", data: d1 },
					{ label: "Hosting", data: d2 },
					{ label: "Services", data: d3 }
				], {
					hoverable: true,
					shadowSize: 0,
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						tickLength: 0
					},
					yaxis: {
						ticks: 10,
						min: -2,
						max: 2,
						tickDecimals: 3
					},
					grid: {
						backgroundColor: { colors: [ "#fff", "#fff" ] },
						borderWidth: 1,
						borderColor:'#555'
					}
				});
			
			
				$('#recent-box [data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('.tab-content')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			
			
				$('.dialogs,.comments').slimScroll({
					height: '300px'
			    });
				
				
				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				});
			
				$('#tasks').sortable({
					opacity:0.8,
					revert:true,
					forceHelperSize:true,
					placeholder: 'draggable-placeholder',
					forcePlaceholderSize:true,
					tolerance:'pointer',
					stop: function( event, ui ) {//just for Chrome!!!! so that dropdowns on items don't appear below other items after being moved
						$(ui.item).css('z-index', 'auto');
					}
					}
				);
				$('#tasks').disableSelection();
				$('#tasks input:checkbox').removeAttr('checked').on('click', function(){
					if(this.checked) $(this).closest('li').addClass('selected');
					else $(this).closest('li').removeClass('selected');
				});
				
			
			})
		</script>
                </body>
                </html>
