<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Meeting Reservation Info</a></li><li class='active'>Add Meeting Reservation Form</li>";
$table="requisationlist_reservation";
if(isset($_POST['submit']))
{
    if(!empty($_POST['title']))
    {
        $insert=array("emplid"=>$input_by,"title"=>$_POST['title'],"startdatetime"=>$_POST['startdatetime'],"enddatetime"=>$_POST['enddatetime'],"supervisor"=>$_POST['supervisor'],"detail"=>$_POST['detail'],"date"=>  date('Y-m-d'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                    $notify_detail="Meeting Reservation Request Has Benn Created";                   
                    $obj->make_notification($table,$notify_detail,$input_by,$_POST['supervisor'],"3");
                    
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        } 
}

if(isset($_POST['preview']))
{
    ?>
    
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <form name="add" method="POST" action="">
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Individual Leave Application Form 
                        <span style="float: right;"><a href="#" target="_blank" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a>
                        <button class="btn btn-info" type="submit" name="submit">Send For Approval</button>
                        </span></h3>

                        
                             <input type="hidden" name="title" value="<?php echo $_POST['title']; ?>">
                             <input type="hidden" name="startdatetime" value="<?php echo $_POST['startdatetime']; ?>">
                             <input type="hidden" name="enddatetime" value="<?php echo $_POST['enddatetime']; ?>">
                             <input type="hidden" name="supervisor" value="<?php echo $_POST['supervisor']; ?>">
                             <input type="hidden" name="detail" value="<?php echo $_POST['detail']; ?>">
                             
 <?php
                    include('class/esm.php');
                    ?>
                    <div class="row" id="printablediv">
                        
                        
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                                                                <div>
                                
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Preview Meeting Reservation"); ?>
                                
                                    </div>
                          <h3>A. PERSONAL INFORMATION (For Applicant)</h3>
                            <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                              <tr>
                                <td valign='top' colspan='3'><strong>Name : </strong><?php echo $obj->SelectAllByVal("employee","id",$input_by,"name");
                                    ?></td>
                                <td valign='top' colspan='2'><strong>Position : </strong>
                                  <?php echo $obj->emp_designation($input_by); ?>
                                </td>
                                </tr>
                              <tr>
                                <td valign='top' height='62' colspan='2'><strong>ID Number : </strong> <?php echo $input_by; ?></td>
                                <td valign='top' colspan='3'><strong>Purpose/Detail : </strong> <?php echo $_POST['detail']; ?></td>
                                </tr>
                              <tr>
                                <td colspan="5" valign='top'><strong>Meeting Title / Location : </strong> <?php echo $_POST['title']; ?></td>
                              </tr>
                              <tr>
                                <td valign='top' width='335'><strong>Meeting Date Time</strong> </td>
                                <td valign='top' colspan='2'><strong>Start Time : </strong> <?php echo $_POST['startdatetime']; ?></td>
                                <td colspan="2" valign='top'><strong>End Time :</strong> <?php echo $_POST['enddatetime']; ?></td>
                              </tr>
                              <tr>
                                <td valign='top' colspan='4'><strong>Signature of the Application :</strong></td>
                                <td width="228" valign='top'><strong>Date :</strong><?php echo date('Y-m-d');  ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>B. APPROVAL : </h3>
                            
<table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td valign='top' height='80'><strong>Recommended By ( Supervisor ) :</strong>
        <br />
        <?php echo $obj->SelectAllByVal("employee","id",$_POST['supervisor'],"name"); ?>
    </td>
    <td valign='top'><strong>Reviewed By ( Admin ) :</strong> <?php echo $obj->SelectAllByVal("employee","status","2","name");  ?></td>
    <td valign='top'><strong>Approved By ( Country Project Director ) :</strong></td>
    </tr>
</table>
  
  
  <br>

<h3>C . GUIDELINES : </h3>
                            
<table width='100%' border='0'>
  <tr>
    <td height='44'>1. This form must be filled in by all staff &amp; must support any absense from work.<br>
    2. Please delegate someone who will perform your current and most urgent desk work on behalf of you while on leave.</td>
    </tr>
  </table>
								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
                </form>
    </body>
</html>

    
    <?php
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
<script>
function showUser(str)
{
    if (str=="")
      {
      document.getElementById("txtHint").innerHTML="";
      return;
      }
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
      }
    startdate = document.getElementById("datetimepicker").value;
    enddate = document.getElementById("datetimepicker_1").value;
    xmlhttp.open("GET","datetimecount.php?q="+str+"&sdate="+startdate+"&edate="+enddate,true);
    xmlhttp.send();
}
</script>
<link rel="stylesheet" type="text/css" href="./jquery.datetimepicker.css"/>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Meeting Reservation Form 
                            <a target="_blank" style="float: right;" href="reservation_requisitionlist.php" class="btn btn-success">Reservation Request List</a>
                            </h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title &AMP; Location<font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <textarea type="text" id="form-field-1" name="title" class="col-xs-10 col-sm-5"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date &AMP; Time<font color="green">*</font> </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control" name="startdatetime" value="<?php echo date('Y-m-d H:i'); ?>" id="datetimepicker" type="text" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>

                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> End Date &AMP; Time<font color="green">*</font> </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control" name="enddatetime"  value="2014-03-15 05:06" id="datetimepicker_1" type="text" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div> 
                                

                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Super Visor <font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="supervisor" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAllNotMe("employee",array("id"=>$input_by));
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Detail<font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <textarea type="text" id="form-field-1" name="detail" class="col-xs-10 col-sm-5"></textarea>
                                    </div>
                                </div> 
                                
                                
                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-danger" type="submit" name="preview"><i class="icon-zoom-in"></i>Preview</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn btn-info" type="submit" name="submit">Send For Approval</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
                <script src="./jquery.datetimepicker.js"></script>
                <script>

                $('#datetimepicker').datetimepicker();
                $('#datetimepicker').datetimepicker({value:'2014-04-15 05:03',step:10});
                
                $('#datetimepicker_1').datetimepicker();
                $('#datetimepicker_1').datetimepicker({value:'2014-04-15 07:03',step:10});

                </script>
    </body>
</html>
