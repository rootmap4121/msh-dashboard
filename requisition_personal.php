<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>User Info</a></li><li class='active'>Admin List</li>";
$table = "requisition";
if (isset($_POST['submit'])) {
    if (!empty($_POST['empid']) && !empty($_POST['projectid'])) {
                $quantity = $_POST['quantity'];
                $pid=$_POST['pid'];
                
                $table1="requisationlist";
                $insert = array("empid" => $_POST['empid'], "projectid" => $_POST['projectid'], "supervisor" => $_POST['to'],"date" => date('Y-m-d'), "status" => 1);
                
                //$req_id=$obj->insert_id($table1,$insert); 
                
                if($obj->insert($table1,$insert)==1)
                { 
                    $fg=$obj->lastid($table1);
                    
                    $notify_detail="Store Requisation Has Been Created";                   
                    $obj->make_notification($table1,$notify_detail,$_POST['empid'],$_POST['to'],"1");
                    
                    
                    foreach ($fg as $rt):
                        $req_id=$rt->id;
                        foreach ($_POST['pid'] as $index=>$aa):
                        if(!empty($aa)){ $obj->insert($table,array("req_id"=>$req_id,"empid"=>$_POST['empid'],"to"=>$_POST['to'],"projectid"=>$_POST['projectid'],"pid"=>$aa,"quantity"=>$_POST['quantity'][$index],"date"=>  date('Y-m-d'),"status"=>1,"description"=>$_POST['des'][$index])); }
                        endforeach;
                    endforeach;
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
                }
                else
                { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                }          
    }else
    { 
            $errmsg_arr[]= 'Failed,Field is Empty';
            $errflag = true;
            if ($errflag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./".$obj->filename());
                exit();
            }
        
    }
}
if(isset($_POST['preview']))
{
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
    <form name="submit" action="" method="post">
        <?php include('class/header.php'); ?>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Requisation Detail Form-<?php echo date('Y'); ?>
                         <span style="float: right;">
                             <a href="#" target="_blank" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a>
                             <button class="btn btn-info" type="submit" name="submit"> Send for Approval </button>
                         </span></h3>
                           
                        <?php include('class/esm.php'); ?>
                        <div class="row" id="printablediv">
                        
                        
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                                   <div>
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Previe Store Requisation Detail"); ?>
                                    </div>
                            <input type="hidden" name="projectid" value="<?php echo $_POST['projectid']; ?>">
                            <input type="hidden" name="empid" value="<?php echo $_POST['empid']; ?>">
                            <input type="hidden" name="to" value="<?php echo $_POST['to']; ?>">
                          <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                            <tr>
                              <td height='31' valign='top'><strong>Project Name : <?php echo $obj->SelectAllByVal("project","id",$_POST['projectid'],"name"); ?></strong></td>
                              <td valign='top'><strong>Signature : </strong></td>
                            </tr>
                            <tr>
                                <td height='31' valign='top'><strong>Requesting : </strong> <?php echo $obj->SelectAllByVal("employee","id",$_POST['empid'],"name"); ?></td>
                                <td valign='top'><strong>Requisation ID : </strong> <?php echo "auto"; ?></td>
                                </tr>
                              <tr>
                                <td width='541' valign='top'><strong>Designation :</strong> <?php echo $obj->emp_designation($_POST['empid']); ?></td>
                                <td valign='top'><strong>Date : </strong><?php echo date('Y-m-d'); ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='23%' valign='top'><strong>Product Name</strong></td>
    <td width='24%' valign='top'><strong>Requested Quantity</strong></td>
    <td width='28%' valign='top'><strong>Description</strong></td>
    </tr>
    <?php
	foreach($_POST['pid'] as $index=>$aa):
	?>
    <input type="hidden" name="pid[]" value="<?php echo $aa; ?>">
    <input type="hidden" name="quantity[]" value="<?php echo $_POST['quantity'][$index]; ?>">
    <input type="hidden" name="des[]" value="<?php echo $_POST['des'][$index]; ?>">
  <tr>
    <td valign='top'><?php echo $obj->SelectAllByVal("product","id",$aa,"name"); ?></td>
    <td valign='top'><?php echo $_POST['quantity'][$index]; ?></td>
    <td valign='top'><?php echo $_POST['des'][$index]; ?></td>
    </tr>
    <?php
	endforeach;
	?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
 <table  width='100%' border='0' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='56%' height='64' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong> 
        <br />
        <?php echo $obj->SelectAllByVal("employee","id",$_POST['to'],"name"); ?></td>
    <td width='44%' valign='top' align='right'><strong>Signature : </strong></td>
    </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>Note : </td>
    </tr>
  </table>
<div class="hr hr-18 dotted hr-double"></div>
								
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
        </form>
    </body>
</html>

    <?php
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
        <script type="text/javascript">
            
            function incre()
            {
                var x=1;
                document.getElementById("in").value=x+1;
            }
            
        </script>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New Store Requisation From-SRF 
                                <a target="_blank" style="float: right;" href="requisitionlist_personal.php" class="btn btn-success">Requisation List</a>
                            </h3>
                            <!-- PAGE CONTENT BEGINS -->


                            <div class="row">

                                <div class="col-xs-12">
                                    <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>
                                                        <select class="width-10" name="empid">
                                                            <option value="">Select Employee</option>
                                                                <?php $data = $obj->SelectAllorderBy("employee");
                                                                foreach ($data as $rowss):
                                                                ?>
                                                                <option <?php if ($rowss->id == $input_by): ?> selected="selected" <?php endif; ?>  value="<?php echo $rowss->id; ?>"><?php echo $rowss->name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>ID NO.</td>
                                                    <td>
                                                        <input type="text" id="form-field-1" disabled="disabled" name="idno" placeholder="System Will Be Generate " />
                                                    </td>
                                                    <td class="center" colspan="2">
                                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                        <button class="btn btn-info" type="submit" name="preview"><i class="icon-zoom-in bigger-110"></i>Preview</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Project</td>
                                                    <td>
                                                        <select class="width-10" name="projectid">
                                                            <option value="">Select Project</option>
<?php $data = $obj->SelectAllorderBy("project");
foreach ($data as $rowss):
    ?>
                                                                <option value="<?php echo $rowss->id; ?>"><?php echo $rowss->name; ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </td>
                                                    <td>Supervisor</td>
                                                    <td>
                                                        <select name="to">
                                                        <?php $tooo=$obj->SelectAll("employee"); foreach($tooo as $to): ?>
                                                            <option value="<?php echo $to->id; ?>"><?php echo $to->name; ?></option>
                                                        <?php endforeach; ?>
                                                        </select>
                                                        
                                                    </td>
                                                    <td class="center" colspan="2">
                                                        <button class="btn btn-info" type="submit" name="submit">Send For Approval</button>
                                                    </td>


                                                </tr>
                                            </tbody>


                                        </table>
                                        <div class="table-header">
                                            Please arrange to supply the following items&because;
                                        </div>

                                        <div class="table-responsive">
                                            <table  id="itemTable" class="table">
                                                <thead>
                                                    <tr>
                                                        <th> Select Product </th>
                                                        <th class="center"> Quantity Request </th>
                                                        <th class="center"> Description </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                        <tr id="InputRow">
                                                            <td>
                                                                <select name="pid[]"  class="form-control" id="form-field-select-1">
                                                                <?php $pro=$obj->SelectAll("product"); foreach($pro as $product): ?>
                                                                    <option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
                                                                <?php endforeach; ?>
                                                                </select>
                                                            </td>
                                                            
                                                            <td class="center"><input type="text" id="form-field-1" name="quantity[]" id="quantity[]" placeholder="Type Your Quantity"  /></td>
                                                            <td><input type="text" id="form-field-3" style="width: 100%;" name="des[]" id="des[]" placeholder="Type Your Product Description"  /></td>

                                                        </tr>



                                                </tbody>
                                            </table>
                                           <button class="btn btn-lg btn-success" type="button" onclick="return addTableRow('#itemTable');return false;">
					    <i class="icon-plus"></i> Add More Products
					   </button>
                                            <div class="clearfix"></div>
                                            <br>
                                        </div>
                                    </form>                                 								

                                    <div class="table-header">
                                        Requested By :________________. Recommended by:____________. Approved by:_______________. Received by:_________________.<br/>
                                        Signature with date .............. Signature with date ...............Signature with date...............Signature with date
                                    </div>

                                </div>


                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.page-content -->
                </div><!-- /.main-content -->

<?php
//include('class/colornnavsetting.php');
include('class/footer.php');
?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
                    function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
                    
		</script>
</body>
</html>
