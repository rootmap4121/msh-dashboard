<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Vehicle Request Info</a></li><li class='active'>Vehicle Request List</li>";
$table="requisition_vehicle";
if (isset ($_POST['edit'])) 
{
    $updatearray=array("id"=>$_POST['id'],"emplid"=>$input_by,"startdatetime"=>$_POST['startdatetime'],"enddatetime"=>$_POST['enddatetime'],"startpoint"=>$_POST['startpoint'],"destination"=>$_POST['destination'],"passengerquantity"=>$_POST['passenger'],"passengernames"=>$_POST['passengernames'],"purpose"=>$_POST['purposeoftravel'],"phone"=>$_POST['contactphoneno'],"typeoftravel"=>$_POST['typeoftravel'],"budgetcode"=>$_POST['budgetcode'],"budgetholder"=>$_POST['budgetholder'],"supervisor"=>$_POST['supervisor']);
    if($obj->update($table,$updatearray)==1)
    { 
        $errmsg_arr[]= 'Successfully Updated';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }
    } 
    else 
    { 
        $errmsg_arr[]= 'Failed';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        } 

    } 
}
elseif (isset ($_GET['del'])=="delete") 
{
    $delarray=array("id"=>$_GET['id']);
    if($obj->delete($table,$delarray)==1)
    { 
        $errmsg_arr[]= 'Successfully Deleted';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        } 
    } 
    else 
    { 
        $errmsg_arr[]= 'Failed';
        $errflag = true;
        if ($errflag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        } 
    }
}
?>

<?php
if(@$_GET['action']=='pdf')
{
include("pdf/MPDF57/mpdf.php");
$html.=$obj->company_report_logo()." ".$obj->company_report_head();
$html.=$obj->company_report_name("Vehicle Request List");
$html.="<div class='table-responsive'><table id='sample-table-2' class='table table-hover' border='1'><thead>
        <tr class='headerrow'>
            <th class='center'>S/N</th>
            <th>Start-End (datetime)</th>
            <th>Start Point - Destination</th>
            <th>Passenger Quantity</th>
            <th>Date</th>
            <th>Status</th>
        </tr>
    </thead><tbody>";
    if(!isset($_POST['search']))
    {
        $data=$obj->SelectAllByID($table,array("supervisor"=>$input_by));
    }
    else 
    {
        $data=$obj->SelectAll_ddate_ind($table,"date",$_POST['strdate'],$_POST['enddate'],"supervisor",$input_by); 
    }
    $x=1;
    foreach ($data as $row):
        
      $html .="<tr><td class='center'>".$x."</td><td>
                                                                <a href='vehiclerequest_requisitionlistdetail.php?id=".$row->id."'>
                                                                    <span class='label label-sm label-success'>(".$row->startdatetime."</span>-<span class='label label-sm label-danger'>".$row->enddatetime.")</span>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <span class='label label-sm label-primary'>".$row->startpoint."</span>
                                                                -
                                                                <span class='label label-sm label-info'>".$row->destination."</span>
                                                            </td>
                                                            <td class='center'><span class='label label-sm label-warning'>".$row->passengerquantity."</span> </td>
                                                            <td><span class='label label-sm label-success'>".$row->date."</span></td>
                                                            <td>".$obj->emp_leave_status($row->status)."</td></tr>";
     $x++;
     endforeach;


$html.="</tbody></table></div>";

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
}
elseif(@$_GET['action']=='excel')
{

header('Content-type: application/excel');
$filename = 'leaveapplicationlist.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Leave Application List</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
$data .=$obj->company_report_head()."Report Creation Date : ".date('d-m-Y H:i:s');
$data .=$obj->company_report_name("Vehicle Request List");
$data .="<table id='sample-table-2' class='table table-hover' border='1'>
    <thead>
        <tr>
            <th class='center'>S/N</th>
            <th>Start-End (datetime)</th>
            <th>Start Point - Destination</th>
            <th>Passenger Quantity</th>
            <th>Date</th>
            <th>Status</th>
        </tr>
</thead>        
<tbody>";

$data .='</tr>';

    if(!isset($_POST['search']))
    {
        $ss=$obj->SelectAllByID($table,array("supervisor"=>$input_by));
    }
    else 
    {
        $ss=$obj->SelectAll_ddate_ind($table,"date",$_POST['strdate'],$_POST['enddate'],"supervisor",$input_by); 
    }
    $x=1;
    foreach ($ss as $row):
        
      $data .="<tr><td class='center'>".$x."</td><td>
                                                                <a href='vehiclerequest_requisitionlistdetail.php?id=".$row->id."'>
                                                                    <span class='label label-sm label-success'>(".$row->startdatetime."</span>-<span class='label label-sm label-danger'>".$row->enddatetime.")</span>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <span class='label label-sm label-primary'>".$row->startpoint."</span>
                                                                -
                                                                <span class='label label-sm label-info'>".$row->destination."</span>
                                                            </td>
                                                            <td class='center'><span class='label label-sm label-warning'>".$row->passengerquantity."</span> </td>
                                                            <td><span class='label label-sm label-success'>".$row->date."</span></td>
                                                            <td>".$obj->emp_leave_status($row->status)."</td></tr>";
     $x++;
     endforeach;


$data .="</tbody></table>";

$data .='</body></html>';

echo $data;

}
elseif(@$_GET['action']=='approve')
{
    $array=array("id"=>$_GET['id'],"status"=>2);
    if($obj->update($table,$array)==1)
    {
                    $errmsg_arr[]= 'Successfully Approved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
elseif(@$_GET['action']=='reject')
{
    $array=array("id"=>$_GET['id'],"status"=>3);
    if($obj->update($table,$array)==1)
    {
                    $errmsg_arr[]= 'Successfully Reject';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); 
        
        ?>
       <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <h3 class="header smaller lighter blue">Vehicle Request List
                                <a target="_blank" href="<?php echo $obj->filename(); ?>?action=pdf"><img src="images/pdf.png"></a> 
                                <a target="_blank" href="<?php echo $obj->filename(); ?>?action=excel"><img src="images/excel.png"></a>
                                
                                <span style="margin-left: 20px;"><a  href="#modal-tablesearch" role="button" data-toggle="modal" class="green"><i class="icon-camera-retro"></i> Search in Multiple Dates</a></span> <span style="float: right;"><a target="_blank" href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                            <div id="modal-tablesearch" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Report Using Multiple Date
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <br>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Start Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="strdate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>
                                    <div class="space-4"></div>
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Ending Date </label>

                                    <div class="col-xs-6 col-sm-6">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="enddate" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                    </div>                                      


                                                                                            <div class="space-4"></div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="search"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->

                                        
                                        <div class="table-header">
                                          <fieldset>
                                                                        <input type="text" class="text-input" style="width: 200px;" id="topsix"  placeholder="Please Search Anything.."  />
                                                                        <span id="topsix-count"></span>
                                                            </fieldset>
                                        </div>
                            <!-- PAGE CONTENT BEGINS -->
                            
                    <div class="row">
                        <div class="col-xs-12">

							
                                <div class="row" id="printablediv">

                                    <div>
                                
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Vehicle Request List"); ?>
                                
                                    </div>
                                        
                                    <div class="col-xs-12">
                                        
                                        <div class="table-responsive">
                                         
                                            <table id='sample-table-2' class='table table-striped table-bordered table-hover'>
                                                <thead>
                                                    <tr>
                                                        <th class='center'>S/N</th>
                                                        <th>Start-End (datetime)</th>
                                                        <th>Start Point - Destination</th>
                                                        <th>Passenger Quantity</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if(!isset($_POST['search']))
                                                {
                                                    $data=$obj->SelectAllByID($table,array("supervisor"=>$input_by));
                                                }
                                                else 
                                                {
                                                    $data=$obj->SelectAll_ddate_ind($table,"date",$_POST['strdate'],$_POST['enddate'],"supervisor",$input_by); 
                                                }
                                                $x=1;
                                                    foreach ($data as $row): 
                                                        
                                                         $chknot=$obj->notification_check_user($row->id,"2",$input_by);
                                                ?>
                                                        <tr>
                                                            <td class='center'><?php echo $x; ?></td>
                                                            <td <?php if($chknot==1): ?> class="unread" <?php endif; ?>>
                                                                <a href='vehiclerequest_requisitionlistdetail.php?id=<?php echo $row->id; ?>'>
                                                                    <span class='label label-sm label-success'>(<?php echo $row->startdatetime; ?></span>-<span class='label label-sm label-danger'><?php echo $row->enddatetime; ?>)</span>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <span class='label label-sm label-primary'>
                                                                <?php echo $row->startpoint; ?>
                                                                </span>
                                                                -
                                                                <span class='label label-sm label-info'>
                                                                <?php echo $row->destination; ?>
                                                                </span>
                                                            </td>
                                                            <td class='center'><span class='label label-sm label-warning'><?php echo $row->passengerquantity; ?></span> </td>
                                                            <td><span class='label label-sm label-success'><?php echo $row->date; ?></span></td>
                                                            <td>
                                                                <?php 
                                                                if($row->supervisor==$input_by)
                                                                {
                                                                    if($row->status==1)
                                                                    {
                                                                    ?>
                                                                    <a href='<?php echo $obj->filename(); ?>?action=approve&AMP;id=<?php echo $row->id; ?>'><span class='label label-sm label-warning'> Accept </span></a>
                                                                    <a href='<?php echo $obj->filename(); ?>?action=reject&AMP;id=<?php echo $row->id; ?>'><span class='label label-sm label-danger'> Reject </span></a>
                                                                    <?php 
                                                                    }
                                                                    elseif($row->status==2){
                                                                    ?>
                                                                    <span class='label label-sm label-success"> <?php echo $obj->emp_leave_status($row->status); ?></span>
                                                                    <?php
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                     echo $obj->emp_leave_status($row->status); 
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a href='<?php echo $obj->filename(); ?>?action=delete&AMP;id=<?php echo $row->id; ?>'><span class='label label-sm label-danger'>Delete</span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
                                
                                
                                $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
        
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
                                
			})
		</script>
    </body>
</html>
