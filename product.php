<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Product Info</a></li><li class='active'>Product List</li>";
$table="product";
if(isset($_POST['submit']))
{
    if(!empty($_POST['name']))
    {
        $exist_array=array("name"=>$_POST['name']);
        $insert=array("name"=>$_POST['name'],"barcode"=>$_POST['barcode'],"cid"=>$_POST['cid'],"scid"=>$_POST['scid'],"pack"=>$_POST['pack'],"quantity"=>$_POST['quantity'],"price"=>$_POST['price'],"mrp"=>$_POST['mrp'],"reorder"=>$_POST['reorder'],"emplid"=>$input_by,"date"=>  date('Y-m-d'),"status"=>1);
        if($obj->exists($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exists';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        }
    }
 else {
            $errmsg_arr[]= 'Fields is Empty';
            $errflag = true;
            if ($errflag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header("location: ./".$obj->filename());
                exit();
            }
    }   
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $updatearray=array("id"=>$_POST['id'],"name"=>$_POST['name'],"barcode"=>$_POST['barcode'],"cid"=>$_POST['cid'],"scid"=>$_POST['scid'],"pack"=>$_POST['pack'],"quantity"=>$_POST['quantity'],"price"=>$_POST['price'],"mrp"=>$_POST['mrp'],"reorder"=>$_POST['reorder']);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        } 
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[]= 'Successfully Deleted';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
                        
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->          

                                <div class="row">

                                    <div class="col-xs-12">
                                        <h3 class="header smaller lighter blue">Product List</h3>


                                        <div class="table-responsive">
                                            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Product Name</th>
                                                        <th>Active Ingredients - Pack Size</th>
                                                        <th>Flat Rate</th>
                                                        <th>MRP</th>
                                                        
                                                        <th>Quantity</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody id="status">
                                                <?php
                                                if($obj->totalrows($table)!=0)
                                                {
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td>
                                                                <?php 
                                                                $pack=$obj->SelectAllByID("product_type",array("id"=>$row->pack));
                                                                foreach($pack as $pac):
                                                                ?>
                                                                <span class="label label-sm label-success"><?php echo $pac->name; ?></span>
                                                                - 
                                                                <span class="label label-sm label-success"><?php echo $pac->size; ?></span>
                                                                <?php endforeach; ?>
                                                            </td>
                                                            <td><span class="label label-sm label-success"><?php echo $row->price; ?></span></td>
                                                            <td><?php echo $row->mrp; ?></td>
                                                            <td><span class="label label-sm label-info"><?php echo $row->quantity; ?></span></td>
                                                            <td>
                                                                    
                                                                    <div class="visible-md visible-lg hidden-sm hidden-xs action-buttons">
                                                                    <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="green"><i class="icon-edit bigger-130"></i> Edit</a> 
                                                                
                                                                    <a class="red" href="<?php echo $obj->filename(); ?>?del=delete&AMP;id=<?php echo $row->id; ?>"><i class="icon-trash bigger-130"></i> Delete</a>
                                                                </div>
								<div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Product Detail
												</div>
											</div>
                                                                                        <!-- /.modal-content -->
                                                                                        <form class="form-horizontal" name="designationedit" role="form" action="" method="POST">
                                                                                            <div class="form-group">
                                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input type="text" id="form-field-1" name="name" value="<?php echo $row->name; ?>" class="col-xs-10 col-sm-5" />
                                                                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"  />
                                                                                                </div>
                                                                                            </div>
                                                                                            
                                                                                            <div class="space-4"></div>
                                                                                            
                                                                                            <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Barcode </label>

                                                                                            <div class="col-sm-9">
                                                                                                <input type="text"  value="<?php echo $row->barcode; ?>"  id="form-field-1" name="barcode" placeholder="Barcode Name" class="col-xs-10 col-sm-5" />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category </label>

                                                                                            <div class="col-sm-9">
                                                                                                <select name="cid">
                                                                                                    <option value="">Category</option>
                                                                                                    <?php $category=$obj->SelectAll("category"); foreach($category as $cat): ?>
                                                                                                    <option <?php if($cat->id==$row->cid): ?> selected="selected" <?php endif; ?>  value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
                                                                                                    <?php endforeach; ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Sub-Category </label>

                                                                                            <div class="col-sm-9">
                                                                                                <select name="scid">
                                                                                                    <option value="">Sub-Category</option>
                                                                                                    <?php $subcategory=$obj->SelectAll("subcategory"); foreach($subcategory as $scat): ?>
                                                                                                    <option<?php if($scat->id==$row->scid): ?> selected="selected" <?php endif; ?> value="<?php echo $scat->id; ?>"><?php echo $scat->name; ?></option>
                                                                                                    <?php endforeach; ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        
                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Ingredients  </label>

                                                                                            <div class="col-sm-9">
                                                                                                <select name="pack">
                                                                                                    <option value="">Select Ingredients</option>
                                                                                                    <?php $subcategory=$obj->SelectAll("product_type"); foreach($subcategory as $scat): ?>
                                                                                                    <option <?php if($row->pack==$scat->id): ?> selected="selected" <?php endif; ?> value="<?php echo $scat->id; ?>"><?php echo $scat->name; ?> - <?php echo $scat->size; ?></option>
                                                                                                    <?php endforeach; ?>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                            
                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Flat Rate </label>

                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" id="form-field-1" name="price" value="<?php echo $row->price; ?>"  placeholder="Price" class="col-xs-10 col-sm-5" />
                                                                                            </div>
                                                                                        </div>

                                                                                      <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> MRP </label>

                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" id="form-field-1" name="mrp" value="<?php echo $row->mrp; ?>"  placeholder="MRP" class="col-xs-10 col-sm-5" />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Quantity </label>

                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" id="form-field-1" name="quantity"  value="<?php echo $row->quantity; ?>"  placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Re-Order </label>

                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" id="form-field-1" name="reorder" value="<?php echo $row->reorder; ?>"  placeholder="Re-Order" class="col-xs-10 col-sm-5" />
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Date </label>

                                                                                            <div class="col-sm-9">
                                                                                                <input type="text" id="form-field-1" readonly="readonly" name="date" value="<?php echo $row->date; ?>"  class="col-xs-10 col-sm-5" />
                                                                                            </div>
                                                                                        </div>

                                                                                            <div class="clearfix form-actions">
                                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                                    <button class="btn btn-info" type="submit" name="edit"><i class="icon-ok bigger-110"></i>Edit Now</button>
                                                                                                    &nbsp; &nbsp; &nbsp;
                                                                                                    <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- end modal form -->
                                                                

                                                            </td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 $x++; endforeach; 
                                                }
                                                 ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
