<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Field Inventory Product List</a></li><li class='active'>Add Inventory Field Product</li>";
$table="fieldproduct";
if(isset($_POST['submit']))
{
    if(!empty($_POST['name']))
    {
        $exist_array=array("barcode"=>$_POST['barcode']);
        $insert=array("name"=>$_POST['name'],
            "barcode"=>$_POST['barcode'],
            "quantity"=>$_POST['quantity'],
            "assettype"=>$_POST['assettype'],
            "msn"=>$_POST['msn'],
            "sid"=>$_POST['sid'],
            "datepr"=>$_POST['datepr'],
            "bdtcost"=>$_POST['bdtcost'],
            "uscost"=>$_POST['uscost'],
            "dollarrate"=>$_POST['dollarrate'],
            "marketvalueperunit"=>$_POST['marketvalueperunit'],
            "disposition"=>$_POST['disposition'],"remarks"=>$_POST['remarks'],"date"=>date('Y-m-d'),"status"=>1);
        if($obj->exists($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Already Exist';
                        $errflag = true;
                        if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
        }
        else
        {
            if($obj->insert($table,$insert)==1)
            {
                        $errmsg_arr[] = 'Successfully Added';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                        
            }
            else 
             {
                        $errmsg_arr[]= 'Failed Please Try Again';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
            }
        }
    }
 else {
        $errmsg_arr[]= 'Field is Empty';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./".$obj->filename());
            exit();
        }
    }   
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
		
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Add New/Existing Field Inventory Product</h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Add New Product Name </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="name" placeholder="Product Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> New Barcode </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="barcode" placeholder="Barcode Name" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Type Quantity </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="quantity" placeholder="Quantity" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Asset Type </label>

                                    <div class="col-sm-8">
                                        <select name="assettype">
                                            <?php 
                                            $ast=$obj->SelectAll("assettype");
                                            foreach($ast as $st):
                                            ?>
                                            <option value="<?php echo $st->legend; ?>"><?php echo $st->name; ?></option>
                                            <?php 
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Manufacturer's Serial Number </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="msn" placeholder="Manufacturer's Serial Number" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Select Supplier/Vendor </label>

                                    <div class="col-sm-8">
                                        <select name="sid">
                                            <?php 
                                            $ast=$obj->SelectAll("supplier");
                                            foreach($ast as $st):
                                            ?>
                                            <option value="<?php echo $st->id; ?>"><?php echo $st->name; ?></option>
                                            <?php 
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                
                                <div class="space-4"></div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Recive/Purchase Date </label>

                                    <div class="col-xs-6 col-sm-2">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="datepr" value="<?php echo date('Y-m-d'); ?>" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                            </div>
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> BDT Cost </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="bdtcost" placeholder="BDT Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> US Cost </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="uscost" placeholder="US Cost" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Dollar Rate </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="dollarrate" placeholder="Dollar Rate" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Market Value Per Unit </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="marketvalueperunit" placeholder="Market Value Per Unit" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                                                
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Item Disposition </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="disposition" placeholder="Item Disposition" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label no-padding-right" for="form-field-1"> Remarks </label>

                                    <div class="col-sm-8">
                                        <input type="text" id="form-field-1" name="remarks" placeholder="Remarks" class="col-xs-10 col-sm-5" />
                                    </div>
                                </div>
                                <div class="space-4"></div>
                                
                                
                                <div class="space-4"></div>
                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                        

                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>
		

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
                                $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
                        
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
                        
		</script>
    </body>
</html>
