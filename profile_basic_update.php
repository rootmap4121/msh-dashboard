<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Profile Info</a></li><li class='active'>User Profile</li>";
$table="employee";
$table1="employee_basic_info";
$table2="employee_present_address";
$table3="employee_emergency_contact";
$w=80; $h=100; $thumb="emp/";
$existemployee=array("emplid"=>$input_by);
if(isset($_POST['update']))
{
	$existemployee=array("emplid"=>$input_by,"father_husbandname"=>$_POST['father_husbandname'],"mothers_name"=>$_POST['mothers_name'],"blood_group"=>$_POST['blood_group'],"marital_status"=>$_POST['marital_status'],"religion"=>$_POST['religion'],"marrige_date"=>$_POST['marrige_date'],"home_district"=>$_POST['home_district'],"relative_in_sc"=>$_POST['relative_in_sc'],"relation"=>$_POST['relation'],"tin_number"=>$_POST['tin_number'],"driving_licence"=>$_POST['driving_licence'],"passport_number"=>$_POST['passport_number'],"date_of_expire"=>$_POST['date_of_expire']);
	if($obj->update($table1,$existemployee)==1)
	{
		if(!empty($_FILES['image']['name']))
        {
        	$files =$obj->image_bigcaption($w,$h,$thumb);
        	$photo=substr($files,4,1600);
			if($obj->update("employee",array("id"=>$input_by,"photo"=>$photo))==1)
			{
				$errmsg_arr[]= 'Successfully Saved';
				$errflag = true;
				if ($errflag) {
					$_SESSION['SMSG_ARR'] = $errmsg_arr;
					session_write_close();
					header("location: ./".$obj->filename());
					exit();
				}
			}
			else
			{
				$errmsg_arr[]= 'Successfully Not Saved';
				$errflag = true;
				if ($errflag) {
					$_SESSION['SMSG_ARR'] = $errmsg_arr;
					session_write_close();
					header("location: ./".$obj->filename());
					exit();
				}
			}
        }
        else 
        {
            	$errmsg_arr[]= 'Successfully Not Saved';
				$errflag = true;
				if ($errflag) {
					$_SESSION['SMSG_ARR'] = $errmsg_arr;
					session_write_close();
					header("location: ./".$obj->filename());
					exit();
				}
        }
	}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
          <style type="text/css">
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:3px; border:#333333 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{

	}
	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		background:#999;
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background:#CCC;
	}
</style>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    
                    
          <div class="row">
                <div class="col-xs-12">
                <?php
				$sqlprofile=$obj->SelectAllByID("employee",array("id"=>$input_by));
				if($obj->exists("employee",array("id"=>$input_by))!=0)
				foreach($sqlprofile as $profile):
				?>
                <form action="" method="post" name="basicupdate" enctype="multipart/form-data">
				<h1 class="header green">Profile Basic Information <span style="float:right;"><button type="submit" name="update" class="btn btn-primary"> Update Basic Info... </button></span></h1>
				<table class="TFtable" width="100%" border="1">
                  <tr>
    <td colspan="5">Basic Information :</td>
    </tr>
    <?php
	$sqlbasic=$obj->SelectAllByID($table1,$existemployee);
	foreach($sqlbasic as $basic):
	?>
  <tr>
    <td width="28%">&nbsp;</td>
    <td colspan="3" rowspan="4"><img style="height:180px; width:180px;" class="btn btn-success" src="emp/<?php echo $profile->photo; ?>"></td>
    </tr>
      <tr>
    <td width="28%">Your Photo : </td>
    </tr>
      <tr>
    <td width="28%">Change Photo : </td>
    </tr>
      <tr>
    <td width="28%" height="55"><input type="file" name="image" id="id-input-file-1" /></td>
    </tr>
  <tr>
    <td width="28%">Fathers / Husband Name :</td>
    <td width="26%"><input name="father_husbandname" value="<?php echo $basic->father_husbandname; ?>" type="text"></td>
    <td width="19%">Date of Birth :</td>
    <td width="13%"><?php echo $profile->dob; ?></td>
    </tr>
  <tr>
    <td>Mothers Name :</td>
    <td><input name="mothers_name" value="<?php echo $basic->mothers_name; ?>" type="text"></td>
    <td>Blood Group :</td>
    <td>
    <select name="blood_group">
    	<option selected value="A+">A+</option>
        <option <?php if($basic->blood_group=='O+'){ ?> selected <?php } ?> value="O+">O+</option>
        <option <?php if($basic->blood_group=='A-'){ ?> selected <?php } ?> value="A-">A-</option>
        <option <?php if($basic->blood_group=='B+'){ ?> selected <?php } ?> value="B+">B+</option>
        <option <?php if($basic->blood_group=='AB+'){ ?> selected <?php } ?> value="AB+">AB+</option>
        <option <?php if($basic->blood_group=='AB-'){ ?> selected <?php } ?> value="AB-">AB-</option>
        <option <?php if($basic->blood_group=='O-'){ ?> selected <?php } ?> value="O-">O-</option>
    </select>
    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Marital Status :</td>
    <td>
    <select name="marital_status">
    	<option <?php if($basic->marital_status=='Single'){ ?> selected <?php } ?> value="Single">Single</option>
        <option <?php if($basic->marital_status=='Married'){ ?> selected <?php } ?>value="Married">Married</option>
        <option <?php if($basic->marital_status=='Complcated'){ ?> selected <?php } ?>value="Complcated">Complcated</option>
        <option <?php if($basic->marital_status=='Divorce'){ ?> selected <?php } ?>value="Divorce">Divorce</option>
        <option <?php if($basic->marital_status=='Widow'){ ?> selected <?php } ?>value="Widow">Widow</option>
   	</select> 
    </td>
    </tr>
  <tr>
    <td>Religion :</td>
    <td>
    <select name="religion">
    	<option <?php if($basic->religion=='islam'){ ?> selected <?php } ?> value="islam">islam</option>
        <option <?php if($basic->religion=='hindu'){ ?> selected <?php } ?> value="hindu">hindu</option>
        <option <?php if($basic->religion=='Complcated'){ ?> selected <?php } ?> value="Complcated">Complcated</option>
        <option <?php if($basic->marital_status=='Divorce'){ ?> selected <?php } ?> value="Divorce">Divorce</option>
        <option <?php if($basic->marital_status=='Widow'){ ?> selected <?php } ?>value="Widow">Widow</option>
   	</select>
    
    </td>
    <td>Marrige Date :</td>
    <td><input name="marrige_date" value="<?php echo $basic->marrige_date; ?>" type="text"></td>
    </tr>
  <tr>
    <td>Home District :</td>
    <td><input name="home_district" value="<?php echo $basic->home_district; ?>" type="text"></td>
    <td>Relative in SC :</td>
    <td><input name="relative_in_sc" value="<?php echo $basic->relative_in_sc; ?>" type="text"></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>Relation :</td>
    <td><input name="relation" value="<?php echo $basic->relation; ?>" type="text"></td>
    </tr>
  <tr>
    <td>TIN Number :</td>
    <td><input name="tin_number" value="<?php echo $basic->tin_number; ?>" type="text"></td>
    <td>Driving Licence :</td>
    <td><input name="driving_licence" value="<?php echo $basic->driving_licence; ?>" type="text"></td>
    </tr>
  <tr>
    <td>Passport Number :</td>
    <td><input name="passport_number" value="<?php echo $basic->passport_number; ?>" type="text"></td>
    <td>Date of Expire :</td>
    <td><input name="date_of_expire" value="<?php echo $basic->date_of_expire; ?>" type="text"></td>
    </tr>
  <?php
  endforeach;
  ?>
  </table>
  </form>
				<?php
				endforeach;
				?>
            </div>
          </div>  
                    
                    
                    
                    
                    
                    <!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>


		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.hotkeys.min.js"></script>
		

		<!-- ace scripts -->
		<script type="text/javascript">
			jQuery(function($) {
				        $('#id-input-file-1 , #id-input-file-2').ace_file_input({
                no_file:'No File ...',
                btn_choose:'Choose',
                btn_change:'Change',
                droppable:false,
                onchange:null,
                thumbnail:false //| true | large
                //whitelist:'gif|png|jpg|jpeg'
                //blacklist:'exe|php'
                //onchange:''
                //
        });
                            
                                                                                        $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			
				$('.easy-pie-chart.percentage').each(function(){
				var barColor = $(this).data('color') || '#555';
				var trackColor = '#E2E2E2';
				var size = parseInt($(this).data('size')) || 72;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate:false,
					size: size
				}).css('color', barColor);
				});
			  
				///////////////////////////////////////////
	
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
			});
                        
                                            function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
		</script>
	</body>

<!-- Mirrored from 192.69.216.111/themes/preview/ace/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Sun, 10 Nov 2013 12:57:55 GMT -->
</html>
