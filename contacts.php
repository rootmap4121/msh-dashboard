<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>User Info</a></li><li class='active'>Admin List</li>";
$table="employee";

?>

<?php
if(@$_GET['action']=='pdf')
{
include("pdf/MPDF57/mpdf.php");
$html.=$obj->company_report_logo()." ".$obj->company_report_head();
$html.=$obj->company_report_name("Employee List");
$html.="<div class='table-responsive'><table id='sample-table-2' class='table table-hover'><thead>
        <tr class='headerrow'>
            <th class='center'>S/N</th>
            <th>Full Name </th>
            <th>Designation </th>
            <th>Mobile Number </th>
            <th>Address </th>
        </tr>
    </thead><tbody>";

$data=$obj->SelectAllorderBy($table);
    $x=1;
    foreach ($data as $row):
        
      $html.="<tr>
                <td class='center'>".$x."</td>
                <td>".$row->name."</td>
                <td><span class='label label-sm label-warning'>".$obj->SelectAllByVal("designation","id",$row->designation,name)."</span></td>
                <td><span class='label label-sm label-success'>".$row->contactnumber."</span></td>
                <td><span class='label label-sm label-danger'>".$row->address."</span></td>
            </tr>";
     $x++;
     endforeach;


$html.="</tbody></table></div>";

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
}
elseif(@$_GET['action']=='excel')
{

header('Content-type: application/excel');
$filename = 'employeelist.xls';
header('Content-Disposition: attachment; filename='.$filename);

$data = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Employee List</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$data .="<body>";
$data .=$obj->company_report_head()."Report Creation Date : ".date('d-m-Y H:i:s');
$data .=$obj->company_report_name("Employee List");
$data .="<table id='sample-table-2' class='table table-hover'>
    <thead>
        <tr>
            <th class='center'>S/N</th>
            <th>Full Name </th>
            <th>Designation </th>
            <th>Mobile Nuumber </th>
            <th>Address </th>
        </tr>
</thead>        
<tbody>";

$data .='</tr>';
    $ss=$obj->SelectAll("employee");
    $x=1;
    foreach ($ss as $row):
        
      $data .="<tr>
                <td class='center'>".$x."</td>
                <td>".$row->name."</td>
                <td>".$obj->SelectAllByVal("designation","id",$row->designation,name)."</td>
                <td>".$row->contactnumber."</td>
                <td>".$row->address."</td>
            </tr>";
     $x++;
     endforeach;


$data .="</tbody></table>";

$data .='</body></html>';

echo $data;

}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Employee List 
                                <a href="<?php echo $obj->filename(); ?>?action=pdf"><img src="images/pdf.png"></a> 
                                <a href="<?php echo $obj->filename(); ?>?action=excel"><img src="images/excel.png"></a>
                                
                                <span style="float: right;"><a href="#" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print Employee List</a></span></h3>
                                       
                        <div class="row" id="printablediv">
                                        
                            <div>
                            <?php echo $obj->company_report_logo(); ?>
                                            <?php echo $obj->company_report_head(); ?>
                                        <?php echo $obj->company_report_name("Employee List"); ?>
                            </div>
                            
                                    <div class="col-xs-12">
                                        
                                        
                                        
                                        <div class="table-responsive">
                                            <table id="sample-table-2" class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="center">S/N</th>
                                                        <th>Full Name </th>
                                                        <th>Designation </th>
                                                        <th>Mobile Nuumber </th>
                                                        <th>Address </th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                <?php
                                                $data=$obj->SelectAllorderBy($table);
                                                $x=1;
                                                if(!empty($data))
                                                foreach ($data as $row): ?>
                                                        <tr>
                                                            <td class="center"><?php echo $x; ?></td>
                                                            <td><?php echo $row->name; ?></td>
                                                            <td><span class="label label-sm label-warning">
                                                            <?php  
                                                            $qdes=$obj->SelectAllByID("designation",array("id"=>$row->designation));
                                                            if(!empty($qdes))
                                                            foreach ($qdes as $dd):
                                                              echo $dd->name;
                                                            endforeach;
                                                            ?></span></td>
                                                            
                                                            <td><span class="label label-sm label-success"><?php echo $row->contactnumber; ?></span></td>
                                                            <td><span class="label label-sm label-danger"><?php echo $row->address; ?></span></td>
                                                            
                                                        </tr>
                                                 <?php 
                                                 $x++;
                                                 endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {

			})
		</script>
    </body>
</html>
