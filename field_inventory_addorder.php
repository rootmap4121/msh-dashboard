<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Order </a></li><li class='active'>Product Order Form</li>";
$table="field_inventory_order_detail";
$table2="field_inventory_order";
$orderid=rand();
if(isset($_POST['submit']))
{

        $exist_array=array("orderid"=>$_POST['orderid']);
        
        if($obj->exists($table,$exist_array)==1)
        {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
        }
        else
        {
            $xprice=0;
            foreach ($_POST['pid'] as $key=>$val):
                if(!empty($val)):

                    $totalprice=$_POST['quantity'][$key]*$_POST['price'][$key];
                    $xprice+=$totalprice;
                    $obj->insert($table2,array("pid"=>$val,"orderid"=>$_POST['orderid'],"description"=>$_POST['description'][$key],"quantity"=>$_POST['quantity'][$key],"price"=>$_POST['price'][$key],"totalprice"=>$totalprice,"emplid"=>$input_by,"sid"=>$_POST['sid'],"date"=>  date('Y-m-d'),"status"=>1));
                endif;
            endforeach;
            
            $insert=array("orderid"=>$_POST['orderid'],"totalprice"=>$xprice,"emplid"=>$input_by,"sid"=>$_POST['sid'],"date"=>  date('Y-m-d'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                if($obj->amount_incre("supplier",array("id"=>$_POST['sid']),"amount",$xprice)==1)
                {
                        $errmsg_arr[] = 'Successfully Added';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                }
                else 
                {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        } 
                }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
            }
        }  
}
elseif (isset ($_POST['edit'])) {
                    //$success="Edit";
                    $updatearray=array("id"=>$_POST['id'],"orderid"=>$_POST['orderid']);
                    if($obj->update($table,$updatearray)==1)
                    { 
                        $errmsg_arr[] = 'Successfully Updated';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                    }
    
}
elseif (isset ($_GET['del'])=="delete") {
                    $delarray=array("id"=>$_GET['id']);
                    if($obj->delete($table,$delarray)==1)
                    { 
                        $errmsg_arr[] = 'Successfully Added';
                        $errflag = true;
                        if ($errflag) 
                        {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                    } 
                    else 
                    { 
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                        }
                    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->

                            
                            
                            
                            
                            
     <form class="form-horizontal" name="addproduct" role="form" action="" method="POST">                       

    <div class="space-6"></div>

    <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                    <div class="widget-box transparent invoice-box">
                            <div class="widget-header widget-header-large">
                                    <h3 class="grey lighter pull-left position-relative">
                                            <i class="icon-leaf green"></i>
                                            Field Inventory Product Order
                                    </h3>

                                    <div class="widget-toolbar no-border invoice-info">
                                            <span class="invoice-info-label">Invoice:</span>
                                            <span class="red">#<?php echo $orderid; ?> </span>
                                            <input type="hidden" value="<?php echo $orderid; ?>" name="orderid">
                                            <br />
                                            <span class="invoice-info-label">Date:</span>
                                            <span class="blue"><?php echo $obj->today(); ?></span>
                                    </div>

                                    <!--<div class="widget-toolbar hidden-480">
                                            <a href="#">
                                                    <i class="icon-print"></i>
                                            </a>
                                    </div>-->
                            </div>

                            <div class="widget-body">
                                    <div class="widget-main padding-24">
                                            <div class="row">
                                                    <div class="col-sm-6">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
                                                                            <b>Company Info</b>
                                                                    </div>
                                                            </div>

                                                            <div class="row">
                                                                    <ul class="list-unstyled spaced">
                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    MSH Field Office 
                                                                            </li>

                                                                            <li>
                                                                                    <i class="icon-caret-right blue"></i>
                                                                                    Admin Name : <?php $admin=$obj->SelectAllByID("employee",array("id"=>$input_by)); foreach ($admin as $ad): echo $ad->name;  endforeach;  ?>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->

                                                    <div class="col-sm-6">
                                                            <div class="row">
                                                                    <div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
                                                                            <b>Supplier Info</b>
                                                                    </div>
                                                            </div>

                                                            <div>
                                                                    <ul class="list-unstyled  spaced">
                                                                            <li>
                                                                                    <i class="icon-caret-right green"></i>
                                                                                    Select Supplier 
                                                                                    <select name="sid">
                                                                                        <option value="">Select Supplier</option>
                                                                                        <?php $supplier=$obj->SelectAll("supplier"); foreach($supplier as $sup):  ?>
                                                                                        <option value="<?php  echo $sup->id;  ?>"><?php  echo $sup->name;  ?></option> 
                                                                                        <?php endforeach;  ?>
                                                                                        
                                                                                        
                                                                                    </select>
                                                                            </li>
                                                                    </ul>
                                                            </div>
                                                    </div><!-- /span -->
                                            </div><!-- row -->

                                            <div class="space"></div>

                                            <div>
                                                    <table   id="itemTable" class="table table-striped table-bordered">
                                                            <thead>
                                                                    <tr>
                                                                            
                                                                            <th>Product</th>
                                                                            <th class="hidden-xs">Description</th>
                                                                            <th class="hidden-480">Quantity</th>
                                                                            <th class="hidden-480">Price</th>
                                                                    </tr>
                                                            </thead>

                                                            <tbody>
                           
                            <tr id="InputRow">
                                    
                                    <td>
                                        <select name="pid[]"   class="form-control" id="form-field-select-1">
                                                <option value="">Select Product</option>
                                                <?php $supplier=$obj->SelectAll("fieldproduct"); foreach($supplier as $sup):  ?>
                                                <option value="<?php  echo $sup->id;  ?>"><?php  echo $sup->barcode;  ?> - <?php  echo $sup->name;  ?> - <?php  echo $sup->msn;  ?></option> 
                                                <?php endforeach;  ?>


                                            </select>
                                    </td>
                                    <td class="hidden-xs"><input type="text" id="form-field-3" name="description[]" id="description[]" placeholder="Other Description" class="col-xs-10 col-sm-10" /></td>
                                    <td class="hidden-480"><input type="text" id="form-field-1" name="quantity[]" id="quantity[]" placeholder="Quantity" class="col-xs-10 col-sm-5" /></td>
                                    <td class="hidden-480"><input type="text" id="form-field-1" name="price[]" id="price[]" placeholder="price" class="col-xs-10 col-sm-5" /></td>
                            
                            </tr>
                           

                                                                    
                                                            </tbody>
                                                    </table>
                                                <button class="btn btn-lg btn-success" type="button" onclick="return addTableRow('#itemTable');return false;">
					    <i class="icon-plus"></i> Add More Line
					   </button>
                                            <div class="clearfix"></div>
                                            <br>
                                            </div>

                                            <div class="hr hr8 hr-double hr-dotted"></div>

                                            <div class="row">
                                                    <!--<div class="col-sm-5 pull-right">
                                                            <h4 class="pull-right">
                                                                    Total amount :
                                                                    <span class="red">$395</span>
                                                            </h4>
                                                    </div>-->
                                                    <div class="col-sm-7 pull-left"> <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-ok bigger-110"></i>Save</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div></div>
                                            </div>

                                            <div class="space-6"></div>
                                            <div class="well">
                                    
                                            </div>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>

    <!-- PAGE CONTENT ENDS -->

                            
                            
     </form>                       
                            
                            
                            
                            
                            
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>

                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
                    function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
                    
		</script>
    </body>
</html>
