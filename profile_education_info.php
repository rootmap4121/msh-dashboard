<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Profile Info</a></li><li class='active'>User Profile</li>";
$table="employee";
$table1="employee_basic_info";
$table2="employee_present_address";
$table3="employee_emergency_contact";
$table4="employee_educational_info";
$existemployee=array("emplid"=>$input_by);
if(isset($_POST['update']))
{
	foreach($_POST['title'] as $index=>$bb)
	{
		if($_POST['eid'][$index]!=0)
		{
		$obj->update($table4,array("id"=>$_POST['eid'],"title"=>$bb,"board"=>$_POST['board'][$index],"year"=>$_POST['year'][$index],"division"=>$_POST['division'][$index],"gpa"=>$_POST['gpa'][$index]));
		}
		else
		{
		$obj->insert($table4,array("emplid"=>$input_by,"title"=>$bb,"board"=>$_POST['board'][$index],"year"=>$_POST['year'][$index],"division"=>$_POST['division'][$index],"gpa"=>$_POST['gpa'][$index]));	
		}
	}
			
	$errmsg_arr[]= 'Successfully Saved';
	$errflag = true;
	if ($errflag) 
	{
		$_SESSION['SMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: ./".$obj->filename());
		exit();
	}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
          <style type="text/css">
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:3px; border:#333333 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{

	}
	/*  Define the background color for all the ODD background rows background:#999; */
	.TFtable tr:nth-child(odd){ 
		
	}
	/*  Define the background color for all the EVEN background rows background:#CCC; */
	.TFtable tr:nth-child(even){
		
	}
</style>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    
                    
          <div class="row">
                <div class="col-xs-12">
                <?php
				$sqlprofile=$obj->SelectAllByID("employee",array("id"=>$input_by));
				if($obj->exists("employee",array("id"=>$input_by))!=0)
				foreach($sqlprofile as $profile):
				?>
                <form action="" method="post">
				<h1 class="header green">Educational Information <span style="float:right;"><button type="submit" name="update" class="btn btn-primary"> Update Educational Info... </button></span></h1>
				<table class="TFtable"  id="itemTable" width="100%" border="1">
				  <tr>
    <td colspan="5">Educational Information...</td>
    </tr>
  <tr>
    <td width="25%" height="27">Degree Title</td>
    <td width="23%">Board</td>
    <td width="11%">Year</td>
    <td width="20%">Division / Class</td>
    <td width="21%">GPA / Grade</td>
    </tr>
    <?php
	$datas=$obj->SelectAllByID($table4,$existemployee);
	foreach($datas as $data):
	?>
  	<tr>
    <td>
    <input name="title[]" value="<?php echo $data->title; ?>" type="text">
    <input name="eid[]" value="<?php echo $data->id; ?>" type="hidden">
    </td>
    <td>
    <select name="board[]">
      <option <?php if($data->board=='Barisal'){ ?> selected <?php } ?> value="Barisal">Barisal</option>
      <option <?php if($data->board=='Chittagong'){ ?> selected <?php } ?>  value="Chittagong">Chittagong</option>
      <option <?php if($data->board=='Comilla'){ ?> selected <?php } ?>  value="Comilla">Comilla</option>
      <option <?php if($data->board=='Dhaka'){ ?> selected <?php } ?>  selected value="Dhaka">Dhaka</option>
      <option <?php if($data->board=='Dinajpur'){ ?> selected <?php } ?>  value="Dinajpur">Dinajpur</option>
      <option <?php if($data->board=='Jessore'){ ?> selected <?php } ?>  value="Jessore">Jessore</option>
      <option <?php if($data->board=='Rajshahi'){ ?> selected <?php } ?>  value="Rajshahi">Rajshahi</option>
      <option <?php if($data->board=='Sylhet'){ ?> selected <?php } ?>  value="Sylhet">Sylhet</option>
      <option <?php if($data->board=='Madrasah'){ ?> selected <?php } ?>  value="Madrasah">Madrasah</option>
      <option <?php if($data->board=='Technical'){ ?> selected <?php } ?>  value="Technical">Technical</option>
      <option <?php if($data->board=='DIBS(Dhaka)'){ ?> selected <?php } ?>  value="DIBS(Dhaka)">DIBS(Dhaka)</option>
    </select>
    </td>
    <td>    
        <select class="width-10" name="year[]">
            <option value="">Select Year</option>
            <?php for ($k = date('Y'); $k >= date('Y') - 100; $k--) : ?>
                <option <?php if($data->year==$k){ ?> selected <?php } ?> value="<?php echo $k; ?>"><?php echo $k; ?></option>
            <?php endfor; ?>
        </select>
    </td>
    <td><input name="division[]" type="text" id="division[]" value="<?php echo $data->division; ?>"></td>
    <td><input name="gpa[]" type="text" id="gpa[]" value="<?php echo $data->gpa; ?>"></td>
    </tr>
    <?php endforeach; ?> 
    <tr id="InputRow">
    <td>
    <input name="title[]" type="text">
    <input name="eid[]" type="hidden" value="0">
    </td>
    <td>
    <select name="board[]">
      <option value="barisal">Barisal</option>
      <option value="chittagong">Chittagong</option>
      <option value="comilla">Comilla</option>
      <option selected value="dhaka">Dhaka</option>
      <option value="dinajpur">Dinajpur</option>
      <option value="jessore">Jessore</option>
      <option value="rajshahi">Rajshahi</option>
      <option value="sylhet">Sylhet</option>
      <option value="madrasah">Madrasah</option>
      <option value="tec">Technical</option>
      <option value="dibs">DIBS(Dhaka)</option>
    </select>
    </td>
    <td>    
        <select class="width-10" name="year[]">
            <option value="">Select Year</option>
            <?php for ($k = date('Y'); $k >= date('Y') - 100; $k--) : ?>
                <option value="<?php echo $k; ?>"><?php echo $k; ?></option>
            <?php endfor; ?>
        </select>
    </td>
    <td><input name="division[]" type="text" id="division[]"></td>
    <td><input name="gpa[]" type="text" id="gpa[]"></td>
    </tr>
    </table>
    </form>
     <button type="button" class="btn btn-success" onclick="return addTableRow('#itemTable');return false;"> Add More Rows </button>           
                
				<?php
				endforeach;
				?>
            </div>
          </div>  
                    
                    
                    
                    
                    
                    <!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>


		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.hotkeys.min.js"></script>
		<script type="text/javascript">
                    function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
                    
		</script>

		<!-- ace scripts -->
		<script type="text/javascript">
			jQuery(function($) {
                            
                                                                                        $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			
				$('.easy-pie-chart.percentage').each(function(){
				var barColor = $(this).data('color') || '#555';
				var trackColor = '#E2E2E2';
				var size = parseInt($(this).data('size')) || 72;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate:false,
					size: size
				}).css('color', barColor);
				});
			  
				///////////////////////////////////////////
	
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
			});
                        
                                            function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
		</script>
	</body>

<!-- Mirrored from 192.69.216.111/themes/preview/ace/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Sun, 10 Nov 2013 12:57:55 GMT -->
</html>
