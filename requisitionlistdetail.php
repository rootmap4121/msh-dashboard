<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Requisation Info</a></li><li class='active'>Add Store Requisation Form</li>";
$table="requisationlist";
$table1="requisition";
if(isset ($_POST['edit'])) 
{                    
                    foreach($_POST['req_oid'] as $index=>$aa):
                    if(!empty($aa))
                    { 
                        $obj->update($table1,array("id"=>$aa,"quantityissued"=>$_POST['quantityissued'][$index])); 

                    }
                    endforeach;
                        $errmsg_arr[]= 'Successfully Modified';
                        $errflag = true;
                        if ($errflag) 
                        {
                            $_SESSION['SMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename()."?id=".$_POST['id']);
                            exit();
                        }
    
}
extract($_GET);

if(@$_GET['action']=='pdf')
{
include("pdf/MPDF57/mpdf.php");
$html .=$obj->company_report_logo()." ".$obj->company_report_head();
$html .=$obj->company_report_name("Store Requisation Detail");
    $datas=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
    foreach ($datas as $data):
       $html .="<table width='100%' border='1'  id='sample-table-2' class='table table-hover'>
                            <tr>
                              <td height='31' valign='top'><strong>Project Name : ".$obj->SelectAllByVal("project","id",$data->projectid,"name")."</strong></td>
                              <td valign='top'><strong>Signature : </strong></td>
                            </tr>
                            <tr>
                                <td height='31' valign='top'><strong>Requesting : </strong> ".$obj->SelectAllByVal("employee","id",$data->empid,"name")."</td>
                                <td valign='top'><strong>Requisation ID : </strong> ".$data->empid."</td>
                                </tr>
                              <tr>
                                <td  valign='top'><strong>Designation :</strong> ".$obj->emp_designation($data->empid)."</td>
                                <td valign='top'><strong>Date : </strong>".$data->date."</td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1'  id='sample-table-1' class='table table-hover'>
  <tr>
    <td  valign='top'><strong>Product Name</strong></td>
    <td valign='top'><strong>Requested Quantity</strong></td>
    <td valign='top'><strong>Quantity Issued</strong></td>
    <td  valign='top'><strong>Description</strong></td>
    </tr>";

	$sqlpro=$obj->SelectAllByID("requisition",array("req_id"=>$id));
	foreach($sqlpro as $pro):

  $html .="<tr>
    <td valign='top'>".$obj->SelectAllByVal("product","id",$pro->pid,"name")."</td>
    <td valign='top'>".$pro->quantity."</td>
    <td valign='top'>".$pro->quantityissued."</td>
    <td valign='top'>".$pro->description."</td>
    </tr>";

	endforeach;

  $html .="<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0'  id='sample-table-2' class='table table-hover'>
  <tr>
    <td width='86%' height='64' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong> <br/>".$obj->SelectAllByVal("employee","id",$data->supervisor,"name")." - ".$obj->emp_leave_status($data->status)."</td>
    <td width='14%' valign='top' align='left'><strong>Signature : </strong></td>
    </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>Note : </td>
    </tr>
  </table>";


endforeach;

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('pdf/MPDF57/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('leave.pdf','I');
}
elseif(@$_GET['action']=='excel')
{

header('Content-type: application/excel');
$filename = 'leaveapplicationlist.xls';
header('Content-Disposition: attachment; filename='.$filename);

$html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Store Requisation Detail</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
</head>';

$html .="<body>";
$html .=$obj->company_report_head()."Report Creation Date : ".date('d-m-Y H:i:s');
$html .=$obj->company_report_name("Store Requisation Detail");

//start table
    $datas=$obj->SelectAllByID($table,array("id"=>$_GET['id']));
    foreach ($datas as $data):
       $html .="<table width='100%' border='1' cellpadding='4' cellspacing='0'>
                            <tr>
                              <td height='31' valign='top'><strong>Project Name : ".$obj->SelectAllByVal("project","id",$data->projectid,"name")."</strong></td>
                              <td valign='top'><strong>Signature : </strong></td>
                            </tr>
                            <tr>
                                <td height='31' valign='top'><strong>Requesting : </strong> ".$obj->SelectAllByVal("employee","id",$data->empid,"name")."</td>
                                <td valign='top'><strong>Requisation ID : </strong> ".$data->empid."</td>
                                </tr>
                              <tr>
                                <td width='541' valign='top'><strong>Designation :</strong> ".$obj->emp_designation($data->empid)."</td>
                                <td valign='top'><strong>Date : </strong>".$data->date."</td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='23%' valign='top'><strong>Product Name</strong></td>
    <td width='24%' valign='top'><strong>Requested Quantity</strong></td>
    <td width='25%' valign='top'><strong>Quantity Issued</strong></td>
    <td width='28%' valign='top'><strong>Description</strong></td>
    </tr>";

	$sqlpro=$obj->SelectAllByID("requisition",array("req_id"=>$id));
	foreach($sqlpro as $pro):

  $html .="<tr>
    <td valign='top'>".$obj->SelectAllByVal("product","id",$pro->pid,"name")."</td>
    <td valign='top'>".$pro->quantity."</td>
    <td valign='top'>".$pro->quantityissued."</td>
    <td valign='top'>".$pro->description."</td>
    </tr>";

	endforeach;

  $html .="<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='56%' height='64' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong> ".$obj->SelectAllByVal("employee","id",$data->supervisor,"name")." - ".$obj->emp_leave_status($data->status)."</td>
    <td width='44%' valign='top' align='right'><strong>Signature : </strong></td>
    </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>Note : </td>
    </tr>
  </table>";

endforeach;
//end table

$html .="</tbody></table>";

$html .='</body></html>';

echo $html;

}
elseif(@$_GET['action']=='approve')
{
    $array=array("id"=>$_GET['id'],"status"=>2);
    if($obj->update($table,$array)==1)
    {
		$obj->notification_check_user_update($_GET['id'],"1",$input_by);
                    $errmsg_arr[]= 'Successfully Approved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
elseif(@$_GET['action']=='reject')
{
	$obj->notification_check_user_update($_GET['id'],"1",$input_by);
    $array=array("id"=>$_GET['id'],"status"=>3);
    if($obj->update($table,$array)==1)
    {
		$obj->notification_check_user_update($id,"1",$input_by);
                    $errmsg_arr[]= 'Successfully Reject';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename()."?id=".$_GET['id']);
                        exit();
                    }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
    </head>

    <body>
    <form name="edit" action="" method="post">
    <input type="hidden" name="id" value="<?php echo $id;  ?>">
        <?php include('class/header.php'); ?>
 <?php
                            $datas=$obj->SelectAllByID($table,array("id"=>$id));
                            foreach ($datas as $data):
                            
                            
                            ?>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                     <h3 class="header smaller lighter blue">Requisation Detail Form-<?php echo date('Y'); ?>
                         <a target="_blank" href="<?php echo $obj->filename(); ?>?action=pdf&amp;id=<?php echo $id; ?>"><img src="images/pdf.png"></a> 
                                <a target="_blank" href="<?php echo $obj->filename(); ?>?action=excel&amp;id=<?php echo $id; ?>"><img src="images/excel.png"></a>
                                <?php 
								if($data->supervisor==$input_by)
								{
									if($data->status==1)
									{
									?> 
									<button class="btn btn-primary" type="submit" name="edit">Save</button>
								  <?php 
									}
									
								}
								?>
                                
                         <span style="float: right;"><a href="#" target="_blank" style="text-decoration: none;"  onclick="javascript:printDiv('printablediv')"><i class="icon-print"></i> Print All</a></span></h3>
                           
                        <?php include('class/esm.php'); ?>
                        <div class="row" id="printablediv">
                        
                        
                        
                        <div class="col-xs-12">
                            <!-- PAGE CONTENT BEGINS -->
                                   <div>
                                    <?php echo $obj->company_report_logo(); ?>
                                    <?php echo $obj->company_report_head(); ?>
                                    <?php echo $obj->company_report_name("Requisation Detail"); ?>
                                    </div>
                           
                          <table width='100%' border='1' cellpadding='4' cellspacing='0'>
                            <tr>
                              <td height='31' valign='top'><strong>Project Name : <?php echo $obj->SelectAllByVal("project","id",$data->projectid,"name"); ?></strong></td>
                              <td valign='top'><strong>Signature : </strong></td>
                            </tr>
                            <tr>
                                <td height='31' valign='top'><strong>Requesting : </strong> <?php echo $obj->SelectAllByVal("employee","id",$data->empid,"name"); ?></td>
                                <td valign='top'><strong>Requisation ID : </strong> <?php echo $data->empid; ?></td>
                                </tr>
                              <tr>
                                <td width='541' valign='top'><strong>Designation :</strong> <?php echo $obj->emp_designation($data->empid); ?></td>
                                <td valign='top'><strong>Date : </strong><?php echo $data->date; ?></td>
                              </tr>
                            </table>


							<br>

                          <h3>&nbsp;</h3>
                          <table  width='100%' border='1' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='23%' valign='top'><strong>Product Name</strong></td>
    <td width='24%' valign='top'><strong>Requested Quantity</strong></td>
    <td width='25%' valign='top'><strong>Quantity Issued</strong></td>
    <td width='28%' valign='top'><strong>Description</strong></td>
    </tr>
    <?php
	$sqlpro=$obj->SelectAllByID("requisition",array("req_id"=>$id));
	foreach($sqlpro as $pro):
	?>
  <tr>
    <td valign='top'><?php echo $obj->SelectAllByVal("product","id",$pro->pid,"name"); ?></td>
    <td valign='top'><?php echo $pro->quantity; ?></td>
    <td valign='top'>
	<?php 
	if($data->supervisor==$input_by)
    {
        if($data->status==1)
        {
        ?> 
        <input type="text" name="quantityissued[]" value="<?php echo $pro->quantityissued;  ?>">
        <input type="hidden" name="req_oid[]" value="<?php echo $pro->id;  ?>">
      <?php 
        }
        elseif($data->status==2){
        ?>
      <?php echo $pro->quantityissued;  ?>
      <?php
        }
    }
    else
    {
         echo $pro->quantityissued; 
    }
	?>
    </td>
    <td valign='top'><?php echo $pro->description; ?></td>
    </tr>
    <?php
	endforeach;
	?>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
 
</table>
<br>
                          <table  width='100%' border='0' cellpadding='4' cellspacing='0'>
  <tr>
    <td width='56%' height='64' valign='top'><strong>Request Recive By  ( Supervisor ) :</strong> 
        <br />
        <?php echo $obj->SelectAllByVal("employee","id",$data->supervisor,"name");
    
    if($data->supervisor==$input_by)
    {
        if($data->status==1)
        {
        ?> 
        <a href="<?php echo $obj->filename(); ?>?action=approve&AMP;id=<?php echo $id; ?>"> <span class="label label-sm label-warning"> Approve </span></a>
      <a href="<?php echo $obj->filename(); ?>?action=reject&AMP;id=<?php echo $id; ?>"> <span class="label label-sm label-danger"> Reject </span></a>
      
            <?php 
        }
        elseif($data->status==2){
        ?>
      <span class="label label-sm label-success"> <?php echo $obj->emp_leave_status($data->status); ?></span>
      <?php
        }
    }
    else
    {
         echo $obj->emp_leave_status($data->status); 
    }
    ?></td>
    <td width='44%' valign='top' align='right'><strong>Signature : </strong></td>
    </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>&nbsp;</td>
  </tr>
  <tr>
    <td height='31' colspan='2' valign='top'>Note : </td>
    </tr>
  </table>
  
  


                            
                           
                            <?php endforeach; ?>    								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
			jQuery(function($) {
                                                            $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			})
		</script>
        </form>
    </body>
</html>
