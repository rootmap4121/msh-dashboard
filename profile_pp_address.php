<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Profile Info</a></li><li class='active'>User Profile</li>";
$table="employee";
$table1="employee_basic_info";
$table2="employee_present_address";
$table3="employee_emergency_contact";
$existemployee=array("emplid"=>$input_by);

if(isset($_POST['update']))
{
	$obj->update2($table2,array("emplid"=>$input_by,"address"=>$_POST['address1'],"phone"=>$_POST['phone1'],"fax"=>$_POST['fax1']),"status","1");
	$obj->update2($table2,array("emplid"=>$input_by,"address"=>$_POST['address2'],"phone"=>$_POST['phone2'],"fax"=>$_POST['fax2']),"status","2");
	
	$errmsg_arr[]= 'Successfully Saved';
	$errflag = true;
	if ($errflag) {
			$_SESSION['SMSG_ARR'] = $errmsg_arr;
			session_write_close();
			header("location: ./".$obj->filename());
			exit();
	}	
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
          <style type="text/css">
	.TFtable{
		width:100%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:3px; border:#333333 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{

	}
	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		background:#999;
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background:#CCC;
	}
</style>
    </head>

    <body>
<?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

<?php
include('class/esm.php');
?>
                    
                    
          <div class="row">
                <div class="col-xs-12">
                <?php
				$sqlprofile=$obj->SelectAllByID("employee",array("id"=>$input_by));
				if($obj->exists("employee",array("id"=>$input_by))!=0)
				foreach($sqlprofile as $profile):
				?>
                <form action="" method="post">
				<h1 class="header green">Profile Permanent &amp; Present Information <span style="float:right;"><button type="submit" name="update" class="btn btn-primary"> Update Information... </button></span></h1>
				<table class="TFtable" width="100%" border="1">
				  <tr>
    <td colspan="2">Present Address </td>
    <td colspan="3">Permanent Address </td>
  </tr>

  <tr>
    <td width="14%">Address :</td>
    <td width="31%"><input name="address1" class="col-sm-8" value="<?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","1","address"); ?>" type="text"></td>
    <td width="18%">Address : </td>
    <td width="37%" colspan="2"><input name="address2" class="col-sm-8"  value="<?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","2","address"); ?>" type="text"></td>
    </tr>
  <tr>
    <td>Phone : </td>
    <td><input name="phone1" class="col-sm-8"  value="<?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","1","phone"); ?>" type="text"></td>
    <td>Phone : </td>
    <td colspan="2"><input class="col-sm-8"  name="phone2" value="<?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","2","phone"); ?>" type="text"></td>
    </tr>
  <tr>
    <td>Fax :</td>
    <td><input class="col-sm-8"  value="<?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","1","fax"); ?>" name="fax1" type="text"></td>
    <td>Fax : </td>
    <td colspan="2"><input class="col-sm-8"  value="<?php echo $obj->SelectAllByVal2($table2,"emplid",$input_by,"status","2","fax"); ?>" name="fax2" type="text"></td>
    </tr>

    </table>
    </form>
  <br>
  <?php
				endforeach;
				?>
            </div>
          </div>  
                    
                    
                    
                    
                    
                    <!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                 <?php echo $obj->bodyfooter(); ?>


		<!-- page specific plugin scripts -->

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->

		<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/jquery.gritter.min.js"></script>
		<script src="assets/js/bootbox.min.js"></script>
		<script src="assets/js/jquery.slimscroll.min.js"></script>
		<script src="assets/js/jquery.easy-pie-chart.min.js"></script>
		<script src="assets/js/jquery.hotkeys.min.js"></script>
		

		<!-- ace scripts -->
		<script type="text/javascript">
			jQuery(function($) {
                            
                                                                                        $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
			
				$('.easy-pie-chart.percentage').each(function(){
				var barColor = $(this).data('color') || '#555';
				var trackColor = '#E2E2E2';
				var size = parseInt($(this).data('size')) || 72;
				$(this).easyPieChart({
					barColor: barColor,
					trackColor: trackColor,
					scaleColor: false,
					lineCap: 'butt',
					lineWidth: parseInt(size/10),
					animate:false,
					size: size
				}).css('color', barColor);
				});
			  
				///////////////////////////////////////////
	
                            $("#topsix").keyup(function(){
                            var filter = $(this).val(), count = 0;
                            $(".topsix").each(function(){
                                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                                    $(this).fadeOut();
                                } else {
                                    $(this).show();
                                    count++;
                                }
                            });
                            var numberItems = count;
                            $("#topsix-count").text("Result= "+count);
                            });
			});
                        
                                            function addTableRow(table)
                    {
                        var $tr = $(table+' tbody:first').children("tr:last").clone();
                            $tr.find("input[type!='hidden'][name*=first_name],select,button").clone();
                            $tr.find("button[name*='ViewButton']").remove();
                        $(table+' tbody:first').children("tr:last").after($tr);
                    }
		</script>
	</body>

<!-- Mirrored from 192.69.216.111/themes/preview/ace/profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Sun, 10 Nov 2013 12:57:55 GMT -->
</html>
