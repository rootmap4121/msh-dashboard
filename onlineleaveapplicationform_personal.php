<?php
include('class/auth.php');
$page = "<li><i class='icon-group group-icon'></i><a href='#'>Online Leave Application Info</a></li><li class='active'>Add Online Leave Application Form</li>";
$table="requisation_leaveapplication_form";
if(isset($_POST['submit']))
{
    if(!empty($_POST['supervisor']))
    {
        $insert=array("emplid"=>$input_by,"supervisor"=>$_POST['supervisor'],"leave_type"=>$_POST['leave_type'],"description"=>$_POST['description'],"datequantity"=>$obj->getDays($_GET['startdate'], $_GET['enddate']),"startdate"=>$_POST['startdate'],"enddate"=>$_POST['enddate'],"startdatetime"=>$_POST['startdatetime'],"enddatetime"=>$_POST['enddatetime'],"phone"=>$_POST['phone'],"incharge"=>$_POST['incharge'],"date"=>date('Y-m-d'),"year"=>date('Y'),"status"=>1);
            if($obj->insert($table,$insert)==1)
            {
                    $errmsg_arr[]= 'Successfully Saved';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./".$obj->filename());
                        exit();
                    }
            }
            else 
             {
                        $errmsg_arr[]= 'Failed';
                        $errflag = true;
                        if ($errflag) {
                            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                            session_write_close();
                            header("location: ./".$obj->filename());
                            exit();
                        }
            }
        } 
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $obj->bodyhead(); ?>
<script>
function showUser(str)
{
    if (str=="")
      {
      document.getElementById("txtHint").innerHTML="";
      return;
      }
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        }
      }
    startdate = document.getElementById("startdate").value;
    enddate = document.getElementById("enddate").value;
    xmlhttp.open("GET","datecount.php?q="+str+"&sdate="+startdate+"&edate="+enddate,true);
    xmlhttp.send();
}
</script>
    </head>

    <body>
        <?php include('class/header.php'); ?>

        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    
                    <?php
                    include('class/esm.php');
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="header smaller lighter blue">Leave Application Form </h3>
                            <!-- PAGE CONTENT BEGINS -->

                            <form class="form-horizontal" name="designationadd" role="form" action="" method="POST">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Type </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="leave_type" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("leave_type");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> From </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control date-picker" name="startdate" value="<?php echo date('Y-m-d'); ?>" id="startdate" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                                    <select name="startdatetime">
                                                        <option value="1">First Half</option>
                                                        <option value="2">2nd Half</option>
                                                    </select>
                                            </div>

                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> To </label>

                                    <div class="col-xs-6 col-sm-3">
                                            <div class="input-group">
                                                <input class="form-control date-picker"  onchange="showUser(this.value)"  name="enddate" value="<?php echo date('Y-m-d'); ?>" id="enddate" type="text" data-date-format="yyyy-mm-dd" />
                                                    <span class="input-group-addon">
                                                            <i class="icon-calendar bigger-110"></i>
                                                    </span>
                                                   <select name="enddatetime">
                                                        <option value="1">First Half</option>
                                                        <option value="2">2nd Half</option>
                                                    </select>
                                            </div>
                                    </div>
                                </div> 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Leave Counts </label>

                                    <div class="col-sm-9">
                                        <div id="txtHint" style="margin-top: 4px; color: #f00; border: 1px solid;" name="datequantity" class="col-xs-6 col-sm-2">0 Days</div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Super Visor<font color="green">*</font> </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="supervisor" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("employee");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>                                
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Incharge<font color="red">*</font> </label>
                                    <div class="col-sm-9">
                                        <select id="form-field-1" name="incharge" class="col-xs-10 col-sm-5">
                                            <?php 
                                            $empsql=$obj->SelectAll("employee");
                                            foreach($empsql as $emp):
                                            ?>
                                            <option value="<?php echo $emp->id; ?>"><?php echo $emp->name; ?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>                                 
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone </label>
                                    <div class="col-sm-9">
                                        <input type="text" id="form-field-1" name="phone" class="col-xs-10 col-sm-5"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Reason </label>
                                    <div class="col-sm-9">
                                        <textarea type="text" id="form-field-1" name="description" class="col-xs-10 col-sm-5"></textarea>
                                    </div>
                                </div> 
                                
                                
                                <div class="space-4"></div>

                                <div class="clearfix form-actions">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button class="btn btn-info" type="submit" name="submit"><i class="icon-fighter-jet bigger-110"></i>Send For Approval</button>
                                        &nbsp; &nbsp; &nbsp;
                                        <button class="btn" type="reset"><i class="icon-undo bigger-110"></i>Reset</button>
                                    </div>
                                </div>
                            </form>
                                								<div class="hr hr-18 dotted hr-double"></div>

								

				
                                                                
                                                                								<div id="modal-table" class="modal fade" tabindex="-1">
									
                                <!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.page-content -->
            </div><!-- /.main-content -->

            <?php
//include('class/colornnavsetting.php');
            include('class/footer.php');
            ?>


                <?php echo $obj->bodyfooter(); ?>

		<!-- inline scripts related to this page -->
		<script type="text/javascript">
                jQuery(function($) 
                {
                    $('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
                             $(this).prev().focus();
                     });
                     $('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
                             $(this).next().focus();
                     });
                     

                })               
		</script>
    </body>
</html>
