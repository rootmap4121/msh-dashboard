<?php
extract($_GET);
include('class/auth.php');
$table3="field_inventory_stock";
$table="field_inventory_stockreport";
if(!empty($quantity))
{
    if($pquantity>=$quantity)
    {
        if($obj->exists($table3,array("pid"=>$pid))!=0)
        {
            if($obj->amount_incre($table3,array("pid"=>$pid),"quantity",$quantity)==1)
            {
                $reportarray=array("oid"=>$oid,"pid"=>$pid,"orderid"=>$orderid,"quantity"=>$quantity,"price"=>$price,"sid"=>$sid,"emplid"=>$emplid,"date"=>date('Y-m-d'),"status"=>1);
                if($obj->insert($table,$reportarray)==1)
                {
                    $errmsg_arr[]= 'Successfully Added';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./field_inventory_orderstockin.php?id=".$id);
                        exit();
                    }
                }
                else
                {
                    $errmsg_arr[]='Failed Please Try Again';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./field_inventory_orderstockin.php?id=".$id);
                        exit();
                    }
                }
            }
            else 
            {
                $errmsg_arr[]='Failed Please Try Again';
                $errflag = true;
                if ($errflag) 
                {
                    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./field_inventory_orderstockin.php?id=".$id);
                    exit();
                }
            }
        }
        else 
        {
            if($obj->insert($table3,array("pid"=>$pid,"quantity"=>$quantity,"price"=>$price,"reorder"=>$reorder,"date"=>date('Y-m-d'),"status"=>1))==1)
            {
                $reportarray=array("oid"=>$oid,"pid"=>$pid,"orderid"=>$orderid,"quantity"=>$quantity,"price"=>$price,"sid"=>$sid,"emplid"=>$emplid,"date"=>date('Y-m-d'),"status"=>1);
                if($obj->insert($table,$reportarray)==1)
                {
                    $errmsg_arr[]= 'Successfully Added';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['SMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./field_inventory_orderstockin.php?id=".$id);
                        exit();
                    }
                }
                else
                {
                    $errmsg_arr[]='Failed Please Try Again';
                    $errflag = true;
                    if ($errflag) 
                    {
                        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                        session_write_close();
                        header("location: ./field_inventory_orderstockin.php?id=".$id);
                        exit();
                    }
                }
            }
            else 
            {
                $errmsg_arr[]='Failed Please Try Again';
                $errflag = true;
                if ($errflag) 
                {
                    $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                    session_write_close();
                    header("location: ./field_inventory_orderstockin.php?id=".$id);
                    exit();
                }
            }
        }
    }
    else 
    {
        $errmsg_arr[]='Your Quantity is More Than Pending Quantity';
        $errflag = true;
        if ($errflag) 
        {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header("location: ./field_inventory_orderstockin.php?id=".$id);
            exit();
        } 
    }   
}
else
{
    $errmsg_arr[]='Something is Wrong';
    $errflag = true;
    if ($errflag) 
    {
        $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
        session_write_close();
        header("location: ./field_inventory_orderstockin.php?id=".$id);
        exit();
    }
}    
?>
